#pragma once

#include <torch/extension.h>
#include <cstdio>

torch::Tensor SoftDepthTestCuda(
    const torch::Tensor& depth_gmms,
    const torch::Tensor& num_gmms
    );

torch::Tensor SoftDepthTest(
    const torch::Tensor& depth_gmms,
    const torch::Tensor& num_gmms)
{
    return SoftDepthTestCuda(
            depth_gmms,
            num_gmms);
}