#version 420

layout(location = 0) out vec4 out_color;

in vec3 vertex_coord;
in mat4 prj_mat;

void main(void) {
	mat4 inversePrjMat = inverse( prj_mat );
	vec4 viewPosH      = inversePrjMat * vec4( gl_FragCoord.x, gl_FragCoord.y, 2.0 * gl_FragCoord.z - 1.0, 1.0 );
	vec3 viewPos       = viewPosH.xyz / viewPosH.w;
	out_color = vec4(-viewPos.z, -viewPos.z, -viewPos.z, 0.0);
}
