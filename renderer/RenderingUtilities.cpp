/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */

#include "RenderingUtilities.hpp"
#include "projects/torchgl_interop/renderer/torchgl_interop.h"

torch::Tensor transformPointsToCamera(const sibr::Camera::Ptr dst_cam,
	const torch::Tensor pointCloud) {
	torch::Tensor world2proj = getWorld2ProjMat(dst_cam).cuda();
	torch::Tensor pts_projected = torch::matmul(pointCloud, world2proj);
	pts_projected = pts_projected.slice(1, 0, 3) / pts_projected.slice(1, 3, 4);

	torch::Tensor world2view = getP3DWorld2ViewMat(dst_cam).cuda();
	torch::Tensor pts_viewspace = torch::matmul(pointCloud, world2view);
	pts_viewspace = pts_viewspace.slice(1, 0, 3) / pts_viewspace.slice(1, 3, 4);

	pts_projected.slice(1, 2, 3) = pts_viewspace.slice(1, 2, 3);

	return pts_projected;
}