#version 460

#define NUM_CAMS (12)

out float out_val;

in vec2 vertex_coord;

uniform int skip_id;

layout(binding=1) uniform sampler2DArray individualScores;
layout(binding=2) uniform sampler2D currentScore;

// Atomic counter
layout(binding = 0, offset = 0) uniform atomic_uint ac[2*NUM_CAMS];

void main(void) {

	float score = texture(currentScore, vertex_coord).x;
	for(int i=0; i<NUM_CAMS; i++){
		if (skip_id==i)  continue;
		/* Use proposed score for max */
		float proposedScore = texture(individualScores, vec3(vertex_coord.xy,i)).x;
		float improvement = max(proposedScore, score);
		atomicCounterAdd(ac[2*i], uint(floor(1024*improvement)));
		atomicCounterAdd(ac[2*i+1], uint(floor(1024*proposedScore)));
	}
	out_val=0.0;
}
