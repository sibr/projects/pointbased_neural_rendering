import torch
import random
from scene_loaders.ibr_scene import MultiScene, Camera
import torchvision
import math
import sys
from datetime import datetime
from diff_rasterization.soft_depth_test import _SoftDepthTest
import time
import cv2
import json
from utils.system_utils import searchForMaxIteration
import argparse
import os
from utils.image_utils import crop_image
from os import makedirs
from scene_loaders.read_scenes_types import readColmapSceneInfo_Test
from PIL import Image
import numpy as np

def PILtoTorch(pil_image, resolution=None):
    if resolution:
        pil_image = pil_image.resize(resolution)
    resized_image = torch.from_numpy(np.array(pil_image)) / 255.0
    if len(resized_image.shape) == 3:
        return resized_image.permute(2, 0, 1).unsqueeze(dim=0)
    else:
        return resized_image.unsqueeze(dim=-1).permute(2, 0, 1).unsqueeze(dim=0)

def mse(img1, img2):
    return (((img1 - img2)) ** 2).mean()

def psnr(img1, img2):
    mse = (((img1 - img2)) ** 2).mean()
    return 20 * torch.log10(1.0 / torch.sqrt(mse))

def render_viewpoint(viewpoint_camera, pcloud_cameras, patch=None, gamma=1.0):
    if patch==None:
        patch = (0, 0, viewpoint_camera.image_width, viewpoint_camera.image_height)
    patch_origin_x, patch_origin_y, patch_size_x, patch_size_y = patch
    features_stack = torch.tensor([]).to(device)
    color_stack = torch.tensor([]).to(device)
    depth_gmms_stack = torch.tensor([]).to(device)
    num_gmms_stack = torch.tensor([]).int().to(device)
    l2_stack = torch.tensor([]).to(device)
    blend_scores_stack = torch.tensor([]).to(device)
    for idx, pcloud_cam in enumerate(pcloud_cameras):
        rendered_point_cloud, depth_gmms, num_gmms, blend_scores = pcloud_cam.render_patch(viewpoint_camera=viewpoint_camera,
                                                                                           patch_origin_x=patch_origin_x, patch_size_x=patch_size_x,
                                                                                           patch_origin_y=patch_origin_y, patch_size_y=patch_size_y,
                                                                                           gamma=gamma)
        color_stack = torch.cat((color_stack, rendered_point_cloud), dim=0)
        blend_scores_stack = torch.cat((blend_scores_stack, blend_scores), dim=0)
        features_stack = torch.cat((features_stack, rendered_point_cloud), dim=0)
        depth_gmms_stack = torch.cat((depth_gmms_stack, depth_gmms), dim=0)
        num_gmms_stack = torch.cat((num_gmms_stack, num_gmms.int()), dim=0)
    color_stack = color_stack.view(color_stack.shape[0], -1, 3, color_stack.shape[2], color_stack.shape[3])

    with torch.no_grad():
        prob_map = _SoftDepthTest.apply(depth_gmms_stack, num_gmms_stack)

    image = neural_renderer(features_stack, prob_map * blend_scores_stack)
    return image, color_stack, None

old_f = sys.stdout
class F:
    def write(self, x):
        if x.endswith("\n"):
            old_f.write(x.replace("\n", " {}\n".format(str(datetime.now()))))
        else:
            old_f.write(x)

    def flush(self):
        old_f.flush()
sys.stdout = F()

device = torch.device("cuda:0")
torch.cuda.set_device(device)
torch.manual_seed(0)
random.seed(0)


parser = argparse.ArgumentParser(description='Train your network sailor.')
parser.add_argument('-i', '--input_path', required=False)

parser.add_argument('--scene_name', required=False, default=None)
parser.add_argument('--load_iter', required=False, type=int, default=None)
parser.add_argument('--max_radius', required=False, type=int, default=8)
parser.add_argument('--test_cameras', required=False, default=3)
parser.add_argument('--extra_features', type=int, default=6)
parser.add_argument('--w', type=int)
parser.add_argument('--h', type=int)

args = parser.parse_args()

with torch.no_grad():
    device = torch.device("cuda:0")
    torch.cuda.set_device(device)
    torch.manual_seed(0)
    random.seed(0)

    dataset_name = "hallway_lamp"
    args.input_path = "/data/graphdeco/user/gkopanas/scenes/catacaustic_new/hallway_lamp/sibr/pbnrScene/" + dataset_name + ".json"

    with open(args.input_path) as json_file:
        input_json = json.load(json_file)
    
    input_json["neural_weights_folder"] = "/data/graphdeco/user/gkopanas/pointbased_neural_rendering/pbnr_pytorch/tensorboard_3d/" + dataset_name + "_final_32/neural_renderer"
    input_json["scenes"][0]["scene_representation_folder"] = "/data/graphdeco/user/gkopanas/pointbased_neural_rendering/pbnr_pytorch/tensorboard_3d/" + dataset_name + "_final_32/" + dataset_name

    for load_iter in [20000, 40000, 60000, 80000, 100000]:
        print("Load Iter {}:".format(load_iter))
        neural_renderer = torch.jit.load(os.path.join(input_json.get("neural_weights_folder"), "model_" + str(load_iter)))

        scene = MultiScene(input_json,
                           args.max_radius, args.extra_features,
                           int(args.test_cameras), load_iter)


        test_cameras_info = readColmapSceneInfo_Test(dataset_name, r"/data/graphdeco/user/gkopanas/scenes/catacaustic_new/hallway_lamp/colmap_1000/test_path_colmap").cameras
        test_cameras = []
        for cam_info in test_cameras_info:
            loaded_image = None
            print(cam_info.image_path)
            if os.path.exists(cam_info.image_path):
                loaded_image = PILtoTorch(Image.open(cam_info.image_path))
            test_cameras.append(Camera(colmap_id=cam_info.uid, R=cam_info.R, T=cam_info.T, FoVx=cam_info.FovX,
                                       FoVy=cam_info.FovY, image=loaded_image,
                                       image_name=cam_info.image_name,
                                       max_radius=scene.scenes[0].getAllCameras()[0].max_radius,
                                       extra_features=scene.scenes[0].getAllCameras()[0].extra_features.shape[1],
                                       loaded_depthmap=None,
                                       loaded_normalmap=None,
                                       uid=0,
                                       loaded_depthdelta=None,
                                       loaded_normaldelta=None,
                                       loaded_uncertainty=None,
                                       loaded_extrafeatures=None,
                                       loaded_expcoefs=None,
                                       loaded_optimizedimages=None))

        print(test_cameras)
        output = "/data/graphdeco/user/gkopanas/pointbased_neural_rendering/pbnr_pytorch/tensorboard_3d/" + dataset_name + "_final_32/backlisted_renders_{}_{}_".format(args.w, args.h) + str(load_iter)
        makedirs(output, exist_ok=True)

        imgs = torch.tensor([])
        gts = torch.tensor([])

        for view_cam in test_cameras:
            print(view_cam.image_name)
            view_list = view_cam.neighbors
            view_cam.image_width = 1000
            view_cam.image_height = 666
            image, image_stack, _ = render_viewpoint(view_cam, scene.scenes[0].getNClosestCameras(view_cam, 10), patch=None)
            torchvision.utils.save_image(image,
                                         os.path.join(output, "out_{}.png".format(view_cam.image_name)))
            torchvision.utils.save_image(view_cam.image,
                                         os.path.join(output, "gt_{}.png".format(view_cam.image_name)))
            imgs = torch.cat((imgs, image.cpu()), dim=0)
            gts = torch.cat((gts, view_cam.image.cpu()), dim=0)

        num_psnr = psnr(imgs, gts)
        print(num_psnr)
        f = open(os.path.join(output, "psnr.txt"), "w")
        f.write("PSNR: {}".format(num_psnr))
        f.close()

