#version 420

layout(binding = 0) uniform sampler2D inputTexture2D;

uniform int level;

out vec4 out_color;

void main()
{    	
	if (level == 0) out_color = texelFetch(inputTexture2D, ivec2(gl_FragCoord.xy), level);
    else
    {
		out_color = vec4(1000, -1000, 0, 0);
		ivec2 coord = ivec2(gl_FragCoord.xy) << 1;
		ivec2 offsets[] = ivec2[] ( ivec2(0,0), ivec2(0,1), ivec2(1,0), ivec2(1,1) );
		for (int i=0; i<4; i++)
		{
			vec4 fetch = texelFetch(inputTexture2D, coord + offsets[i], level - 1);
			if (fetch.a != 0)
			{
				out_color.x = min(fetch.x, out_color.x);	
				out_color.y = max(fetch.y, out_color.y);
				out_color.a = 1;
			}
		}
    }
}