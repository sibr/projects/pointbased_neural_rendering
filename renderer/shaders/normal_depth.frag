#version 420

uniform mat4 world2view;


layout(location = 0) out vec4 out_color;

in vec3 vertex_coord;
in vec3 normal;

void main(void) {
	vec4 pointViewSpace = world2view * vec4(vertex_coord, 1.0);
	pointViewSpace = pointViewSpace/pointViewSpace.w;
	out_color = vec4(normal, -pointViewSpace.z);
}
