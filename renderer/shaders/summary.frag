#version 420

out vec4 out_color;

layout(binding=0) uniform sampler2DArray inputArray1;
layout(binding=1) uniform sampler2DArray inputArray2;
layout(binding=2) uniform sampler2D inputTex1;
layout(binding=3) uniform sampler2D inputTex2;
layout(binding=4) uniform sampler2DArray inputArray3;

void arrayGrid(sampler2DArray inputArray)
{
	out_color = vec4(0);

	int layerCount = textureSize(inputArray, 0).z;
	ivec2 inputResolution = textureSize(inputArray, 0).xy;
	ivec2 outputResolution = inputResolution;

	float inputAspect = inputResolution.x / float(inputResolution.y);
	float outputAspect = outputResolution.x / float(outputResolution.y);

	ivec2 gridResolution = ivec2(sqrt(float(layerCount)) + 0.5);
	gridResolution.x = int(sqrt(layerCount * outputAspect / inputAspect) + 0.5);
	gridResolution.y = int(ceil(layerCount / float(gridResolution.x)));

	vec2 positioRel = gl_FragCoord.xy / outputResolution;
	vec2 texCoord = gridResolution * positioRel;
	int index = int(texCoord.y) * gridResolution.x + int(texCoord.x);
	
	if (index < layerCount) 
	{
		out_color = texture(inputArray, vec3(fract(texCoord), index), 0);
	}
}


void main()
{
	ivec2 readCoord = ivec2(gl_FragCoord.xy);
	readCoord.y = textureSize(inputTex2, 0).y - readCoord.y - 1;
	out_color = texelFetch(inputTex2, readCoord, 0);
	return;
	
	out_color = texelFetch(inputArray3, ivec3(gl_FragCoord.xy, 12), 0);
	arrayGrid(inputArray3);
	return;

	int level = 0;
	vec2 posScale = vec2(textureSize(inputTex1, 0)) / textureSize(inputArray1, 0).xy / pow(2, level);
	out_color = texelFetch(inputTex1, ivec2(gl_FragCoord.xy * posScale), level);
	//out_color *= 0.1;
	//out_color = pow(out_color, vec4(100));
	
}
