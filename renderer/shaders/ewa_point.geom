#version 450

#define IN_CAM_COUNT (8)

layout (points) in;
layout (points, max_vertices=2) out;

in vec3 vPosition[];
in vec3 vColor[];
in vec3 vNormal[];
in float vUncertainty[];
in vec3 vFeats1[];
in vec3 vFeats2[];
in int vDrawID[];

out vec3 gColor;
out vec3 gFeats1;
out vec3 gFeats2;
out float gWeight;
out vec3 gInvCov;
out float gPointSize;
out float gDepth;
out float gDistortion;

uniform mat4 viewMatrix;
uniform mat4 projMatrix;

uniform int splatLayerCount;
uniform vec3 outCamPosition;

struct CameraInfos
{
  mat4 viewMatrix;
  mat4 projMatrix;
  vec4 position;
};

layout(std140, binding=6) uniform InputCameras
{
  CameraInfos inCameras[IN_CAM_COUNT];
};

uniform float maxSplatSize = 15.;

uniform vec2 depthBounds;

vec3 depth2Layers(float depth)
{
	float depthRemap = (depth - depthBounds.x) / (depthBounds.y - depthBounds.x);
	depthRemap = clamp(depthRemap, 0, 1);
	float cont = depthRemap * (splatLayerCount-1);
	float lowerIdx = max(0, floor(cont));
	float upperIdx = min(splatLayerCount-1, lowerIdx + 1);
	float weight = fract(cont);
	weight = round(weight); // use this to revert to hard layering
	return vec3(lowerIdx, upperIdx, weight);
}

vec3 transformPoint(vec3 p, mat4 matrix)
{
	vec4 res = matrix * vec4(p, 1);
	return res.xyz / res.w;
}

vec3 transformVector(vec3 v, mat4 matrix)
{
	vec4 res = matrix * vec4(v, 0);
	return res.xyz;
}

mat2 computeJacobian(vec3 pos, vec3 normal, mat4 viewMatrix, mat4 projMatrix)
{
	vec3 s_world = normalize(vec3(1, 0, -normal.x / normal.z));
	vec3 t_world = cross(normal, s_world);

	vec3 o = transformPoint(pos, viewMatrix);
	vec3 s = transformVector(s_world, viewMatrix);
	vec3 t = transformVector(t_world, viewMatrix);

	mat2 jac;
	jac[0][0] = s.x * o.z - s.z * o.x;
	jac[0][1] = t.x * o.z - t.z * o.x;
	jac[1][0] = s.z * o.y - s.y * o.z;
	jac[1][1] = t.z * o.y - t.y * o.z;
	jac = (mat2(projMatrix) * jac) / (o.z * o.z); 
	return jac;
}


mat2 getCovariance()
{
	int camID = vDrawID[0];
	float cos_eps = 0.1;
	
	// ignore points which are too slanted
	vec3 view_in_ray = normalize(vPosition[0] - inCameras[camID].position.xyz);
	vec3 view_out_ray = normalize(vPosition[0] - outCamPosition);
	float cos_in_view = abs(dot(vNormal[0], view_in_ray));
	float cos_out_view = abs(dot(vNormal[0], view_out_ray));
	if (cos_in_view <= cos_eps && cos_out_view <= cos_eps)
		return mat2(0);

	// from Jacobians to covariance
	mat2 out_view_jacobian = computeJacobian(
		vPosition[0], vNormal[0], 
		viewMatrix, projMatrix);
	mat2 in_view_jacobian = computeJacobian(
		vPosition[0], vNormal[0], 
		inCameras[camID].viewMatrix, inCameras[camID].projMatrix);
	mat2 full_jacobian = out_view_jacobian * inverse(in_view_jacobian);
	mat2 covariance = transpose(full_jacobian) * full_jacobian;
	return mat2(vUncertainty[0]) * covariance;
}


vec2 splatSizeAndDistortionFromCovariance(mat2 cov)
{
	// find eigenvalues of covariance
	const float eps = 0.00001;
	float trace = cov[0][0] + cov[1][1];
	float det = determinant(cov);
	float sTerm = sqrt(pow(trace, 2) - 4 * (det + eps));
	float x1 = abs(0.5 * (trace - sTerm));
	float x2 = abs(0.5 * (trace + sTerm));
	
	float xMin = min(x1, x2);
	float xMax = max(x1, x2);
	float size = clamp(2 * 3 * xMax, 1, maxSplatSize); // 2 to turn into diameter, 3 to have three sigmas
	float distortion = clamp(xMin / (xMax + eps), 0, 1);	
	return vec2(size, distortion);
}

void main(void)
{
	
	// re-project
	gl_Position = projMatrix * viewMatrix * vec4(vPosition[0], 1.0);
	gl_Position.y *= -1;

	//--------------------------------------
	// pass through payload
	gColor = vColor[0];
	gFeats1 = vFeats1[0];
	gFeats2 = vFeats2[0];
	
	//--------------------------------------
	// determine splat size and shape
	mat2 cov = getCovariance();
	if (cov[0][0] < 0 || cov[1][1] < 0 || abs(cov[0][1]) > 1) return;


	mat2 invCov =  (determinant(cov) > 0) ? inverse(cov) : mat2(1);
	gInvCov = vec3(invCov[0][0], invCov[1][0], invCov[1][1]);
	if (gInvCov == vec3(0)) return;
	vec2 sizeDist = splatSizeAndDistortionFromCovariance(cov); 
	gl_PointSize = sizeDist.x;
	gPointSize = gl_PointSize; // diameter
	gDistortion = sizeDist.y;

	//gColor = vec3(determinant(cov)); // / gPointSize);
	//gColor = vec3(gl_PointSize * 0.1);

	//--------------------------------------
	// determine splat layers
	gDepth = distance(vPosition[0], outCamPosition);
	vec3 layers = depth2Layers(gDepth);
	int drawBaseLayer = vDrawID[0] * splatLayerCount;

	gl_Layer = drawBaseLayer + int(layers.x);
	gWeight = 1. - layers.z;
	if (gWeight != 0) EmitVertex();

	gl_Layer = drawBaseLayer + int(layers.y);
	gWeight = layers.z;
	if (gWeight != 0) EmitVertex();

} 
