/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */

#pragma once

#include <torch/torch.h>
#include <cstdio>

torch::Tensor SoftDepthTestCuda(
    const torch::Tensor& depth_gmms,
    const torch::Tensor& num_gmms
    );

torch::Tensor SoftDepthTest(
    const torch::Tensor& depth_gmms,
    const torch::Tensor& num_gmms);
