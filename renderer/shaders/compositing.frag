#version 450

out vec4 out_color;

layout(binding=0) uniform sampler2DArray layeredColor;
layout(binding=1) uniform sampler2DArray layeredAlphaDepth;
layout(binding=2) uniform sampler2DArray layeredDistortion;
layout(binding=3) uniform sampler2DArray layeredFeats1;
layout(binding=4) uniform sampler2DArray layeredFeats2;

flat in int layerID;

uniform int splatLayerCount;
uniform int featureCount;


vec3 fetch(ivec2 coord2D, int layer, int bufferID)
{
	switch (bufferID)
	{
	case 0:
		return texelFetch(layeredColor, ivec3(coord2D, layer), 0).rgb;
		break;
	case 1:
		return texelFetch(layeredFeats1, ivec3(coord2D, layer), 0).rgb;
		break;
	case 2:
		return texelFetch(layeredFeats2, ivec3(coord2D, layer), 0).rgb;
		break;
	}
	return vec3(0);
}


void main()
{		
	int outputLayersPerView = 1 + featureCount;
	int view = layerID / outputLayersPerView;
	int channel = layerID % outputLayersPerView;

	int startLayer = splatLayerCount * view;
	int endLayer = startLayer + splatLayerCount - 1;
	
	ivec2 coord2D = ivec2(gl_FragCoord.xy);
	
	out_color = vec4(0, 0, 0, 1);
	
	float cumAlpha = 1;
	float out_distortion = 0;

	for (int l = endLayer; l >= startLayer; l--)
	{
		vec3 color = fetch(coord2D, l, channel);
		float alpha = texelFetch(layeredAlphaDepth, ivec3(coord2D, l), 0).x;
		alpha = exp(alpha);
		out_color.rgb = color + out_color.rgb * alpha;
		if (channel == 1)
		{
			float distortion = texelFetch(layeredDistortion, ivec3(coord2D, l), 0).x;
			out_distortion = distortion + out_distortion * alpha;
		}
		cumAlpha *= alpha;
	}	
	out_color.a = (channel != 1) ? cumAlpha : out_distortion; // save distortion in alpha channel of first feature map
}


