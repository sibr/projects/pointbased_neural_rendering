/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */

#pragma once
#include <torch/torch.h>
#include <cstdio>
#include <tuple>


std::tuple<torch::Tensor, torch::Tensor, torch::Tensor, torch::Tensor, torch::Tensor, torch::Tensor>
RasterizePointsPbnrCuda(
    const torch::Tensor& points,
    const torch::Tensor& colors,
    const torch::Tensor& inv_cov,
    const int max_radius,
    const int image_height,
    const int image_width,
    const int points_per_pixel,
    const float zfar,
    const float znear,
    const float gamma);



std::tuple<torch::Tensor, torch::Tensor, torch::Tensor, torch::Tensor, torch::Tensor, torch::Tensor>
RasterizePoints(
    const torch::Tensor& points,
    const torch::Tensor& colors,
    const torch::Tensor& inv_cov,
    const int max_radius,
    const int image_height,
    const int image_width,
    const int points_per_pixel,
    const float zfar,
    const float znear,
    const float gamma)
{
    return RasterizePointsPbnrCuda(
        points,
        colors,
        inv_cov,
        max_radius,
        image_height,
        image_width,
        points_per_pixel,
        zfar,
        znear,
        gamma);
}