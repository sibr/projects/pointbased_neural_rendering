import torch
from scene_loaders.ibr_scene import MultiScene
import uuid
from torch.utils.tensorboard import SummaryWriter
import argparse
from model_defs.ResNet_2DConv import FixUpResNet
from random import randint
import random
from model_defs.Loss_Functions import l1_loss
from utils.system_utils import mkdir_p, searchForMaxIteration
from utils.image_utils import crop_image
import os
import subprocess
import sys
from datetime import datetime
import torchvision
from diff_rasterization.soft_depth_test import _SoftDepthTest
import json

old_f = sys.stdout
class F:
    def write(self, x):
        if x.endswith("\n"):
            old_f.write(x.replace("\n", " {}\n".format(str(datetime.now()))))
        else:
            old_f.write(x)

    def flush(self):
        old_f.flush()
sys.stdout = F()


def render_viewpoint(viewpoint_camera, pcloud_cameras, patch=None, gamma=1.0):
    if patch==None:
        patch = (0, 0, viewpoint_camera.image_width, viewpoint_camera.image_height)
    patch_origin_x, patch_origin_y, patch_size_x, patch_size_y = patch
    features_stack = torch.tensor([]).to(device)
    color_stack = torch.tensor([]).to(device)
    depth_gmms_stack = torch.tensor([]).to(device)
    num_gmms_stack = torch.tensor([]).int().to(device)
    l2_stack = torch.tensor([]).to(device)
    blend_scores_stack = torch.tensor([]).to(device)
    for idx, pcloud_cam in enumerate(pcloud_cameras):
        rendered_point_cloud, depth_gmms, num_gmms, blend_scores = pcloud_cam.render_patch(viewpoint_camera=viewpoint_camera,
                                                                                           patch_origin_x=patch_origin_x, patch_size_x=patch_size_x,
                                                                                           patch_origin_y=patch_origin_y, patch_size_y=patch_size_y,
                                                                                           gamma=gamma)
        color_stack = torch.cat((color_stack, rendered_point_cloud), dim=0)
        blend_scores_stack = torch.cat((blend_scores_stack, blend_scores), dim=0)
        features_stack = torch.cat((features_stack, rendered_point_cloud), dim=0)
        depth_gmms_stack = torch.cat((depth_gmms_stack, depth_gmms), dim=0)
        num_gmms_stack = torch.cat((num_gmms_stack, num_gmms.int()), dim=0)
        l2_stack = torch.cat((l2_stack, torch.nn.functional.mse_loss(rendered_point_cloud[:,:3,:,:], crop_image(viewpoint_camera.image, patch)*blend_scores).unsqueeze(0)), dim=0)
    color_stack = color_stack.view(color_stack.shape[0], -1, 3, color_stack.shape[2], color_stack.shape[3])

    with torch.no_grad():
        prob_map = _SoftDepthTest.apply(depth_gmms_stack, num_gmms_stack)

    image = neural_renderer(features_stack, prob_map * blend_scores_stack)
    return image, color_stack, l2_stack.mean()

parser = argparse.ArgumentParser(description='Train your network sailor.')
parser.add_argument('-i', '--input_path', required=True)
parser.add_argument('-o', '--output_path', required=False)

parser.add_argument('--load_iter', required=False, type=int, default=None)

parser.add_argument('--neural_lr', required=False, type=float, default=0.0001)
parser.add_argument('--depth_lr', required=False, type=float, default=0.0001)
parser.add_argument('--uncertainty_lr', required=False, type=float, default=0.01)
parser.add_argument('--feature_lr', required=False, type=float, default=0.001)
parser.add_argument('--normal_lr', required=False, type=float, default=0.0001)
parser.add_argument('--exp_coef_lr', required=False, type=float, default=0.001)
parser.add_argument('--image_lr', required=False, type=float, default=0.0)

parser.add_argument('--exp_coef_reg_w', required=False, type=float, default=0.2)
parser.add_argument('--photocons_L_w', required=False, type=float, default=0.0)

parser.add_argument('--residual_blocks', required=False, type=int, default=16)
parser.add_argument('--internal_depth', required=False, type=int, default=64)
parser.add_argument('--dropout', required=False, type=float, default=0.0)
parser.add_argument('--reguralization', required=False, type=float, default=0.0)

parser.add_argument('--skip_validation', action='store_true', dest='skip_validation')

parser.add_argument('--extra_features', type=int, default=6)
parser.add_argument('--max_radius', required=False, type=int, default=8)

parser.add_argument('--test_cameras', required=False, default=0)

parser.add_argument('--input_views', required=False, default=9)

args = parser.parse_args()

device = torch.device("cuda:0")
torch.cuda.set_device(device)
torch.manual_seed(0)
random.seed(0)

with open(args.input_path) as json_file:
    input_json = json.load(json_file)



neural_renderer = None
if input_json.get("neural_weights_folder"):
    if args.load_iter:
        load_iter = args.load_iter
    else:
        load_iter = searchForMaxIteration(input_json.get("neural_weights_folder"))
    neural_renderer = torch.jit.load(os.path.join(input_json.get("neural_weights_folder"), "model_" + str(load_iter)))
else:
    # 5 + extra_features is 3 for color and 2 for depth and backgrouind bleeding
    neural_renderer = FixUpResNet(in_channels=args.extra_features+3,
                                  internal_depth=args.internal_depth,
                                  blocks=args.residual_blocks,
                                  kernel_size=3,
                                  dropout=args.dropout).to(device)
print(neural_renderer)
neural_renderer = torch.jit.script(neural_renderer)

scene = MultiScene(input_json,
                   args.max_radius, args.extra_features,
                   int(args.test_cameras), args.load_iter)

if args.output_path:
    unique_str = args.output_path
else:
    unique_str = str(uuid.uuid4())

tensorboard_folder = "./tensorboard_3d/{}".format(unique_str)
assert not os.path.exists(tensorboard_folder), ("Output folder already exists!")
tb_writer = SummaryWriter(tensorboard_folder)

print("Output Folders:")
print(" * {}".format(tensorboard_folder))

with open(os.path.join(tensorboard_folder, "run_info"), 'w') as git_log_f:
    git_log_f.write(" ".join(sys.argv))
    subprocess.run(["git", "log", "-n", "1"], stdout=git_log_f)
    subprocess.run(["git", "diff"], stdout=git_log_f)


optimizer_depth_map = torch.optim.Adam(scene.getAllDepthParameters(), lr=args.depth_lr)
optimizer_uncertainty = torch.optim.Adam(scene.getAllUncertaintyParameters(), lr=args.uncertainty_lr)
optimizer_neural_renderer = torch.optim.Adam(neural_renderer.parameters(), lr=args.neural_lr, weight_decay=args.reguralization)
optimizer_features = torch.optim.Adam(scene.getAllFeatureParameters(), lr=args.feature_lr)
optimizer_normal_map = torch.optim.Adam(scene.getAllNormalParameters(), lr=args.normal_lr)
optimizer_exp_coef = torch.optim.Adam(scene.getAllExpCoefParameters(), lr=args.exp_coef_lr)
optimizer_image = torch.optim.Adam(scene.getAllImageParameters(), lr=args.image_lr)

iteration = 1
gamma = 1.0

viewpoint_stack = scene.getAllTrainCameras().copy()
while True:
    if iteration%500==0:
        print("[ITER {}]".format(iteration))
    if iteration==100000:
        break
    viewpoint_idx = randint(0, len(viewpoint_stack)-1)
    viewpoint_cam = viewpoint_stack.pop(viewpoint_idx)

    n_num = randint(5, 8)
    pcloud_cams = scene.getPCloudCamsForScore(viewpoint_cam, sample=True, N=n_num)
    random.shuffle(pcloud_cams)

    # Zero out the gradients
    optimizer_depth_map.zero_grad()
    optimizer_uncertainty.zero_grad()
    optimizer_neural_renderer.zero_grad()
    optimizer_features.zero_grad()
    optimizer_normal_map.zero_grad()
    optimizer_exp_coef.zero_grad()
    optimizer_image.zero_grad()

    patch_size = 150
    patch = (randint(0, viewpoint_cam.image_width - patch_size), randint(0, viewpoint_cam.image_height - patch_size), patch_size, patch_size)

    image, image_stack, l2 = render_viewpoint(viewpoint_cam, pcloud_cams, patch)
    gt_image = viewpoint_cam.getImageHarmonized().to(device)
    gt_image = crop_image(gt_image, patch)

    Ll1 = l1_loss(image, gt_image)
    L_harmReg = args.exp_coef_reg_w*((1.0 - torch.stack(scene.getAllExpCoefParameters(), dim=0))**2).mean()
    L_l2pc = args.photocons_L_w * l2
    loss = Ll1 + L_harmReg + L_l2pc

    loss.backward()

    # We do the optimization step once for all the scales
    optimizer_depth_map.step()
    optimizer_uncertainty.step()
    optimizer_neural_renderer.step()
    optimizer_features.step()
    optimizer_normal_map.step()
    optimizer_exp_coef.step()
    optimizer_image.step()

    with torch.no_grad():
        scene.normalizeNormals()

    tb_writer.add_scalars('train_loss_patches', {"total_loss": loss.item(),
                                                 "l1_loss":  Ll1.item(),
                                                 "L_harmReg": L_harmReg.item(),
                                                 "L_l2pc": L_l2pc.item()}, iteration)
    with torch.no_grad():
        if not viewpoint_stack:
            viewpoint_stack = scene.getAllTrainCameras().copy()
        if iteration%20000==0:
            print("[ITER {}]Saving Model...".format(iteration))
            mkdir_p("./{}/neural_renderer/".format(tensorboard_folder))
            neural_renderer.save("./{}/neural_renderer/model_{}".format(tensorboard_folder, iteration))
            scene.save(tensorboard_folder, iteration)
        if iteration%500==0 and not args.skip_validation:
            torch.cuda.empty_cache()
            total_loss = 0.0
            total_L1 = 0.0
            test_cam_list = scene.scenes[0].blacklist_cameras[:3]
            for test_viewpoint in test_cam_list:
                torch.cuda.empty_cache()

                test_pcloud_cams = scene.getPCloudCamsForScore(test_viewpoint, sample=False, N=12)
                random.shuffle(test_pcloud_cams)
                image, image_stack, _ = render_viewpoint(test_viewpoint, test_pcloud_cams, None)
                gt_image = test_viewpoint.getImageHarmonized().to("cuda")

                Ll1 = l1_loss(image, gt_image)
                loss = Ll1

                total_loss += loss.item()/(len(test_cam_list))
                total_L1 += Ll1.item()/(len(test_cam_list))

                tb_writer.add_images("test_neural_render_{}".format(test_viewpoint.image_name), torch.clamp(image, 0.0, 1.0),
                                     global_step=iteration)
                tb_writer.add_images("test_average_input_{}".format(test_viewpoint.image_name),
                                     torch.clamp(image_stack.mean(0).mean(0, keepdim=True), 0.0, 1.0),
                                     global_step=iteration)
                """
                for pc_render_idx in range(image_stack.shape[0]):
                    for feature_idx in range(image_stack.shape[1]):
                        tb_writer.add_images(
                            "test_point_cloud_viewpoint_{}_pcloud_{}_feature_{}".format(test_viewpoint.image_name,
                                                                                        test_pcloud_cams[pc_render_idx].image_name,
                                                                                        feature_idx),
                            image_stack[pc_render_idx:pc_render_idx+1, feature_idx, :, :, :],
                            global_step=iteration)
                """
            tb_writer.add_scalars('test_loss_viewpoint', {"total_loss": total_loss, "l1_loss": total_L1}, iteration)
            print("[ITER {}] Test Loss {}".format(iteration, total_loss))

            total_loss = 0.0
            total_L1 = 0.0
            validation_cameras = [scene.getAllTrainCameras()[idx] for idx in [4, 9, 12]]
            for train_viewpoint in validation_cameras:
                torch.cuda.empty_cache()

                train_pcloud_cams = scene.getPCloudCamsForScore(train_viewpoint, sample=False)
                random.shuffle(train_pcloud_cams)
                image, image_stack, _ = render_viewpoint(train_viewpoint, train_pcloud_cams, None)
                gt_image = train_viewpoint.getImageHarmonized().to("cuda")

                Ll1 = l1_loss(image, gt_image)
                loss = Ll1

                total_loss += loss.item()/(len(validation_cameras))
                total_L1 += Ll1.item()/(len(validation_cameras))

                tb_writer.add_images("train_image_harmonized_{}".format(train_viewpoint.image_name),
                                     torchvision.utils.make_grid([train_viewpoint.getImageHarmonized().clamp(0.0, 1.0).squeeze().cpu(), train_viewpoint.original_image.squeeze()]).unsqueeze(0),
                                     global_step=iteration)
                tb_writer.add_images("train_neural_render_{}".format(train_viewpoint.image_name), torch.clamp(image, 0.0, 1.0),
                                     global_step=iteration)
                tb_writer.add_images("train_average_input_{}".format(train_viewpoint.image_name),
                                     torch.clamp(image_stack.mean(0).mean(0, keepdim=True), 0.0, 1.0),
                                     global_step=iteration)
                """
                for pc_render_idx in range(image_stack.shape[0]):
                    for feature_idx in range(image_stack.shape[1]):
                        tb_writer.add_images(
                            "train_point_cloud_viewpoint_{}_pcloud_{}_feature_{}".format(train_viewpoint.image_name,
                                                                                         train_pcloud_cams[pc_render_idx].image_name,
                                                                                         feature_idx),
                            image_stack[pc_render_idx:pc_render_idx+1, feature_idx, :, :, :],
                            global_step=iteration)
                """
            tb_writer.add_scalars('train_loss_viewpoint',
                                  {"total_loss": total_loss, "l1_loss": total_L1}, iteration)
            tb_writer.add_scalars('exp_coef',
                                  {cam.image_name: cam.exposure_coef for cam in scene.scenes[0].cameras},
                                  global_step=iteration)
            print("[ITER {}] Train Loss {}".format(iteration, total_loss))
            torch.cuda.empty_cache()

    iteration += 1
print("[ITER {}]Saving Model...".format(iteration))
neural_renderer.save("./{}/neural_renderer/model_{}".format(tensorboard_folder, iteration))
scene.save(tensorboard_folder, iteration)

