# Copyright (C) 2020, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
#
# This software is free for non-commercial, research and evaluation use
# under the terms of the LICENSE.md file.
#
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr


set(PROJECT_PAGE "pointbasedNeuralRenderingPage")
set(PROJECT_LINK "https://gitlab.inria.fr/sibr/projects/pointbased_neural_rendering")
set(PROJECT_DESCRIPTION "Point Based Neural Rendering with Per-View Optimization")
set(PROJECT_TYPE "OURS")

