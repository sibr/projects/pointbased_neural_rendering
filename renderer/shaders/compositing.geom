#version 440

layout(points) in;
layout(triangle_strip, max_vertices = 4) out;

flat out int layerID;

void main()
{
	gl_Layer = int(gl_PrimitiveIDIn);
	layerID = gl_Layer;
	gl_Position = vec4(-1, -1, 0, 1);
	EmitVertex();
	gl_Position = vec4(1, -1, 0, 1);
	EmitVertex();
	gl_Position = vec4(-1, 1, 0, 1);
	EmitVertex();
	gl_Position = vec4(1, 1, 0, 1);
	EmitVertex();
 }