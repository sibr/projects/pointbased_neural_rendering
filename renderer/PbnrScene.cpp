/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */

#include "PbnrScene.hpp"
#include "json.hpp"
#include "projects/torchgl_interop/renderer/torchgl_interop.h"
#include "projects/torchgl_interop/renderer/torchgl_interop.h"
#include "RenderingUtilities.hpp"

#include <filesystem>

using json = nlohmann::json;

namespace sibr {
	PbnrScene::PbnrScene() : BasicIBRScene() {
	}

	PbnrScene::PbnrScene(const PbnrAppArgs& myArgs, bool preprocess)
		: BasicIBRScene(myArgs, true, false)
	{

		position_depth_shader.init("ULRV3Depth",
			sibr::loadFile(sibr::getShadersDirectory() + "/pbnr/ulr_intersect_pbnr.vert"),
			sibr::loadFile(sibr::getShadersDirectory() + "/pbnr/ulr_intersect_pbnr.frag"));
		position_depth_proj.init(position_depth_shader, "proj");
		position_depth_world2view.init(position_depth_shader, "world2view");

		normal_depth_shader.init("NormalDepth",
			sibr::loadFile(sibr::getShadersDirectory() + "/pbnr/normal_depth.vert"),
			sibr::loadFile(sibr::getShadersDirectory() + "/pbnr/normal_depth.frag"));
		normal_depth_proj.init(normal_depth_shader, "proj");
		normal_depth_world2view.init(normal_depth_shader, "world2view");

		score_shader.init("score",
			sibr::loadFile(sibr::getShadersDirectory() + "/pbnr/repro.vert"),
			sibr::loadFile(sibr::getShadersDirectory() + "/pbnr/score.frag"));
		score_shader_sampled_proj.init(score_shader, "sampledCamProj");
		score_shader_sampled_pos.init(score_shader, "sampledCamPos");
		score_shader_sampled_dir.init(score_shader, "sampledCamDir");
		score_shader_current_pos.init(score_shader, "currentCamPos");
		
		int num_cams = cameras()->inputCameras().size();
		GLShader::Define::List defines;
		defines.emplace_back("NUM_CAMS", num_cams);
		compare_shader.init("compare",
			sibr::loadFile(sibr::getShadersDirectory() + "/pbnr/repro.vert"),
			sibr::loadFile(sibr::getShadersDirectory() + "/pbnr/compare.frag",defines));
		skid_id_uniform.init(compare_shader, "skip_id");
		maxPool_shader.init("maxPool",
			sibr::loadFile(sibr::getShadersDirectory() + "/pbnr/repro.vert"),
			sibr::loadFile(sibr::getShadersDirectory() + "/pbnr/maxPool.frag"));
		maxPool_shader_bestId.init(maxPool_shader, "bestImprId");

		//Precompute 3d Pos textures for camera selection
		for (int cam_idx = 0; cam_idx < num_cams; cam_idx++) {

			//3D pos map of the input camera
			const sibr::InputCamera::Ptr cam = cameras()->inputCameras()[cam_idx];
			sibr::RenderTargetRGBA32F::Ptr pos3DRT(new sibr::RenderTargetRGBA32F(900*(1/4.0), 600*(1/4.0)));
			renderDepthAtRT(proxies()->proxy(), *cam, *pos3DRT);
			pos3DTextures.push_back(pos3DRT);
		}


		if (preprocess) {
			std::cout << "PreProcessing" << std::endl;
			json j;
			int w = 1297;
			int h = 840;
			for (int idx = 0; idx < cameras()->inputCameras().size(); idx++) {
				const sibr::InputCamera::Ptr cam = cameras()->inputCameras()[idx];

				std::string image_path = myArgs.dataset_path.get() + "/pbnrScene/images/";
				makeDirectory(image_path);

				ImageRGB image = images()->inputImages()[idx]->resized(w, h);
				torch::Tensor im_tensor = imToTensor(image) / 255.0;
				saveTensor(im_tensor, image_path + cam->name().substr(0, cam->name().find_last_of(".")) + ".ts");
				image.save(image_path + cam->name());


				torch::Tensor normal_and_depth;
				sibr::Mesh pvMesh;
				pvMesh.load(myArgs.dataset_path.get() + "/deep_blending/pvmeshes/" + cam->name().substr(0, cam->name().find_last_of(".")) + ".ply");
				pvMesh.generateNormals();
				getNormalAndDepthMap(*cam, pvMesh, image.w(), image.h(), normal_and_depth);

				//getNormalAndDepthMap(*cam, proxies()->proxy(), image.w(), image.h(), normal_and_depth);


				std::string depth_map_path = myArgs.dataset_path.get() + "/pbnrScene/depth_maps_type_2/";
				makeDirectory(depth_map_path);
				saveTensor(normal_and_depth.slice(1, 3, 4),
					depth_map_path + cam->name().substr(0, cam->name().find_last_of(".")) + ".ts");

				ImageRGB32F depth_image;
				torch::Tensor normalizeddepth = torch::log(1.0 + normal_and_depth.slice(1, 3, 4)) / torch::log(normal_and_depth.slice(1, 3, 4).max() + 1.0) * 255.0;
				depth_image = tensorToIm(torch::cat({ normalizeddepth ,normalizeddepth ,normalizeddepth }, 1));
				cv::imwrite(depth_map_path + cam->name().substr(0, cam->name().find_last_of(".")) + ".png", depth_image.toOpenCV());

				std::string normal_map_path = myArgs.dataset_path.get() + "/pbnrScene/normal_maps/";
				makeDirectory(normal_map_path);
				saveTensor(normal_and_depth.slice(1, 0, 3),
					normal_map_path + cam->name().substr(0, cam->name().find_last_of(".")) + ".ts");

				ImageRGB32F normal_image;
				normal_image = tensorToIm((normal_and_depth.slice(1, 0, 3) + 1.0) * 128.0);
				cv::imwrite(normal_map_path + cam->name().substr(0, cam->name().find_last_of(".")) + ".png", normal_image.toOpenCV());

				//std::vector<int> neighbor_ids = getNBestCoverageCamerasGPU(*cam, Vector2u(w, h), 8 + 5, 1.0 / 16.0, true, idx);
				std::vector<int> neighbor_ids = getNBestCoverageCameras(*cam, Vector2u(900, 600), std::min(8 + 5, num_cams - 1), 1.0 / 16.0, true, idx);

				std::vector<std::string> neighbor_names;
				for (int i = 0; i < neighbor_ids.size(); i++) {
					const sibr::InputCamera::Ptr neighbor = cameras()->inputCameras()[neighbor_ids[i]];
					neighbor_names.push_back(neighbor->name().substr(0, neighbor->name().find_last_of(".")));
				}
				j[cam->name().substr(0, cam->name().find_last_of("."))] = neighbor_names;
			}
			std::ofstream o(myArgs.dataset_path.get() + "/pbnrScene/neighbors_dict.json");
			o << std::setw(4) << j << std::endl;
			return;
		}

		for (int idx = 0; idx < cameras()->inputCameras().size(); idx++) {
			_cam_weights_idx.push_back(std::make_tuple(0.0, idx));
		}

		std::string normal_map_path    = myArgs.tensorboard_path.get() + "/" + myArgs.scene_name.get() + "/normals/iteration_" + myArgs.iteration.get() + "/";
		std::string depth_delta_path   = myArgs.tensorboard_path.get() + "/" + myArgs.scene_name.get() + "/depth_deltas/iteration_" + myArgs.iteration.get() + "/";
		std::string images_path        = myArgs.tensorboard_path.get() + "/" + myArgs.scene_name.get() + "/images/iteration_" + myArgs.iteration.get() + "/";
		std::string features_path      = myArgs.tensorboard_path.get() + "/" + myArgs.scene_name.get() + "/learned_features/iteration_" + myArgs.iteration.get() + "/";
		std::string uncertainties_path = myArgs.tensorboard_path.get() + "/" + myArgs.scene_name.get() + "/uncertainty_map/iteration_" + myArgs.iteration.get() + "/";
		std::string exp_coef_path      = myArgs.tensorboard_path.get() + "/" + myArgs.scene_name.get() + "/exposure_coef/iteration_" + myArgs.iteration.get() + "/";

		readNormalMaps(normal_map_path);
		readDepthMaps(myArgs.dataset_path.get() + "/pbnrScene/depth_maps_type_2/", depth_delta_path);
		readImages(images_path);
		readFeatureMaps(features_path);
		readUncertainties(uncertainties_path);
		readExpCoef(exp_coef_path);
		
		exp_coef_mean = torch::zeros({1}, torch::TensorOptions().device(torch::kCUDA, 0));
		for (uint i = 0; i < cameras()->inputCameras().size(); i++) {
			exp_coef_mean += _exp_coefs[i];
		}
		exp_coef_mean = exp_coef_mean / float(cameras()->inputCameras().size());

		renderPixelPositionsNDC();
		for (uint i = 0; i < cameras()->inputCameras().size(); i++) {
			torch::Tensor filter = (_depths[i] != -1.0).flatten();
			_points3d_color_tuple.push_back(get3DPointCloudFromCamera(cameras()->inputCameras()[i],
				_images[i],
				_depths[i],
				_pixelsNDC[i].cuda(),
				filter));
			_normals[i] = _normals[i].index({ {filter} });
			_features[i] = _features[i].index({ {filter} });
			_uncertainties[i] = _uncertainties[i].index({ {filter} });
		}
	}

	torch::Tensor PbnrScene::readTensorFromDisk(std::string path) {
		std::ifstream fin(path, std::ios::out | std::ios::binary);

		// get length of file:
		fin.seekg(0, fin.end);
		int length = fin.tellg();
		fin.seekg(0, fin.beg);

		// read file
		std::vector<char> buffer(length);
		fin.read(buffer.data(), length);
		torch::IValue x = torch::pickle_load(buffer);

		return x.toTensor();
	}

	void PbnrScene::readImages(std::string images_path) {
		for (sibr::InputCamera::Ptr cam : cameras()->inputCameras()) {
			torch::Tensor imagesTensor = readTensorFromDisk(images_path + cam->name().substr(0, cam->name().find_last_of(".")) + ".img");
			_images.push_back(imagesTensor.cuda());
		}
	}

	void PbnrScene::readFeatureMaps(std::string features_path) {
		if (directoryExists(features_path)) {
			for (sibr::InputCamera::Ptr cam : cameras()->inputCameras()) {
				torch::Tensor featuresTensor = readTensorFromDisk(features_path + cam->name().substr(0, cam->name().find_last_of(".")) + ".feat");
				featuresTensor = torch::sigmoid(featuresTensor);
				_features.push_back(featuresTensor.view({ featuresTensor.sizes()[1],
														 featuresTensor.sizes()[2] * featuresTensor.sizes()[3] })
					.permute({ 1,0 }).cuda());
			}
		}
		else {
			SIBR_WRG << "Could not find features." << std::endl;
		}
	}

	void PbnrScene::readUncertainties(std::string uncertainties_path) {
		for (sibr::InputCamera::Ptr cam : cameras()->inputCameras()) {
			if (directoryExists(uncertainties_path)) {
				torch::Tensor uncertainty = readTensorFromDisk(uncertainties_path + cam->name().substr(0, cam->name().find_last_of(".")) + ".uncertainty");
				_uncertainties.push_back(uncertainty.cuda());
			}
			else {
				SIBR_WRG << "Could not find uncertainties." << std::endl;
				_uncertainties.push_back(torch::ones({ cam->h() * cam->w(), 1 }).cuda());
			}
		}
	}

	void PbnrScene::readExpCoef(std::string exp_coef_path) {
		for (sibr::InputCamera::Ptr cam : cameras()->inputCameras()) {
			if (directoryExists(exp_coef_path)) {
				torch::Tensor exp_coef = readTensorFromDisk(exp_coef_path + cam->name().substr(0, cam->name().find_last_of(".")) + ".exp_coef");
				_exp_coefs.push_back(exp_coef.cuda());
			}
			else {
				SIBR_WRG << "Could not find exposure coefficients for harmonization." << std::endl;
				_exp_coefs.push_back(torch::ones({ 1 }).cuda());
			}
		}
	}

	void PbnrScene::readDepthMaps(std::string depth_map_path, std::string depth_delta_path) {
		for (sibr::InputCamera::Ptr cam : cameras()->inputCameras()) {
			// Read depth map
			std::string cam_filename = cam->name().substr(0, cam->name().find_last_of("."));
			torch::Tensor depthTensor = readTensorFromDisk(depth_map_path + cam_filename + ".ts");
			cam->zfar(2.0 * torch::max(depthTensor).item<float>());
			cam->znear(getNonZeroMin(depthTensor) / 2.0);
			//Read depth delta
			if (directoryExists(depth_delta_path)) {
				torch::Tensor depthDelta = readTensorFromDisk(depth_delta_path + cam_filename + ".depth_delta");
				_depths.push_back((depthTensor + depthDelta).cuda());
			}
			else {
				SIBR_WRG << "Could not find depth delta for image " << cam->name() << std::endl;
				_depths.push_back(depthTensor.cuda());
			}
		}
	}

	void PbnrScene::readNormalMaps(std::string normal_maps_path) {
		for (sibr::InputCamera::Ptr cam : cameras()->inputCameras()) {
			std::string cam_filename = cam->name().substr(0, cam->name().find_last_of("."));

			//Read optimized normal map.
			if (directoryExists(normal_maps_path)) {
				torch::Tensor normal_map = readTensorFromDisk(normal_maps_path + cam_filename + ".normals");
				_normals.push_back(normal_map.cuda().view({ 3, normal_map.sizes()[2] * normal_map.sizes()[3] }).permute({ 1, 0 }));
			}
			else {
				SIBR_WRG << "Could not find normal map for image " << cam->name() << std::endl;
			}
		}
	}


	void PbnrScene::renderPixelPositionsNDC(void) {
		torch::Tensor x_axis, y_axis;
		for (int idx = 0; idx < _images.size(); idx++) {
			// Creates X and Y axises and adds half pixel to center them.
			int w = _images[idx].sizes()[3];
			int h = _images[idx].sizes()[2];
			x_axis = torch::linspace(-1.0, 1.0 - 2.0 / w, w) + 2.0 / (2.0 * w);
			y_axis = torch::linspace(-1.0, 1.0 - 2.0 / h, h) + 2.0 / (2.0 * h);
			// MeshGrid creates a 2-D grid from two 1-D rulers
			std::vector<torch::Tensor> yx_vector = torch::meshgrid(torch::TensorList(std::vector<torch::Tensor>({ y_axis, x_axis })));
			std::vector<torch::Tensor> xy_vector = { yx_vector[1], yx_vector[0] };
			// Adds standard 1.0 everywhere for the z-component of the 3-d NDC positions of the pixels.
			xy_vector.push_back(torch::ones_like(xy_vector[0]));
			// Add the tensor to the list.
			_pixelsNDC.push_back(torch::stack(torch::TensorList(xy_vector)));
		}
	}

	void PbnrScene::get3DPositionAndDepthMap(const sibr::Camera& ref_cam, const int w, const int h,
		torch::Tensor& out_renderedTensor) {
		ImageRGBA32F renderedImage(w, h);
		sibr::RenderTargetRGBA32F depthRT(w, h);
		renderDepthAtRT(proxies()->proxy(), ref_cam, depthRT);
		depthRT.readBack(renderedImage);
		out_renderedTensor = imToTensor(renderedImage);
	}

	void PbnrScene::getNormalAndDepthMap(const sibr::Camera& ref_cam, const sibr::Mesh& mesh, const int w, const int h,
		torch::Tensor& out_renderedTensor) {
		ImageRGBA32F renderedImage(w, h);
		sibr::RenderTargetRGBA32F depthRT(w, h);
		renderNormalDepthAtRT(mesh, ref_cam, depthRT);
		depthRT.readBack(renderedImage);
		out_renderedTensor = imToTensor(renderedImage);
	}

	std::tuple < torch::Tensor, torch::Tensor >
		PbnrScene::get3DPointCloudFromCamera(static sibr::Camera::Ptr source_cam,
			static torch::Tensor color,
			static torch::Tensor depth,
			static torch::Tensor pixelsNDC,
			static torch::Tensor filter)
	{
		// In the forward projection after perspective trasnformation the w-component
		// holds the depth and we divide by it to get a non-homogenous point. So here
		// we have to invert that process.
		torch::Tensor points = depth * pixelsNDC;
		torch::Tensor points_color = color;
		points = points.permute({ 0, 2, 3, 1 }).view({ points.sizes()[2] * points.sizes()[3],
													   points.sizes()[1] });
		points_color = points_color.permute({ 0, 2, 3, 1 }).view({ points_color.sizes()[2] * points_color.sizes()[3],
																   points_color.sizes()[1] });

		torch::Tensor projT = getP3DProjMat(source_cam);
		auto a = projT.accessor<float, 2 >();
		a[2][2] = 1.0; // This removes the z component of the trasnformation because z is already at view space.
		projT = projT.slice(0, 0, 3).slice(1, 0, 3);
		projT = torch::inverse(projT).cuda();

		torch::Tensor viewP3D = getP3DWorld2ViewMat(source_cam).cuda();
		torch::Tensor viewT = torch::inverse(viewP3D);

		torch::Tensor points_proj_inv = torch::matmul(points, projT);

		torch::Tensor points_hom = torch::cat(torch::TensorList({ points_proj_inv, torch::ones({ points_proj_inv.sizes()[0], 1 }, torch::kCUDA) }), 1);
		torch::Tensor points_wc = torch::matmul(points_hom, viewT);
		points_wc = points_wc.slice(1, 0, 4) / points_wc.slice(1, 3, 4);

		return std::make_tuple(points_wc.index({ { filter } }), points_color.index({ { filter } }));
	}

	float PbnrScene::getNonZeroMin(const torch::Tensor t) {
		float* ptr = (float*)t.data_ptr();
		float min = 100000.0;
		// iterate through all elements
		for (int i = 0; i < t.numel(); ++i)
		{
			if (ptr[i] >= 0.0 && ptr[i] < min) {
				min = ptr[i];
			}
		}
		return min;
	}

	void PbnrScene::renderDepthAtRT(const sibr::Mesh& mesh,
		const sibr::Camera& eye,
		sibr::RenderTargetRGBA32F& rt)
	{

		// Bind and clear RT.
		rt.bind();
		glViewport(0, 0, rt.w(), rt.h());
		glClearColor(0, 0, 0, -1.0);
		glClearDepth(1.0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		// Render the mesh from the current viewpoint.
		position_depth_shader.begin();

		position_depth_world2view.set(eye.view());
		position_depth_proj.set(eye.proj());

		mesh.render();

		position_depth_shader.end();
		rt.unbind();
	}

	void PbnrScene::renderScoreAtRT(sibr::RenderTargetLum32F& score,
		sibr::RenderTargetRGBA32F& current_pos3D,
		const sibr::Camera& current_cam,
		sibr::RenderTargetRGBA32F& sampled_pos3D,
		const sibr::Camera& sampled_cam
	)
	{

		// Bind and clear RT.
		score.bind();
		glViewport(0, 0, score.w(), score.h());
		glClearColor(0, 0, 0, 1.0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		score_shader.begin();
		score_shader_sampled_proj.set(sampled_cam.viewproj());
		score_shader_sampled_pos.set(sampled_cam.position());
		score_shader_sampled_dir.set(sampled_cam.dir());
		score_shader_current_pos.set(current_cam.position());

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, current_pos3D.handle());
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, sampled_pos3D.handle());

		// Perform rendering.
		RenderUtility::renderScreenQuad();
		score_shader.end();
		score.unbind();
	}

	int PbnrScene::renderBestImprovementAtRT(
		sibr::RenderTargetLum32F& currentScore,
		sibr::RenderTargetLum32F& newScore,
		sibr::Texture2DArrayLum32F& individualScores,
		float &total_score,
		int criterion,
		std::vector<int> best_cams,
		int skip_id
	)
	{
		int num_cams = individualScores.depth();
		//inspired by https://www.lighthouse3d.com/tutorials/opengl-atomic-counters/

		// declare and generate a buffer object name
		GLuint atomicsBuffer;
		glGenBuffers(1, &atomicsBuffer);
		// bind the buffer and define its initial storage capacity
		glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, atomicsBuffer);
		glBufferData(GL_ATOMIC_COUNTER_BUFFER, sizeof(GLuint) * 2*num_cams, NULL, GL_DYNAMIC_DRAW);
		std::vector<GLuint> init_ac(2*num_cams, 0);
		glBufferSubData(GL_ATOMIC_COUNTER_BUFFER, 0, sizeof(GLuint) * 2 * num_cams, init_ac.data());
		// unbind the buffer 
		glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, 0);


		// Bind and clear RT.
		newScore.bind(); //We won't write here for now
		glViewport(0, 0, currentScore.w(), currentScore.h());
		glClearColor(0, 0, 0, 1.0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		compare_shader.begin();
		skid_id_uniform.set(skip_id);
		glBindBufferBase(GL_ATOMIC_COUNTER_BUFFER, 0, atomicsBuffer);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D_ARRAY, individualScores.handle());
		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, currentScore.handle());

		// Perform rendering.
		RenderUtility::renderScreenQuad();

		//////// Read back the counter
		GLuint* userCounters;
		glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, atomicsBuffer);
		// again we map the buffer to userCounters, but this time for read-only access
		userCounters = (GLuint*)glMapBufferRange(GL_ATOMIC_COUNTER_BUFFER,
			0,
			sizeof(GLuint) * 2*num_cams,
			GL_MAP_READ_BIT
		);
		//Get the id of the best improvement
		int bestImprId = -1;
		uint bestImpr = 0;
		if (criterion == 0) {
			for (int i = 0; i < num_cams; i++) {
				int camimprovement = userCounters[i * 2 + 0];
				if (camimprovement >= bestImpr)
				{
					bestImprId = i;
					bestImpr = camimprovement;
				}
			}
			total_score = bestImpr/1024.0;
		}
		else {

			for (int i = 0; i < num_cams; i++) {
				if (std::find(best_cams.begin(), best_cams.end(), i) != best_cams.end()) {
					continue;
				}
				int camabsolutemax = userCounters[i * 2 + 1];
				if (camabsolutemax >= bestImpr)
				{
					bestImprId = i;
					bestImpr = camabsolutemax;
				}
			}
			total_score = bestImpr / 1024.0;
		}
		glUnmapBuffer(GL_ATOMIC_COUNTER_BUFFER);
		compare_shader.end();

		//Now we compute the updated currentScore texture
		maxPool_shader.begin();

		newScore.bind(); //We won't write here for now
		glViewport(0, 0, currentScore.w(), currentScore.h());
		glClearColor(0, 0, 0, 1.0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D_ARRAY, individualScores.handle());
		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, currentScore.handle());
		maxPool_shader_bestId.set(bestImprId);

		// Perform rendering.
		RenderUtility::renderScreenQuad();

		maxPool_shader.end();
		newScore.unbind();

		//sibr::ImageL32F testMaxPool;
		//newScore.readBack(testMaxPool);
		//showFloat(testMaxPool,false,0.8,1.0);

		return bestImprId;
	}





	void PbnrScene::renderNormalDepthAtRT(const sibr::Mesh& mesh,
		const sibr::Camera& eye,
		sibr::RenderTargetRGBA32F& rt)
	{

		// Bind and clear RT.
		rt.bind();
		glViewport(0, 0, rt.w(), rt.h());
		glClearColor(0, 0, 0, -1.0);
		glClearDepth(1.0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		// Render the mesh from the current viewpoint.
		normal_depth_shader.begin();

		normal_depth_world2view.set(eye.view());
		normal_depth_proj.set(eye.proj());

		mesh.render();

		normal_depth_shader.end();
		rt.unbind();
	}

	float PbnrScene::getMaxDistanceCamera(const sibr::Camera& ref_cam) {
		float max_dist = 0.0;
		for (int i = 0; i < cameras()->inputCameras().size(); i++) {
			sibr::InputCamera::Ptr other_cam = cameras()->inputCameras()[i];
			float dist = (ref_cam.position() - other_cam->position()).norm();
			if (dist > max_dist) {
				max_dist = dist;
			}
		}
		return max_dist;
	}

	std::vector<int> PbnrScene::getNBestCoverageCameras(const sibr::Camera& ref_cam, Vector2u resolution, const int N,
		const float scale, const bool weighted, const int skip_id) {


		torch::Tensor single_one = torch::tensor({ 1.0 }).cuda();
		torch::Tensor single_zero = torch::tensor({ 0.0 }).cuda();

		int width = resolution[0] * scale;
		int height = resolution[1] * scale;
		int num_views = cameras()->inputCameras().size();


		torch::Tensor refCam_renderedTensor;
		get3DPositionAndDepthMap(ref_cam, width, height, refCam_renderedTensor);
		torch::Tensor refCam_renderedDepth = refCam_renderedTensor.slice(1, 3, 4).cuda();
		torch::Tensor positions3d = refCam_renderedTensor.view({ 4, height * width }).slice(0, 0, 3).permute({ 1, 0 }).cuda();

		torch::Tensor viewScoreMasks = torch::ones({ num_views, height, width, 1 }, torch::TensorOptions().device(torch::kCUDA, 0));

		for (int cam_idx = 0; cam_idx < num_views; cam_idx++) {
			const sibr::InputCamera::Ptr cam = cameras()->inputCameras()[cam_idx];
			if (!cam->isActive()) {
				continue;
			}

			torch::Tensor tarCam_renderedTensor;
			get3DPositionAndDepthMap(*cam, cam->w() * scale, cam->h() * scale, tarCam_renderedTensor);
			torch::Tensor tarCam_depthWC = tarCam_renderedTensor.slice(1, 3, 4).cuda();

			torch::Tensor cam_proj = getP3DProjMat(cam).cuda();
			torch::Tensor c = cam_proj[2][2];
			torch::Tensor d = cam_proj[3][2];
			torch::Tensor tarCam_depthNDC = (c * tarCam_depthWC + d) / tarCam_depthWC;

			torch::Tensor projT = getWorld2ProjMat(cam).cuda();
			torch::Tensor points_hom = torch::cat(torch::TensorList({ positions3d, torch::ones({ positions3d.sizes()[0], 1 }, torch::TensorOptions().device(torch::kCUDA, 0)) }), 1);
			torch::Tensor projected_points = torch::matmul(points_hom, projT);
			projected_points = projected_points.slice(1, 0, 3) / projected_points.slice(1, 3, 4);
			projected_points = projected_points.view({ height, width, 3 });

			/* Depth Test */
			torch::Tensor flow_field = projected_points.unsqueeze(0).slice(3, 0, 2);

			torch::Tensor refcam_depth_from_tarcam_WC = torch::nn::functional::grid_sample(tarCam_depthWC, flow_field,
				torch::nn::functional::GridSampleFuncOptions().mode(torch::kBilinear).padding_mode(torch::kBorder).align_corners(false));
			torch::Tensor refcam_depth_from_tarcam_NDC = torch::nn::functional::grid_sample(tarCam_depthNDC, flow_field,
				torch::nn::functional::GridSampleFuncOptions().mode(torch::kBilinear).padding_mode(torch::kBorder).align_corners(false));

			/* Compute score */
			if (weighted) {
				torch::Tensor test = refCam_renderedDepth.permute({ 0, 2,3,1 }).squeeze() / refcam_depth_from_tarcam_WC.squeeze();
				test.clamp_(0.0, 1.25);
				viewScoreMasks.index_put_({ cam_idx }, test.unsqueeze(2));
			}
			else {
				viewScoreMasks.index_put_({ cam_idx }, single_one);
			}

			/* Depth Test */
			torch::Tensor depthTestRejections = torch::abs(projected_points.slice(2, 2, 3).squeeze() - refcam_depth_from_tarcam_NDC.squeeze()) > 0.10;
			viewScoreMasks.index({ cam_idx }).index_put_({ (depthTestRejections).squeeze() }, single_zero);

			/* Pixel Out Of Camera/Image Test*/
			torch::Tensor x_bool = (projected_points.slice(2, 0, 1) > 1.0).logical_or((projected_points.slice(2, 0, 1) < -1.0));
			torch::Tensor y_bool = (projected_points.slice(2, 1, 2) > 1.0).logical_or((projected_points.slice(2, 1, 2) < -1.0));
			torch::Tensor z_bool = (projected_points.slice(2, 2, 3) > 1.0).logical_or((projected_points.slice(2, 2, 3) < -1.0));

			viewScoreMasks.index({ cam_idx }).index_put_({ x_bool.logical_or(y_bool).logical_or(z_bool) }, single_zero);

			/* No Geometry Test */
			torch::Tensor noGeometryRejections = positions3d.view({ height, width, 3 }).slice(2, 0, 1) == INFINITY;
			viewScoreMasks.index({ cam_idx }).index_put_({noGeometryRejections}, single_zero);

		}
		/*for (int cam_idx = 0; cam_idx < num_views; cam_idx++) {
			ImageRGB32F fake_image;
			fake_image = tensorToIm(torch::cat({ viewScoreMasks.index({ cam_idx }).unsqueeze(0),
				viewScoreMasks.index({ cam_idx }).unsqueeze(0), viewScoreMasks.index({ cam_idx }).unsqueeze(0) }, 3).permute({0, 3, 1, 2}));
			std::stringstream stream;
			stream << "F:/score_mask_" << cam_idx <<  "_sibr.exr";
			cv::imwrite(stream.str(), fake_image.toOpenCV());

		}*/
		std::vector<int> best_cams;
		torch::Tensor cam_selection = torch::tensor({}).cuda();
		float prev_max = 0.0;
		for (int i = 0; i < N; i++) {
			float max = 0.0;
			int cam_choice = -1;
			for (int cam_idx = 0; cam_idx < num_views; cam_idx++) {
				if (std::find(best_cams.begin(), best_cams.end(), cam_idx) != best_cams.end() ||
					cam_idx == skip_id || !cameras()->inputCameras()[cam_idx]->isActive()) {
					continue;
				}
				torch::Tensor tmp_selection = torch::cat(torch::TensorList({ cam_selection, viewScoreMasks.index({ cam_idx }) }), 2);
				tmp_selection = torch::amax(tmp_selection, 2);
				torch::Tensor score = torch::sum(tmp_selection).cpu();
				if (score.data<float>()[0] > max) {
					max = score.data<float>()[0];
					cam_choice = cam_idx;
				}
			}

			/*
			if (i == 0) {
				std::cout << max << " ";
			}
			else {
				std::cout << max / prev_max << " ";
			}*/

			// If the new image is not a 1% improvement over the previous one then skip-out and use most visible pixels.
			if (max - prev_max < 0.01 * prev_max) {
				float vis_max = 0.0;
				for (int cam_idx = 0; cam_idx < num_views; cam_idx++) {
					if (std::find(best_cams.begin(), best_cams.end(), cam_idx) != best_cams.end() ||
						cam_idx == skip_id || !cameras()->inputCameras()[cam_idx]->isActive()) {
						continue;
					}
					torch::Tensor visibility_score = torch::sum(viewScoreMasks.index({ cam_idx })).cpu();
					float s = visibility_score.data<float>()[0];
					if (s > vis_max) {
						vis_max = s;
						cam_choice = cam_idx;
					}
				}
			}

			prev_max = max;
			cam_selection = torch::cat(torch::TensorList({ cam_selection, viewScoreMasks.index({ cam_choice }) }), 2);
			best_cams.push_back(cam_choice);
		}

		//std::cout << std::endl;

		//std::cout << best_cams << std::endl;
		return best_cams;
	}

	std::vector<int> PbnrScene::getNBestCoverageCamerasGPU(const sibr::Camera& ref_cam, Vector2u resolution, const int N,
		const float scale, const bool weighted, const int skip_id) {

		int width = resolution[0]*scale;
		int height = resolution[1]*scale;
		int num_views = cameras()->inputCameras().size();

		sibr::RenderTargetRGBA32F ref_pos3DRT(width, height);
		renderDepthAtRT(proxies()->proxy(), ref_cam, ref_pos3DRT);

		std::vector<sibr::ImageRGBA32F> pos3D;
		sibr::Texture2DArrayRGBA32F::Ptr Textures3D = std::make_shared<sibr::Texture2DArrayRGBA32F>();

		std::vector<sibr::RenderTargetLum32F::Ptr> score_rts;
		for (int cam_idx = 0; cam_idx < num_views; cam_idx++) {

			//3D pos map of the input camera
			const sibr::InputCamera::Ptr cam = cameras()->inputCameras()[cam_idx];
			//Score for current view and current camera;
			sibr::RenderTargetLum32F::Ptr score(new sibr::RenderTargetLum32F(width, height));

			renderScoreAtRT(*score, ref_pos3DRT, ref_cam, *pos3DTextures[cam_idx], *cam);
			/*sibr::ImageL32F scoreImage(width, height);
			score->readBack(scoreImage);
			std::stringstream stream;
			stream << "F:/score_mask_" << cam_idx << "_gpu.exr";
			cv::imwrite(stream.str(), scoreImage.toOpenCV());
			float sum = 0;
			for (int i = 0; i < width; i++)
				for (int j = 0; j < height; j++) {
					sum += scoreImage(i, j).x();
				}
			std::cout << "Improvement of camera cpu " << cam_idx << ": " << sum << std::endl;
			showFloat(scoreImage);*/
			//pos3D.push_back(renderedImage.clone());
			score_rts.push_back(score);

		}

		sibr::Texture2DArrayLum32F scores;
		scores.createFromRTs(score_rts);

		int criterion = 0;
		float total_score = 0;
		std::vector<int> best_cams;
		sibr::RenderTargetLum32F::Ptr currentScoreTex(new sibr::RenderTargetLum32F(width, height));
		for (int c = 0; c < N; c++) {
			sibr::RenderTargetLum32F::Ptr newScoreTex(new sibr::RenderTargetLum32F(width, height));
			float prev_score = total_score;
			int impr_cam = renderBestImprovementAtRT(*currentScoreTex, *newScoreTex, scores, total_score, criterion, best_cams, skip_id);
			if (total_score - prev_score < 0.01 * prev_score) {
				criterion = 1;
			}
			best_cams.push_back(impr_cam);
			//std::cout << "impr_cam " << impr_cam << std::endl;
			currentScoreTex = newScoreTex;
		}

		return best_cams;
	}




}