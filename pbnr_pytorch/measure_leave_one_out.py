from pathlib import Path
import os
from PIL import Image
import torch
import torchvision.transforms.functional as tf
import pytorch_ssim
from lpips_pytorch import lpips
import json

def mse(img1, img2):
    return (((img1 - img2)) ** 2).view(img1.shape[0], -1).mean(1, keepdim=True)

def psnr(img1, img2):
    mse = (((img1 - img2)) ** 2).view(img1.shape[0], -1).mean(1, keepdim=True)
    return 20 * torch.log10(1.0 / torch.sqrt(mse))

device = torch.device("cuda:0")
torch.cuda.set_device(device)


scenes = [Path(r"F:\results\results_db\truck\leave_one_out")]

full_dict = {}
for scene in scenes:
    scene_name = str(scene).split("\\")[3]
    print(scene_name)
    full_dict[scene_name] = {}
    for algorithm in os.listdir(scene):
        print(algorithm)
        if algorithm == "masks":
            continue
        full_dict[scene_name][algorithm] = {}
        gt_dir = scene/algorithm/"gt"
        renders_dir = scene/algorithm/"renders"

        renders = []
        gts = []

        for fname in os.listdir(renders_dir):
            render = Image.open(renders_dir/fname)
            gt = Image.open(gt_dir/fname)
            renders.append(tf.to_tensor(render).unsqueeze(0).to(device))
            gts.append(tf.to_tensor(gt).unsqueeze(0).to(device))

        ssims = []
        psnrs = []
        lpipss = []

        for idx in range(len(renders)):
            print(idx)
            ssims.append(pytorch_ssim.ssim(renders[idx], gts[idx]))
            psnrs.append(psnr(renders[idx], gts[idx]))
            lpipss.append(lpips(renders[idx], gts[idx], net_type='vgg'))

        print("SSIM: {}".format(torch.tensor(ssims).mean()))
        print("PSNR: {}".format(torch.tensor(psnrs).mean()))
        print("LPIPS: {}".format(torch.tensor(lpipss).mean()))
        full_dict[scene_name][algorithm].update({"SSIM": torch.tensor(ssims).mean().item(),
                                                 "PSNR": torch.tensor(psnrs).mean().item(),
                                                 "LPIPS": torch.tensor(lpipss).mean().item()})

with open('F:/results/results_db/leave_one_out_ablation_data.json', 'w') as fp:
    json.dump(full_dict, fp, indent=True)

