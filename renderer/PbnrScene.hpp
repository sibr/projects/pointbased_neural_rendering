/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */

#pragma once

#include "Config.hpp"
#include <core/scene/BasicIBRScene.hpp>
#include "torch/torch.h"

namespace sibr
{
	class SIBR_EXP_PBNR_EXPORT PbnrScene : public BasicIBRScene
	{
	public:
		SIBR_CLASS_PTR(PbnrScene);

		PbnrScene();
		PbnrScene(const PbnrAppArgs& myArgs, bool preprocess = false);

	public:
		float getMaxDistanceCamera(const sibr::Camera& ref_cam);
		void getNormalAndDepthMap(const sibr::Camera& ref_cam, const sibr::Mesh& mesh,
			const int w, const int h,
			torch::Tensor& out_renderedTensor);
		void get3DPositionAndDepthMap(const sibr::Camera& ref_cam, const int w, const int h,
			torch::Tensor& out_renderedTensor);

		std::vector<int> getNBestCoverageCameras(const sibr::Camera& ref_cam, Vector2u resolution,
			const int N, const float scale, const bool weighted, const int skip_id);

		std::vector<int> getNBestCoverageCamerasGPU(const sibr::Camera& ref_cam, Vector2u resolution,
			const int N, const float scale, const bool weighted, const int skip_id);

		std::vector<std::tuple<int, int>> getNBestCoverageCameras_2(const sibr::Camera& ref_cam, Vector2u resolution,
			const int N, const float scale, const bool weighted, const int skip_id);

	protected:
		std::tuple < torch::Tensor, torch::Tensor >
			get3DPointCloudFromCamera(static sibr::Camera::Ptr source_cam,
				static torch::Tensor color,
				static torch::Tensor depth,
				static torch::Tensor pixelsNDC,
				static torch::Tensor filter);
		torch::Tensor readTensorFromDisk(std::string path);

		void readDepthMaps(std::string depth_map_path, std::string depth_delta_path);
		void readNormalMaps(std::string normal_maps_path);
		void readImages(std::string images_path);
		void readFeatureMaps(std::string features_path);
		void readUncertainties(std::string uncertainties_path);
		void readExpCoef(std::string exp_coef_path);

		void renderPixelPositionsNDC(void);
		float getNonZeroMin(const torch::Tensor t);

		void renderNormalDepthAtRT(const sibr::Mesh& mesh,
			const sibr::Camera& eye,
			sibr::RenderTargetRGBA32F& rt);

		void renderDepthAtRT(const sibr::Mesh& mesh,
			const sibr::Camera& eye,
			sibr::RenderTargetRGBA32F& rt);

		void renderScoreAtRT(sibr::RenderTargetLum32F& score,
			sibr::RenderTargetRGBA32F& current_pos3D,
			const sibr::Camera& current_cam,
			sibr::RenderTargetRGBA32F& sampled_pos3D,
			const sibr::Camera& sampled_cam);

		int renderBestImprovementAtRT(sibr::RenderTargetLum32F& currentScore,
			sibr::RenderTargetLum32F& dropOutput,
			sibr::Texture2DArrayLum32F& individualScores,
			float &total_score,
			int criterion,
			std::vector<int> best_cams,
			int skip_id);

		sibr::GLShader position_depth_shader;
		GLuniform<Matrix4f> position_depth_proj;
		GLuniform<Matrix4f> position_depth_world2view;

		sibr::GLShader normal_depth_shader;
		GLuniform<Matrix4f> normal_depth_proj;
		GLuniform<Matrix4f> normal_depth_world2view;

		//Camera selection data
		sibr::GLShader score_shader;
		GLuniform<Vector3f> score_shader_current_pos;
		GLuniform<Matrix4f> score_shader_sampled_proj;
		GLuniform<Vector3f> score_shader_sampled_pos;
		GLuniform<Vector3f> score_shader_sampled_dir;

		sibr::GLShader compare_shader;
		GLuniform<int> skid_id_uniform;

		sibr::GLShader maxPool_shader;
		sibr::GLuniform<int> maxPool_shader_bestId;
		std::vector<sibr::RenderTargetRGBA32F::Ptr> pos3DTextures;

	public:
		std::vector <std::tuple <float, int>> _cam_weights_idx;
		std::vector <torch::Tensor> _images;
		std::vector <torch::Tensor> _depths;
		std::vector <torch::Tensor> _features;
		std::vector <torch::Tensor> _uncertainties;
		std::vector <torch::Tensor> _exp_coefs;
		std::vector <torch::Tensor> _normals;
		std::vector <torch::Tensor> _pixelsNDC;
		std::vector < std::tuple < torch::Tensor, torch::Tensor >> _points3d_color_tuple;
		torch::Tensor exp_coef_mean;
	};

}