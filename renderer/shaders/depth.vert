#version 420

uniform mat4 mvp;
uniform mat4 view2proj;

layout(location = 0) in vec3 in_vertex;

out vec3 vertex_coord;
out mat4 prj_mat;

void main(void) {
	gl_Position = mvp * vec4(in_vertex, 1.0);
	prj_mat = view2proj;
    vertex_coord  = in_vertex;
}
