/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */

#include <math.h>
#include <torch/torch.h>
#include <cstdio>
#include <sstream>
#include <iostream>
#include <tuple>
#include <stdio.h>
#include "rasterization_utils.cuh"



namespace {
    // A little structure for holding details about a pixel.
    struct Pix {
        float z; // Depth of the reference point.
        int32_t idx; // Index of the reference point.
        float dist2; // Euclidean distance square to the reference point.
        float alpha; // Alpha blending weight
    };

    __device__ inline bool operator<(const Pix& a, const Pix& b) {
        return a.z < b.z;

    }

    struct Bin {
        float z; // Depth of the reference point.
        float color; // Index of the reference point.
        float alpha; // Alpha blending weight
        int n_points;
    };
}

// ****************************************************************************
// *                          Pbnr RASTERIZATION                             *
// ****************************************************************************

__global__ void OrderPointsPbnrCudaKernel(
    int32_t* point_idxs, // (N, H, W, K)
    uint32_t* k_idxs,
    const float* points,
    int H,
    int W,
    int K)
{
    // Simple version: One thread per output pixel
    const int num_threads = gridDim.x * blockDim.x;
    const int tid = blockDim.x * blockIdx.x + threadIdx.x;
    for (int i = tid; i < H * W; i += num_threads) {
        // Convert linear index to 3D index
        const int pix_idx = i % (H * W);

        const int yi = pix_idx / W;
        const int xi = pix_idx % W;

        int idx = 0 * H * W * K + yi * W * K + xi * K + 0;
        int32_t q[kMaxPointsPerPixel];
        int k = min(k_idxs[yi * W + xi], K);
        for (int i = 0; i < k; i++) {
            q[i] = point_idxs[idx + i];
        }
        BubbleSort2(q, points, k);
        for (int i = 0; i < k; i++) {
            point_idxs[idx + i] = q[i];
        }
    }
}


__global__ void BlendPoints_adaptivebinning_PbnrCudaKernel(
    const float* points, // (P, 3)
    int32_t* point_idx, // (N, H, W, K)
    const float* colors, // (P, C)
    const int32_t* k_idxs, // (N, H, W)
    const float* inv_cov, // (P, 4)
    const int max_radius,
    const float gamma,
    const int N,
    const int H,
    const int W,
    const int C,
    const int K,
    const float zfar,
    const float znear,
    float* color, // (N, 3, H, W)
    float* mask, // (N, 1, H, W)
    float* depth_gmms, // (N, 1, H, W, kMaxPointPerPixelLocal, 2)
    int* num_gmms)

{
    const int radius2 = max_radius * max_radius;
    // One thread per output pixel
    const int num_threads = gridDim.x * blockDim.x;
    const int tid = blockDim.x * blockIdx.x + threadIdx.x;
    for (int i = tid; i < H * W; i += num_threads) {
        // Convert linear index to 3D index
        const int pix_idx = i % (H * W);

        const int yi = pix_idx / W;
        const int xi = pix_idx % W;

        Bin adaptive_bins[10];
        int next_empty_bin = 0;

        int y_start = yi - max_radius;
        int y_finish = yi + max_radius;
        int x_start = xi - max_radius;
        int x_finish = xi + max_radius;

        int gathered_points_idx = 0;
        int gathered_points_idx_max = -1;
        float gathered_points_z_max = -1000000.0;
        for (int y_idx = y_start; y_idx < y_finish + 1; y_idx++) {
            for (int x_idx = x_start; x_idx < x_finish + 1; x_idx++) {
                if (y_idx < 0 || y_idx > H - 1 || x_idx < 0 || x_idx > W - 1)
                    continue;
                int k = k_idxs[y_idx * W + x_idx];
                int idx = 0 * H * W * K + y_idx * W * K + x_idx * K + 0;
                for (int i = 0; i < k; i++) {
                    int p_idx = point_idx[idx + i];
                    float px_ndc = points[p_idx * 3 + 0];
                    float py_ndc = points[p_idx * 3 + 1];
                    float pz = points[p_idx * 3 + 2];
                    if (pz < 0)
                        // Don't render points behind the camera.
                        continue;
                    float dx = NdcToPix(px_ndc, W) - xi;
                    float dy = NdcToPix(py_ndc, H) - yi;
                    float dist2 = dx * dx + dy * dy;
                    // Trim it to a circle
                    if (dist2 > radius2)
                        continue;

                    float inv_cov00 = inv_cov[p_idx * 4 + 0];
                    float inv_cov01 = inv_cov[p_idx * 4 + 1];
                    float inv_cov10 = inv_cov[p_idx * 4 + 2];
                    float inv_cov11 = inv_cov[p_idx * 4 + 3];

                    float power = (-1.0 / 2.0) * (inv_cov00 * dx * dx + (inv_cov01 + inv_cov10) * dx * dy + inv_cov11 * dy * dy);
                    if (power > 0.0) {
                        //printf("%f || %f %f %f %f?\n", power, inv_cov00, inv_cov01, inv_cov10, inv_cov11);
                        continue;
                    }
                    float g_w = exp(power);
                    float alpha = pow(g_w, gamma);
                    if (alpha < 1 / 255.0)
                        continue;

                    // If more than kMaxPointPerPixelLocal we need to compare against the max z
                    // if we are closer we replace our selves and search for the max again
                    for (int i = 0; i < 10; i++) {
                        if (i == next_empty_bin) {
                            adaptive_bins[i] = { pz, colors[p_idx], alpha, 1 };
                            next_empty_bin++;
                        }
                        //abs(b.depth - p_i.depth) < sigma: # This means that we should merge with the current bucket
                        else if (abs(adaptive_bins[i].z - pz) < 0.5) {
                            float alpha_fg, alpha_bg, color_fg, color_bg;
                            if (adaptive_bins[i].z < pz) {
                                alpha_fg = adaptive_bins[i].alpha;
                                color_fg = adaptive_bins[i].color;
                                alpha_bg = alpha;
                                color_bg = colors[p_idx];
                            }
                            else {
                                alpha_fg = alpha;
                                color_fg = colors[p_idx];
                                alpha_bg = adaptive_bins[i].alpha;
                                color_bg = adaptive_bins[i].color;
                            }
                            adaptive_bins[i].color = color_fg + (1 - alpha_fg) * color_bg;
                            adaptive_bins[i].alpha = alpha_fg + alpha_bg * (1 - alpha_fg);
                            adaptive_bins[i].z += pz;
                            adaptive_bins[i].n_points++;
                        }
                        else if (abs(adaptive_bins[i].z - pz) < 0.5) {
                            for (int j = next_empty_bin; j < i; j--) {
                                if (j > 10 - 1) continue;
                                adaptive_bins[j] = adaptive_bins[j - 1];
                            }
                            adaptive_bins[i] = { pz, colors[p_idx], alpha, 1 };
                            next_empty_bin = min(next_empty_bin + 1, 10);
                        }
                    }
                }
            }
        }

        float cum_alpha = 1.0;
        /* TODO: Adding iteratively to global memory can be slow, but dynamic allocation doesnt work. */
        /* TODO: Hard-Code number of features to see perfomance gain */
        //float result[3] = {0.0, 0.0, 0.0};
        //float* result = new float[C]();
        //float *result = (float *)malloc(3*sizeof(float));
        int k;
        for (k = 0; k < next_empty_bin; k++) {
            float alpha = adaptive_bins[k].alpha;
            float weight = cum_alpha * alpha;
            for (int ch = 0; ch < C; ch++) {
                color[ch * H * W + yi * W + xi] += adaptive_bins[k].color * weight;
            }

            depth_gmms[yi * W * (kMaxPointsPerPixel + 1) * 2 + xi * (kMaxPointsPerPixel + 1) * 2 + k * 2 + 0] = adaptive_bins[k].z / adaptive_bins[k].n_points;
            depth_gmms[yi * W * (kMaxPointsPerPixel + 1) * 2 + xi * (kMaxPointsPerPixel + 1) * 2 + k * 2 + 1] = weight;

            cum_alpha = cum_alpha * (1 - alpha);
            if (cum_alpha < 0.001) {
                k = k + 1;
                break;
            }
        }
        depth_gmms[yi * W * (kMaxPointsPerPixel + 1) * 2 + xi * (kMaxPointsPerPixel + 1) * 2 + k * 2 + 0] = 100000.0;
        depth_gmms[yi * W * (kMaxPointsPerPixel + 1) * 2 + xi * (kMaxPointsPerPixel + 1) * 2 + k * 2 + 1] = cum_alpha;
        num_gmms[yi * W + xi] = k + 1;
        mask[yi * W + xi] = 1.0 - cum_alpha;

    }
}


__global__ void BlendPointsPbnrCudaKernel(
    const float* points, // (P, 3)
    int32_t* point_idx, // (N, H, W, K)
    const float* colors, // (P, C)
    const int32_t* k_idxs, // (N, H, W)
    const float* inv_cov, // (P, 4)
    const int max_radius,
    const float gamma,
    const int N,
    const int H,
    const int W,
    const int C,
    const int K,
    const float zfar,
    const float znear,
    float* color, // (N, 3, H, W)
    float* mask, // (N, 1, H, W)
    float* depth_gmms, // (N, 1, H, W, kMaxPointPerPixelLocal, 2)
    int* num_gmms)

{
    const int radius2 = max_radius * max_radius;
    // One thread per output pixel
    const int num_threads = gridDim.x * blockDim.x;
    const int tid = blockDim.x * blockIdx.x + threadIdx.x;
    for (int i = tid; i < H * W; i += num_threads) {
        // Convert linear index to 3D index
        const int pix_idx = i % (H * W);

        const int yi = pix_idx / W;
        const int xi = pix_idx % W;

        Pix gathered_points[kMaxPointPerPixelLocal - 1];
        int y_start = yi - max_radius;
        int y_finish = yi + max_radius;
        int x_start = xi - max_radius;
        int x_finish = xi + max_radius;

        int gathered_points_idx = 0;
        int gathered_points_idx_max = -1;
        float gathered_points_z_max = -1000000.0;
        for (int y_idx = y_start; y_idx < y_finish + 1; y_idx++) {
            for (int x_idx = x_start; x_idx < x_finish + 1; x_idx++) {
                if (y_idx < 0 || y_idx > H - 1 || x_idx < 0 || x_idx > W - 1)
                    continue;
                int k = k_idxs[y_idx * W + x_idx];
                int idx = 0 * H * W * K + y_idx * W * K + x_idx * K + 0;
                for (int i = 0; i < k; i++) {
                    int p_idx = point_idx[idx + i];
                    float px_ndc = points[p_idx * 3 + 0];
                    float py_ndc = points[p_idx * 3 + 1];
                    float pz = points[p_idx * 3 + 2];
                    if (pz < 0)
                        // Don't render points behind the camera.
                        continue;
                    float dx = NdcToPix(px_ndc, W) - xi;
                    float dy = NdcToPix(py_ndc, H) - yi;
                    float dist2 = dx * dx + dy * dy;
                    // Trim it to a circle
                    if (dist2 > radius2)
                        continue;

                    float inv_cov00 = inv_cov[p_idx * 4 + 0];
                    float inv_cov01 = inv_cov[p_idx * 4 + 1];
                    float inv_cov10 = inv_cov[p_idx * 4 + 2];
                    float inv_cov11 = inv_cov[p_idx * 4 + 3];

                    float power = (-1.0 / 2.0) * (inv_cov00 * dx * dx + (inv_cov01 + inv_cov10) * dx * dy + inv_cov11 * dy * dy);
                    if (power > 0.0) {
                        //printf("%f || %f %f %f %f?\n", power, inv_cov00, inv_cov01, inv_cov10, inv_cov11);
                        continue;
                    }
                    float g_w = exp(power);
                    float alpha = pow(g_w, gamma);
                    if (alpha < 1 / 255.0)
                        continue;

                    // If more than kMaxPointPerPixelLocal we need to compare against the max z
                    // if we are closer we replace our selves and search for the max again
                    if (gathered_points_idx > kMaxPointPerPixelLocal - 2) {
                        if (pz < gathered_points_z_max) {
                            gathered_points[gathered_points_idx_max].idx = p_idx;
                            gathered_points[gathered_points_idx_max].dist2 = dist2;
                            gathered_points[gathered_points_idx_max].alpha = alpha;
                            gathered_points[gathered_points_idx_max].z = pz;

                            gathered_points_z_max = -1.0;
                            for (int j = 0; j < gathered_points_idx; j++) {
                                if (gathered_points[j].z > gathered_points_z_max) {
                                    gathered_points_idx_max = j;
                                    gathered_points_z_max = gathered_points[j].z;
                                }
                            }
                        }
                    }
                    else {
                        if (pz > gathered_points_z_max) {
                            gathered_points_idx_max = i;
                            gathered_points_z_max = pz;
                        }
                        gathered_points[gathered_points_idx].idx = p_idx;
                        gathered_points[gathered_points_idx_max].dist2 = dist2;
                        gathered_points[gathered_points_idx].alpha = alpha;
                        gathered_points[gathered_points_idx].z = pz;
                        gathered_points_idx++;
                    }
                }
            }
        }
        BubbleSort(gathered_points, gathered_points_idx);

        float cum_alpha = 1.0;
        /* TODO: Adding iteratively to global memory can be slow, but dynamic allocation doesnt work. */
        /* TODO: Hard-Code number of features to see perfomance gain */
        //float result[3] = {0.0, 0.0, 0.0};
        //float* result = new float[C]();
        //float *result = (float *)malloc(3*sizeof(float));
        int k;
        for (k = 0; k < gathered_points_idx; k++) {
            float alpha = gathered_points[k].alpha;
            float weight = cum_alpha * alpha;
            for (int ch = 0; ch < C; ch++) {
                color[ch * H * W + yi * W + xi] += colors[gathered_points[k].idx * C + ch] * weight;
            }

            depth_gmms[yi * W * (kMaxPointsPerPixel + 1) * 2 + xi * (kMaxPointsPerPixel + 1) * 2 + k * 2 + 0] = gathered_points[k].z;
            depth_gmms[yi * W * (kMaxPointsPerPixel + 1) * 2 + xi * (kMaxPointsPerPixel + 1) * 2 + k * 2 + 1] = weight;

            cum_alpha = cum_alpha * (1 - alpha);
            if (cum_alpha < 0.001) {
                k = k + 1;
                break;
            }
        }
        depth_gmms[yi * W * (kMaxPointsPerPixel + 1) * 2 + xi * (kMaxPointsPerPixel + 1) * 2 + k * 2 + 0] = 100000.0;
        depth_gmms[yi * W * (kMaxPointsPerPixel + 1) * 2 + xi * (kMaxPointsPerPixel + 1) * 2 + k * 2 + 1] = cum_alpha;
        num_gmms[yi * W + xi] = k + 1;
        mask[yi * W + xi] = 1.0 - cum_alpha;

    }
}

__global__ void RasterizePointsPbnrCudaKernel(
    const float* points, // (P, 3)
    const int P,
    uint32_t* k_idxs, // (N, H, W)
    const int N,
    const int H,
    const int W,
    const int K,
    int32_t* point_idxs) // (N, H, W, K)
{
    // Simple version: One thread per output pixel
    const int num_threads = gridDim.x * blockDim.x;
    const int tid = blockDim.x * blockIdx.x + threadIdx.x;
    // TODO Pbnropanas more than 1 batches?
    for (int i = tid; i < P; i += num_threads) {
        const float px_ndc = points[i * 3 + 0];
        const float py_ndc = points[i * 3 + 1];

        const float px = NdcToPix(px_ndc, W);
        const float py = NdcToPix(py_ndc, H);

        const int px_rounded = int(px + 0.5);
        const int py_rounded = int(py + 0.5);
        if (py_rounded < 0 || py_rounded > H - 1 || px_rounded < 0 || px_rounded > W - 1)
            continue;

        int k_idx = atomicInc(&(k_idxs[0 * H * W + py_rounded * W + px_rounded]), K + 1);
        if (k_idx == K) {
            printf("Pixel y:%d x:%d exceeded point projection limit\n", py_rounded, px_rounded);
            //assert(0);
        }

        int idx = 0 * H * W * K + py_rounded * W * K + px_rounded * K + k_idx;
        point_idxs[idx] = i;
    }
}

std::tuple<torch::Tensor, torch::Tensor, torch::Tensor, torch::Tensor, torch::Tensor, torch::Tensor>
RasterizePointsPbnrCuda(
    const torch::Tensor& points, // (P, 3)
    const torch::Tensor& colors, // (P, C)
    const torch::Tensor& inv_cov, // (P, 4)
    const int max_radius,
    const int image_height,
    const int image_width,
    const int points_per_pixel,
    const float zfar,
    const float znear,
    const float gamma) {

    if (points.ndimension() != 2 || points.size(1) != 3) {
        AT_ERROR("points must have dimensions (num_points, 3)");
    }

    const int P = points.size(0);
    const int C = colors.size(1);
    const int N = 1; // batch size hard-coded
    const int H = image_height;
    const int W = image_width;
    const int K = points_per_pixel;

    if (K > kMaxPointsPerPixel) {
        std::stringstream ss;
        ss << "Must have points_per_pixel <= " << kMaxPointsPerPixel;
        AT_ERROR(ss.str());
    }

    auto int_opts = points.options().dtype(torch::kInt32);
    auto float_opts = points.options().dtype(torch::kFloat32);
    torch::Tensor point_idxs = torch::full({ N, H, W, kMaxPointsPerPixel }, -1, int_opts);
    torch::Tensor k_idxs = torch::full({ N, H, W }, 0, int_opts);
    torch::Tensor out_color = torch::full({ N, C, H, W }, 0.0, float_opts);
    torch::Tensor depth_gmms = torch::full({ N, H, W, kMaxPointsPerPixel + 1, 2 }, -1.0, float_opts);
    torch::Tensor num_gmms = torch::full({ N, H, W, 1 }, -1, int_opts);
    torch::Tensor mask = torch::full({ N, 1, H, W }, 0.0, float_opts);

    const size_t blocks = 1024;
    const size_t threads = 64;

    RasterizePointsPbnrCudaKernel << <blocks, threads >> > (
        points.contiguous().data<float>(),
        P,
        (unsigned int*)k_idxs.data<int32_t>(),
        N,
        H,
        W,
        kMaxPointsPerPixel,
        point_idxs.contiguous().data<int32_t>());

    cudaDeviceSynchronize();

    BlendPointsPbnrCudaKernel << <blocks, threads >> > (
        points.contiguous().data<float>(),
        point_idxs.contiguous().data<int32_t>(),
        colors.contiguous().data<float>(),
        k_idxs.data<int32_t>(),
        inv_cov.data<float>(),
        max_radius,
        gamma,
        N,
        H,
        W,
        C,
        kMaxPointsPerPixel,
        zfar,
        znear,
        out_color.contiguous().data<float>(),
        mask.contiguous().data<float>(),
        depth_gmms.contiguous().data<float>(),
        num_gmms.contiguous().data<int32_t>());

    //point_idxs = point_idxs.narrow(-1, 0, K);

    return std::make_tuple(point_idxs, out_color, k_idxs, depth_gmms, num_gmms, mask);
}
