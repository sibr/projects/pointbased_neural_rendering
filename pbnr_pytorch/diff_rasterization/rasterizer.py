from typing import NamedTuple
import torch.nn as nn

from .rasterize_points import rasterize_points

class PointsRasterizationSettings(NamedTuple):
    image_height: int = 256
    image_width: int =256
    points_per_pixel: int = 8
    zfar: float = None
    znear: float = None
    gamma: float = None


class PointsRasterizer(nn.Module):
    def __init__(self, raster_settings):
        super().__init__()
        self.raster_settings = raster_settings


    def forward(self, points_screen, features, point_score, inv_cov, max_radius):
        raster_settings = self.raster_settings
        idx, color, depth_gmms, num_gmms, mask = rasterize_points(
            points_screen,
            features,
            point_score,
            inv_cov,
            max_radius,
            image_height=raster_settings.image_height,
            image_width=raster_settings.image_width,
            points_per_pixel=raster_settings.points_per_pixel,
            zfar=raster_settings.zfar,
            znear=raster_settings.znear,
            gamma=raster_settings.gamma
        )
        return color, depth_gmms, num_gmms, mask
