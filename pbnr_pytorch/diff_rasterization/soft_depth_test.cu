#include <stdio.h>
#include <torch/extension.h>
#include "rasterization_utils.cuh"

__device__ inline float H_step(float d0, float d1) {
	if (d0 < d1)
		return 0;
	else if (d0 > d1)
		return 1;
	return 0.5;
}

__device__ inline float H_tri(float d0, float d1) {
	float sigma = 7.0;
	if (d0 < d1 - sigma)
		return 0;
	else if (d0 > d1 + sigma)
		return 1;
	else if (d0 < d1) {
		return (d0 - d1 + sigma) * (d0 - d1 + sigma) / (2 * sigma * sigma);
	}
	else if (d0 > d1) {
		return 1.0 - (d1 + sigma - d0) * (d1 + sigma - d0) / (2 * sigma * sigma);
	}
	return 0.5;
}


__device__ float recurseProd(int start_v, int current_v,
	const float* depth_gmms, // (N, H, W, 150, 2)
	const int* num_gmms,
	float d,
	const int N,
	const int H,
	const int W,
	const int yi,
	const int xi
) {

	if (start_v == current_v)
		return 1.0;

	const int id_NHW = current_v * H * W + yi * W + xi;
	const int v_point_num = num_gmms[id_NHW];

	float inner_sum = 0;
	float lower_prod = recurseProd(start_v, (current_v + 1) % N, depth_gmms, num_gmms, d, N, H, W, yi, xi);

	for (int k = v_point_num - 1; k >= 0; k--) {
		const int id_NHWP2 =
			current_v * H * W * (kMaxPointsPerPixel + 1) * 2 +
			yi * W * (kMaxPointsPerPixel + 1) * 2 +
			xi * (kMaxPointsPerPixel + 1) * 2 +
			k * 2;

		float d_new = depth_gmms[id_NHWP2];
		float h = H_tri(d_new, d);

		if (h == 0) {
			break;//We start from high depth back to front, if this h is zero, all the subsequent will also be;
		}
		else {

			float alpha_new = depth_gmms[id_NHWP2 + 1];

			inner_sum += alpha_new * h;

		}
	}

	return inner_sum * lower_prod;


}

__device__ float linProd(int start_v,
	const float* depth_gmms, // (N, H, W, 150, 2)
	const int* num_gmms,
	float d,
	const int N,
	const int H,
	const int W,
	const int yi,
	const int xi
) {


	float prod = 1.0;

	for (int v_it = 1; v_it < N; v_it++) {
		const int current_v = (start_v + v_it) % N;
		const int id_NHW = current_v * H * W + yi * W + xi;
		const int v_point_num = num_gmms[id_NHW];

		float inner_sum = 0;
		for (int k = v_point_num - 1; k >= 0; k--) {
			const int id_NHWP2 =
				current_v * H * W * (kMaxPointsPerPixel + 1) * 2 +
				yi * W * (kMaxPointsPerPixel + 1) * 2 +
				xi * (kMaxPointsPerPixel + 1) * 2 +
				k * 2;

			float d_new = depth_gmms[id_NHWP2];
			float h = H_tri(d_new, d);

			if (h == 0) {
				break;//We start from high depth back to front, if this h is zero, all the subsequent will also be;
			}
			else {

				float alpha_new = depth_gmms[id_NHWP2 + 1];

				inner_sum += alpha_new * h;

			}
		}
		prod *= inner_sum;
	}

	return prod;


}

__global__ void ComputeProbabilityDMM(
	const float* depth_gmms, // (N, H, W, 150, 2)
	const int* num_gmms,
	const int N,
	const int H,
	const int W,
	float* output)
{
	// One thread per output pixel
	const int num_threads = gridDim.x * blockDim.x;
	const int tid = blockDim.x * blockIdx.x + threadIdx.x;
	for (int i = tid; i < H * W; i += num_threads) {
		const int yi = i / W;
		const int xi = i % W;

		for (int v = 0; v < N; v++) { //Loop over all the views
			float p = 0.0;
			const int id_NHW = v * H * W + yi * W + xi;
			const int v_point_num = num_gmms[id_NHW];

			for (int k = 0; k < v_point_num; k++) {
				const int id_NHWP2 =
					v * H * W * (kMaxPointsPerPixel + 1) * 2 +
					yi * W * (kMaxPointsPerPixel + 1) * 2 +
					xi * (kMaxPointsPerPixel + 1) * 2 +
					k * 2;

				float d = depth_gmms[id_NHWP2];
				float alpha = depth_gmms[id_NHWP2 + 1];

				//float prod = recurseProd(v, (v+1)%N,depth_gmms, num_gmms, d,  N, H, W, yi, xi);
				float prod = linProd(v, depth_gmms, num_gmms, d, N, H, W, yi, xi);

				p += alpha * prod;

			}
			output[id_NHW] = p;
		}

	}
}


torch::Tensor SoftDepthTestCuda(
	const torch::Tensor& depth_gmms,
	const torch::Tensor& num_gmms
)
{
	const int N = depth_gmms.size(0);
	const int H = depth_gmms.size(1);
	const int W = depth_gmms.size(2);
	const int P = depth_gmms.size(3);

	auto float_opts = depth_gmms.options().dtype(torch::kFloat32);
	torch::Tensor out_probability = torch::full({ N, 1, H, W }, 0.0, float_opts);

	const size_t blocks = 1024;
	const size_t threads = 64;

	ComputeProbabilityDMM << <blocks, threads >> > (
		depth_gmms.contiguous().data<float>(),
		num_gmms.contiguous().data<int32_t>(),
		N,
		H,
		W,
		out_probability.contiguous().data<float>());

	return out_probability;
}
