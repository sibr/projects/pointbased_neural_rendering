import torch
import torch.nn as nn
from diff_rasterization.rasterizer import PointsRasterizationSettings, PointsRasterizer
import torchvision

# Set the cuda device
device = torch.device("cuda:0")
torch.cuda.set_device(device)

#########################
# Common Initializations
#########################
image_width = 718
image_height = 476

znear = 0.1
zfar = 15.0
gamma = 1.0


raster_settings = PointsRasterizationSettings(
    image_height=image_height,
    image_width=image_width,
    points_per_pixel=50,
    znear=znear,
    zfar=zfar,
    gamma=gamma
)
rasterizer = PointsRasterizer(
    raster_settings=raster_settings
)


start_radius = 50
max_radius = 4*start_radius

#########################
# Ground Truth Initializations
#########################
with torch.no_grad():
    verts_gt = torch.tensor([[-0.1, -0.1, 1.0], [0.05, 0.05, 5.0], [0.0, 0.0, 10.0]]).to(device)
    verts_rgb_gt = torch.tensor([[1.0, 0.0, 0.0, 0.5, 0.3],
                                 [0.0, 1.0, 0.0, 0.0, 0.2],
                                 [1.0, 0.0, 1.0, 0.7, 0.3]]).to(device)
    inv_cov_gt = torch.tensor([[[0.005, 0.0], [0.0, 0.005]],
                               [[0.005, 0.0], [0.0, 0.005]],
                               [[0.005, 0.0], [0.0, 0.005]]]).to(device)
    gt, _, _ = rasterizer(points_screen=verts_gt,
                          features=verts_rgb_gt,
                          inv_cov=inv_cov_gt,
                          max_radius=max_radius)
    torchvision.utils.save_image(gt[:,:3,:,:], "F:/gt.png")


# Constructing point cloud
verts = torch.tensor([[-0.15, -0.05, 1.0], [0.15, 0.05, 5.0], [0.05, 0.0, 10.0]]).to(device)
verts_delta = nn.Parameter(torch.zeros(verts.shape, requires_grad=True).to(device))

verts_rgb = torch.tensor([[1.0, 0.1, 1.0, 0.3, 0.5],
                         [0.5, 0.5, 0.0, 0.9, 0.0],
                         [1.0, 0.0, 1.0, 0.0, 0.0]]).to(device)
verts_rgb_delta = nn.Parameter(torch.zeros(verts_rgb.shape, requires_grad=True).to(device))

inv_cov = torch.tensor([[[0.001, 0.0], [0.0, 0.001]],
                        [[0.005, 0.0], [0.0, 0.01]],
                        [[0.0005, 0.0], [0.0, 0.05]]]).to(device)
inv_cov_delta = nn.Parameter(torch.zeros((3,2,2), requires_grad=True).to(device))


optimizer_verts_rgb_delta = torch.optim.Adam([verts_rgb_delta], lr=0.01)
optimizer_verts_delta = torch.optim.Adam([verts_delta], lr=0.001)
optimizer_inv_cov_delta = torch.optim.Adam([inv_cov_delta], lr=0.0001)



for iter in range(600):
    optimizer_verts_delta.zero_grad()
    optimizer_verts_rgb_delta.zero_grad()
    optimizer_inv_cov_delta.zero_grad()

    image, _, _ = rasterizer(points_screen=verts + verts_delta,
                             features=verts_rgb + verts_rgb_delta,
                             inv_cov=inv_cov + inv_cov_delta,
                             max_radius=max_radius)
    loss = torch.sum((image - gt) ** 2)
    loss.backward()
    optimizer_verts_delta.step()
    optimizer_verts_rgb_delta.step()
    optimizer_inv_cov_delta.step()

    torchvision.utils.save_image(image[:,:3,:,:], "F:/image_{}.png".format(iter))
    print("[ITER {}] Loss {}".format(iter, loss.item()))
print("====POSITIONS=====")
print(verts + verts_delta)
print(verts_gt)
print((verts + verts_delta - verts_gt).mean())
print("======FEATURES=====")
print(verts_rgb + verts_rgb_delta)
print(verts_rgb_gt)
print((verts_rgb + verts_rgb_delta - verts_rgb_gt).mean())
print("=======INV_COV======")
print(inv_cov + inv_cov_delta)
print(inv_cov_gt)
print((inv_cov+inv_cov_delta-inv_cov_gt).mean())