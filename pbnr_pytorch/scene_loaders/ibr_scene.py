import os
import math
import sys
import json
import torch
from torch import nn
import numpy as np
from diff_rasterization.rasterizer import PointsRasterizationSettings, PointsRasterizer
import random
from utils.graphics_utils import geom_transform_points, geom_transform_vectors,\
    projectToCamera, getWorld2View,\
    getProjectionMatrix, getPatchProjectionMatrix
from utils.system_utils import mkdir_p, searchForMaxIteration
from scene_loaders.read_scenes_types import sceneLoadTypeCallbacks


def randomizeNeighbors(neighbors, N):
    return np.array(neighbors)[np.random.rand(len(neighbors)) > 1.0 / 20.0][:N]


def getNormalisedImageGrid(height, width):
    x_axis = torch.linspace(-1.0, 1.0-2.0/width, width) + 2.0/(2.0*width)
    y_axis = torch.linspace(-1.0, 1.0-2.0/height, height) + 2.0/(2.0*height)
    yv, xv = torch.meshgrid(y_axis, x_axis)
    return torch.stack((xv, yv, torch.ones_like(yv)), dim=0)


class CameraSimple(nn.Module):
    def __init__(self, R, T, top, bottom, right, left, znear, zfar, image_height, image_width):
        super(CameraSimple, self).__init__()

        self.image_height = int(image_height)
        self.image_width = int(image_width)

        self.world_view_transform = getWorld2View(torch.tensor(R), torch.tensor(T)).cuda()
        # Add real zfar znear here?
        self.projection_matrix = getPatchProjectionMatrix(znear=znear, zfar=zfar, top=top, bottom=bottom, right=right, left=left).transpose(1,2).cuda()
        self.full_proj_transform = self.world_view_transform.bmm(self.projection_matrix)
        self.camera_center = self.world_view_transform.inverse()[:, 3, :3]


class Camera(nn.Module):
    def __init__(self, colmap_id, R, T, FoVx, FoVy, image, image_name,
                 max_radius, extra_features, loaded_depthmap, loaded_normalmap, uid,
                 loaded_depthdelta=None,
                 loaded_normaldelta=None,
                 loaded_uncertainty=None,
                 loaded_extrafeatures=None,
                 loaded_expcoefs=None,
                 loaded_optimizedimages=None
                 ):
        super(Camera, self).__init__()

        self.neighbors = None
        self.uid = uid
        self.colmap_id = colmap_id
        self.R = R
        self.T = T
        self.FoVx = FoVx
        self.FoVy = FoVy

        self.original_image = image.clamp(0.0, 1.0)
        if loaded_optimizedimages is not None:
            self.image = nn.Parameter(loaded_optimizedimages.requires_grad_(True))
        else:
            image = image.clamp(0.0, 1.0)
            self.image = nn.Parameter(image.cuda().requires_grad_(True))

        self.image_width = self.image.shape[3]
        self.image_height = self.image.shape[2]
        self.image_name = image_name

        self.max_radius = int(max_radius)
        if loaded_expcoefs is not None:
            self.exposure_coef = nn.Parameter(loaded_expcoefs.requires_grad_(True))
        else:
            self.exposure_coef = nn.Parameter(torch.ones(1, device="cuda").requires_grad_(True))

        if loaded_uncertainty is not None:
            self.uncertainty_map = nn.Parameter(loaded_uncertainty.requires_grad_(True))
        else:
            self.uncertainty_map = nn.Parameter(0.5*torch.ones(self.image_height*self.image_width, 1, device="cuda").requires_grad_(True))

        if loaded_extrafeatures is not None:
            self.extra_features = nn.Parameter(loaded_extrafeatures.requires_grad_(True))
        else:
            self.extra_features = nn.Parameter(torch.zeros(1, extra_features, self.image_height, self.image_width, device="cuda").requires_grad_(True))

        self.point_grid = getNormalisedImageGrid(self.image_height, self.image_width).cuda()

        if loaded_depthmap is not None:
            self.depth_map = loaded_depthmap.cuda()
            if loaded_depthdelta is not None:
                self.depth_delta = nn.Parameter(loaded_depthdelta.requires_grad_(True))
            else:
                self.depth_delta = nn.Parameter(torch.zeros_like(self.depth_map, device="cuda").requires_grad_(True))

        if loaded_normalmap is not None:
            if loaded_normaldelta is not None:
                self.normal_map = nn.Parameter(loaded_normaldelta.cuda().requires_grad_(True))
            else:
                self.normal_map = nn.Parameter(loaded_normalmap.cuda().requires_grad_(True))

        try:
            self.zfar = self.getDepth().max()*2.0
            self.znear = self.getDepth()[self.getDepth() > 0].min()/2.0
        except:
            self.znear = 0.0000001
            self.zfar = 100.0

        self.world_view_transform = getWorld2View(torch.tensor(R), torch.tensor(T)).cuda()
        self.projection_matrix = getProjectionMatrix(znear=self.znear, zfar=self.zfar, fovX=self.FoVx, fovY=self.FoVy).transpose(1,2).cuda()
        self.full_proj_transform = self.world_view_transform.bmm(self.projection_matrix)
        self.camera_center = self.world_view_transform.inverse()[:, 3, :3]

    def getLookAtVector(self):
        view_cam_dir = torch.matmul(self.torch_cam.R, torch.tensor([0.0, 0.0, 1.0]).cuda())
        return (self.torch_cam.get_camera_center() + view_cam_dir).squeeze(dim=0)

    def getDepth(self):
        return self.depth_map

    def getDirRays(self):
        width = self.image.shape[3]
        height = self.image.shape[2]

        points = getNormalisedImageGrid(height, width).to("cuda")

        pointsA = points.permute(1,2,0).view(height*width, 3)

        K = self.projection_matrix[:, :3, :3].clone()
        Kinv = K.inverse()
        points_proj_inv = torch.matmul(pointsA, Kinv).squeeze(0)
        dirs = geom_transform_points(points_proj_inv, self.world_view_transform.inverse())
        dirs = dirs.view(height, width, 3)
        return dirs/torch.norm(dirs, dim=2, keepdim=True)

    def computeJacobian(self, point_cloud, normal, camera):
        s_world = torch.ones_like(normal, device="cuda")
        s_world[:, 1] = 0.0
        s_world[:, 2] = -normal[:, 0] / (normal[:, 2]+0.0000001)
        s_world = s_world / s_world.norm(dim=1, keepdim=True)

        t_world = normal.cross(s_world)
        t_world = t_world / t_world.norm(dim=1, keepdim=True)

        o = geom_transform_points(point_cloud, camera.world_view_transform)
        s = geom_transform_vectors(s_world, camera.world_view_transform)
        t = geom_transform_vectors(t_world, camera.world_view_transform)

        jacobian = torch.zeros(o.shape[0], 2, 2, device="cuda")
        jacobian[:, 0, 0] = s[:, 0] * o[:, 2] - s[:, 2] * o[:, 0]
        jacobian[:, 0, 1] = t[:, 0] * o[:, 2] - t[:, 2] * o[:, 0]
        jacobian[:, 1, 0] = s[:, 2] * o[:, 1] - s[:, 1] * o[:, 2]
        jacobian[:, 1, 1] = t[:, 2] * o[:, 1] - t[:, 1] * o[:, 2]


        # Fixes scaling issue when using cropped camera.
        test_scale_x = self.projection_matrix[0,0,0]/camera.projection_matrix[0,0,0]
        #test_scale_x = camera.projection_matrix[0, 0, 0]
        test_scale_y = self.projection_matrix[0,1,1]/camera.projection_matrix[0,1,1]
        #test_scale_y = camera.projection_matrix[0, 1, 1]

        scale = torch.matmul(camera.projection_matrix[:,:2,:2], torch.tensor([[test_scale_x, 0.0], [0.0, test_scale_y]], device="cuda"))
        jacobian = torch.matmul(jacobian, scale)
        jacobian = jacobian/(o[:, 2] * o[:, 2]).unsqueeze(dim=1).unsqueeze(dim=1)

        return jacobian

    def computeCovariance(self, point_cloud, normal, novel_view):
        novel_view_jacobian = self.computeJacobian(point_cloud, normal, novel_view)
        in_view_jacobian = self.computeJacobian(point_cloud, normal, self)

        full_jacobian = torch.bmm(in_view_jacobian.inverse(), novel_view_jacobian)

        covariance = torch.bmm(full_jacobian, full_jacobian.transpose(1,2))
        return covariance

    def getImageHarmonized(self):
        return self.image*self.exposure_coef

    def getPointsFromDepthMap(self, depth_map):
        image = self.getImageHarmonized().clamp(0.0, 1.0).to("cuda")
        extra_features_orig = self.extra_features.to("cuda")
        extra_features = torch.sigmoid(extra_features_orig)

        pixel_grid = self.point_grid.to("cuda")

        feature_map = torch.cat((image, extra_features), 1)

        points = depth_map*pixel_grid
        pointsA = points.permute(0,2,3,1).view(points.shape[2]*points.shape[3], points.shape[1])

        point_features = feature_map.permute(0, 2, 3, 1).view(feature_map.shape[2]*feature_map.shape[3],
                                                            feature_map.shape[1])

        K = self.projection_matrix[:, :3, :3]
        K[0,2,2] = 1.0 # This removes the z component of the trasnformation because z is already at view space.
        Kinv = K.inverse()
        points_proj_inv = torch.matmul(pointsA, Kinv).squeeze(0)
        points_wc = geom_transform_points(points_proj_inv, self.world_view_transform.inverse())

        return points_wc, point_features

    def render(self, viewpoint_camera, gamma):

        #torch.cuda.synchronize()
        #time_begin = time.time()

        raster_settings = PointsRasterizationSettings(
            image_height=int(viewpoint_camera.image_height),
            image_width=int(viewpoint_camera.image_width),
            points_per_pixel=50,
            znear=self.znear,
            zfar=self.zfar,
            gamma=gamma
        )
        rasterizer = PointsRasterizer(
                raster_settings=raster_settings
        )

        depth = self.getDepth().to("cuda")
        depth_delta = self.depth_delta.to("cuda")
        depth_map = depth + depth_delta

        uncertainty_map = self.uncertainty_map.to("cuda").clamp(0.1, 4.0)
        uncertainty_map_scale_matrix = uncertainty_map.unsqueeze(1) * torch.eye(2, 2, device="cuda").unsqueeze(0).repeat(uncertainty_map.shape[0], 1, 1)

        #torch.cuda.synchronize()
        #time_end = time.time()
        #print("Pre: {} | ".format(time_end - time_begin), end="")


        #torch.cuda.synchronize()
        #time_begin = time.time()

        point_cloud, features = self.getPointsFromDepthMap(depth_map)

        #torch.cuda.synchronize()
        #time_end = time.time()
        #print("2d->3d: {} | ".format(time_end - time_begin), end="")


        #torch.cuda.synchronize()
        #time_begin = time.time()

        ones = torch.ones((1, point_cloud.shape[0], 1), dtype=point_cloud.dtype, device="cuda")
        hom_point_cloud = torch.cat([point_cloud.unsqueeze(0), ones], dim=2)
        viewspace_points = projectToCamera(hom_point_cloud, viewpoint_camera)

        #torch.cuda.synchronize()
        #time_end = time.time()
        #print("3d->2d: {} | ".format(time_end - time_begin), end="")

        #torch.cuda.synchronize()
        #time_begin = time.time()

        normal_map = self.normal_map.to("cuda")
        normal_map = normal_map.view(3, normal_map.shape[2] * normal_map.shape[3]).permute(1, 0)
        normal_map = normal_map / normal_map.norm(2, dim=1, keepdim=True)

        filter_pos_x = torch.logical_and(viewspace_points[:, 0] < 1.0, viewspace_points[:, 0] > -1.0)
        filter_pos_y = torch.logical_and(viewspace_points[:, 1] < 1.0, viewspace_points[:, 1] > -1.0)
        filter_pos = torch.logical_and(filter_pos_x, filter_pos_y)

        view_in_ray = (point_cloud - self.camera_center)/(
                       point_cloud - self.camera_center).norm(dim=1, keepdim=True)
        cos_in_view = torch.abs(torch.bmm(normal_map.view(-1, 1, 3), view_in_ray.view(-1, 3, 1)))

        view_out_ray = (point_cloud - viewpoint_camera.camera_center) / (
                        point_cloud - viewpoint_camera.camera_center).norm(dim=1, keepdim=True)
        cos_out_view = torch.abs(torch.bmm(normal_map.view(-1, 1, 3), view_out_ray.view(-1, 3, 1)))

        filter_slanted = torch.logical_and(cos_in_view > 0.1, cos_out_view > 0.1).squeeze()
        filter = torch.logical_and(filter_pos, filter_slanted)

        filtered_pc = point_cloud[filter]
        filtered_normals = normal_map[filter]
        filtered_uncertainty = uncertainty_map_scale_matrix[filter]
        if filtered_pc.shape[0] == 0:
            return (torch.zeros(1, features.shape[1], viewpoint_camera.image_height, viewpoint_camera.image_width, device="cuda"),
                    torch.cat([10000.0 * torch.ones((1, viewpoint_camera.image_height, viewpoint_camera.image_width, 102, 1), device="cuda"),
                               torch.ones((1, viewpoint_camera.image_height, viewpoint_camera.image_width, 102, 1), device="cuda")], dim=-1),
                    torch.ones((1, viewpoint_camera.image_height, viewpoint_camera.image_width, 1)).cuda(),
                    torch.zeros(1, 1, viewpoint_camera.image_height, viewpoint_camera.image_width, device="cuda"))

        covariance = self.computeCovariance(filtered_pc, filtered_normals, viewpoint_camera)
        scaled_covariance = torch.bmm(covariance, filtered_uncertainty)
        with torch.no_grad():
            eigenval = np.linalg.eigvals(scaled_covariance.cpu())
            distortion = eigenval.min(axis=1)/eigenval.max(axis=1)
            dist_torch = torch.tensor(distortion).cuda()
            ninetinth = np.percentile(eigenval.flatten(), 95)
            pixel_sigma = int(np.ceil(3*np.sqrt(ninetinth)))
        inv_cov = scaled_covariance.inverse()

        #torch.cuda.synchronize()
        #time_end = time.time()
        #print("Cov: {} | ".format(time_end - time_begin), end="")

        #torch.cuda.synchronize()
        #time_begin = time.time()

        col_image, depth_gmms, num_gmms, mask = rasterizer(viewspace_points[filter],
                                            features[filter],
                                            dist_torch,
                                            inv_cov,
                                            pixel_sigma)

        #torch.cuda.synchronize()
        #time_end = time.time()
        #print("Rast: {} | ".format(time_end - time_begin), end="")

        return col_image, depth_gmms, num_gmms, mask

    def render_patch(self, viewpoint_camera, patch_size_y, patch_size_x, patch_origin_x, patch_origin_y,
                     gamma):
        pixel_size_y = (2 * math.tan(viewpoint_camera.FoVy / 2)) / viewpoint_camera.image_height
        pixel_size_x = (2 * math.tan(viewpoint_camera.FoVx / 2)) / viewpoint_camera.image_width
        cx = viewpoint_camera.image_width/2
        cy = viewpoint_camera.image_height/2
        top_pixel = cy - patch_origin_y
        bottom_pixel = top_pixel - patch_size_y
        right_pixel = cx - patch_origin_x
        left_pixel = right_pixel - patch_size_x
        camera_for_patch = CameraSimple(R=viewpoint_camera.R, T=viewpoint_camera.T,
                                        top=top_pixel*pixel_size_y, bottom=bottom_pixel*pixel_size_y,
                                        right=right_pixel*pixel_size_x, left=left_pixel*pixel_size_x,
                                        znear=viewpoint_camera.znear,
                                        zfar=viewpoint_camera.zfar,
                                        image_height=patch_size_y,
                                        image_width=patch_size_x)
        return self.render(viewpoint_camera=camera_for_patch, gamma=gamma)

class Scene():

    def __init__(self, path, name, scene_type, scene_representation_folder,
                 max_radius, extra_features, test_cameras, load_iter):
        """
        :param path: Path to colmap scene main folder.
        """
        self.extra_features = extra_features
        self.name = name
        max_radius = max_radius

        if load_iter or scene_representation_folder:
            if load_iter:
                load_iter = load_iter
            else:
                load_iter = searchForMaxIteration(os.path.join(scene_representation_folder, "depth_deltas"))


        self.test_cameras = test_cameras
        self.map_imgfilename_to_idx = {}
        scene_info = sceneLoadTypeCallbacks[scene_type](name, path)

        self.cameras = []
        self.blacklist_cameras = []
        self.blacklist = scene_info.blacklist
        for idx, cam_info in enumerate(scene_info.cameras):
            sys.stdout.write('\r')
            # the exact output you're looking for:
            sys.stdout.write("Loading \"{}\" | Camera {}/{} - Blacklist: {}".format(name, idx,
                                                                                    len(scene_info.cameras),
                                                                                    len(self.blacklist)))
            sys.stdout.flush()

            depthmap_path = os.path.join(path, "pbnrScene/depth_maps_type_2/",
                                         cam_info.image_name + ".ts")
            loaded_depthmap = None
            if os.path.exists(depthmap_path):
                loaded_depthmap = torch.load(depthmap_path)

            normalmap_path = os.path.join(path, "pbnrScene/normal_maps/",
                                         cam_info.image_name + ".ts")
            loaded_normalmap = None
            if os.path.exists(normalmap_path):
                loaded_normalmap = torch.load(normalmap_path)

            image_path = os.path.join(path, "pbnrScene/images/",
                                      cam_info.image_name + ".ts")
            loaded_image = None
            if os.path.exists(image_path):
                loaded_image = torch.load(image_path)

            loaded_depthdelta = None
            loaded_normaldelta = None
            loaded_uncertainty = None
            loaded_expcoefs = None
            loaded_optimizedimages = None
            loaded_extrafeatures = None

            if load_iter:
                try:
                    loaded_depthdelta = torch.load(os.path.join(scene_representation_folder, "depth_deltas",
                                                   "iteration_" + str(load_iter),
                                                   cam_info.image_name + ".depth_delta")).cuda()
                    loaded_normaldelta = torch.load(os.path.join(scene_representation_folder, "normals",
                                                   "iteration_" + str(load_iter),
                                                   cam_info.image_name + ".normals")).cuda()
                    loaded_uncertainty = torch.load(os.path.join(scene_representation_folder, "uncertainty_map",
                                                   "iteration_" + str(load_iter),
                                                   cam_info.image_name + ".uncertainty")).cuda()
                    loaded_expcoefs = torch.load(os.path.join(scene_representation_folder, "exposure_coef",
                                                   "iteration_" + str(load_iter),
                                                   cam_info.image_name + ".exp_coef")).cuda()
                    loaded_optimizedimages = torch.load(os.path.join(scene_representation_folder, "images",
                                                   "iteration_" + str(load_iter),
                                                   cam_info.image_name + ".img")).cuda()
                    loaded_extrafeatures = torch.load(os.path.join(scene_representation_folder, "learned_features",
                                                   "iteration_" + str(load_iter),
                                                   cam_info.image_name + ".feat")).cuda()
                except:
                    print("\nSkipping Loading View {} idx {}".format(cam_info.image_name, idx))
            camera = Camera(colmap_id=cam_info.uid, R=cam_info.R, T=cam_info.T, FoVx=cam_info.FovX,
                                       FoVy=cam_info.FovY, image=loaded_image,
                                       image_name=cam_info.image_name,
                                       max_radius=max_radius,
                                       extra_features=extra_features,
                                       loaded_depthmap=loaded_depthmap,
                                       loaded_normalmap=loaded_normalmap,
                                       uid=len(self.cameras),
                                       loaded_depthdelta=loaded_depthdelta,
                                       loaded_normaldelta=loaded_normaldelta,
                                       loaded_uncertainty=loaded_uncertainty,
                                       loaded_extrafeatures=loaded_extrafeatures,
                                       loaded_expcoefs=loaded_expcoefs,
                                       loaded_optimizedimages=loaded_optimizedimages)
            if (cam_info.image_name in self.blacklist):
                self.blacklist_cameras.append(camera)
            else:
                self.cameras.append(camera)

        sys.stdout.write('\n')
        self.shuffleCameras()
        for idx, cam in enumerate(self.cameras):
            self.map_imgfilename_to_idx[os.path.basename(cam.image_name)] = idx

        neighbors_dict_path = os.path.join(path, "pbnrScene/neighbors_dict.json")
        if os.path.exists(neighbors_dict_path):
            with open(neighbors_dict_path) as json_file:
                neighbors_dict = json.load(json_file)
            for cam in self.cameras:
                cam.neighbors = [self.map_imgfilename_to_idx[image_name] for image_name in neighbors_dict[os.path.basename(cam.image_name)]]
            for cam in self.blacklist_cameras:
                cam.neighbors = [self.map_imgfilename_to_idx[image_name] for image_name in neighbors_dict[os.path.basename(cam.image_name)]]

    def getNClosestCameras(self, ref_camera, N):
        camera_distances = []
        ref_camera_center = ref_camera.camera_center
        for idx, cam in enumerate(self.getAllCameras()):
            cam_center = cam.camera_center
            distance = torch.dist(ref_camera_center, cam_center)
            camera_distances.append((idx, distance))
        camera_distances.sort(key=lambda tup: tup[1])

        return [self.getAllCameras()[idx] for idx, distance in camera_distances][1:N+1]


    def shuffleCameras(self):
        random.shuffle(self.cameras)
        random.shuffle(self.blacklist_cameras)

    def getAllCameras(self):
        return self.cameras

    def getTestCameras(self):
        return self.cameras[:self.test_cameras]

    def getTrainCameras(self):
        return self.cameras[self.test_cameras:]

    def save(self, path, iteration):
        depth_deltas_path = os.path.join(path, self.name, "depth_deltas/iteration_{}".format(iteration))
        normals_path = os.path.join(path, self.name, "normals/iteration_{}".format(iteration))
        learned_feat_path = os.path.join(path, self.name, "learned_features/iteration_{}".format(iteration))
        uncertainty_map_path = os.path.join(path, self.name, "uncertainty_map/iteration_{}".format(iteration))
        exposure_coef_path = os.path.join(path, self.name, "exposure_coef/iteration_{}".format(iteration))
        images_path = os.path.join(path, self.name, "images/iteration_{}".format(iteration))
        mkdir_p(depth_deltas_path)
        mkdir_p(learned_feat_path)
        mkdir_p(uncertainty_map_path)
        mkdir_p(normals_path)
        mkdir_p(exposure_coef_path)
        mkdir_p(images_path)
        for cam in self.cameras:
            torch.save(cam.depth_delta.detach().cpu(), os.path.join(depth_deltas_path, cam.image_name) + ".depth_delta", _use_new_zipfile_serialization=True)
            torch.save(cam.normal_map.detach().cpu(), os.path.join(normals_path, cam.image_name) + ".normals", _use_new_zipfile_serialization=True)
            torch.save(cam.extra_features.detach().cpu(), os.path.join(learned_feat_path, cam.image_name) + ".feat", _use_new_zipfile_serialization=True)
            torch.save(cam.uncertainty_map.detach().cpu(), os.path.join(uncertainty_map_path, cam.image_name) + ".uncertainty", _use_new_zipfile_serialization=True)
            torch.save(cam.exposure_coef.detach().cpu(), os.path.join(exposure_coef_path, cam.image_name) + ".exp_coef", _use_new_zipfile_serialization=True)
            torch.save(cam.image.detach().cpu(), os.path.join(images_path, cam.image_name) + ".img", _use_new_zipfile_serialization=True)

    def getMaxDistanceCamera(self, ref_camera):
        max_dists_cams = (0.0, None)
        for cam in self.cameras:
            dist = torch.norm(cam.torch_cam.get_camera_center() - ref_camera.torch_cam.get_camera_center())
            if dist > max_dists_cams[0]:
                max_dists_cams = (dist, cam)
        return max_dists_cams


class MultiScene:
    def __init__(self, multi_scene_json, max_radius, extra_features,
                 test_cameras=3, load_iter=None):
        self.scenes = []
        for s in multi_scene_json["scenes"]:
            self.scenes.append(Scene(s.get("path"), s.get("name"), s.get("type"), s.get("scene_representation_folder"),
                                     max_radius, extra_features, test_cameras, load_iter))

    def normalizeNormals(self):
        for scene in self.scenes:
            for cam in scene.cameras:
                cam.normal_map = torch.nn.Parameter(cam.normal_map / cam.normal_map.norm(2, dim=1, keepdim=True))

    def getAllDepthParameters(self):
        p = []
        for scene in self.scenes:
            for cam in scene.cameras:
                p.append(cam.depth_delta)
        return p

    def getAllUncertaintyParameters(self):
        p = []
        for scene in self.scenes:
            for cam in scene.cameras:
                p.append(cam.uncertainty_map)
        return p

    def getAllFeatureParameters(self):
        p = []
        for scene in self.scenes:
            for cam in scene.cameras:
                p.append(cam.extra_features)
        return p

    def getAllNormalParameters(self):
        p = []
        for scene in self.scenes:
            for cam in scene.cameras:
                p.append(cam.normal_map)
        return p

    def getAllExpCoefParameters(self):
        p = []
        for scene in self.scenes:
            for cam in scene.cameras:
                p.append(cam.exposure_coef)
        return p

    def getAllImageParameters(self):
        p = []
        for scene in self.scenes:
            for cam in scene.cameras:
                p.append(cam.image)
        return p

    def getAllTrainCameras(self):
        traincams = []
        for scene in self.scenes:
            traincams.extend(scene.getTrainCameras())
        return traincams

    def getAllTestCameras(self):
        testcams = []
        for scene in self.scenes[:1]:
            testcams.extend(scene.getTestCameras())
        return testcams

    def getPCloudCamsForEuclidean(self, viewpoint_cam, sample, N=9):
        for scene in self.scenes:
            if viewpoint_cam in scene.getAllCameras():
                euclidean_neighbors = scene.getNClosestCameras(viewpoint_cam, N+5)
                if sample:
                    filtered_neighbors = randomizeNeighbors(euclidean_neighbors, N)
                    while len(filtered_neighbors) < 8:
                        filtered_neighbors = randomizeNeighbors(viewpoint_cam.neighbors, N)
                else:
                    filtered_neighbors = euclidean_neighbors[:N]
                pcloud_cams = [scene.getAllCameras()[cam_idx] for cam_idx in filtered_neighbors]
                return pcloud_cams
        assert 0, "Didnt find camera in scenes"

    def getPCloudCamsForScore(self, viewpoint_cam, sample, N=9):
        max_coverage_neighbors = viewpoint_cam.neighbors
        if sample:
            filtered_neighbors = random.sample(max_coverage_neighbors, N)
        else:
            filtered_neighbors = max_coverage_neighbors[:N]
        pcloud_cams = [self.scenes[0].getAllCameras()[cam_idx] for cam_idx in filtered_neighbors]
        return pcloud_cams

    def save(self, path, iteration):
        for scene in self.scenes:
            scene.save(path, iteration)
