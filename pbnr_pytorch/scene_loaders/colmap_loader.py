import numpy as np
import collections

CameraModel = collections.namedtuple(
    "CameraModel", ["model_id", "model_name", "num_params"])
Camera = collections.namedtuple(
    "Camera", ["id", "model", "width", "height", "params"])
BaseImage = collections.namedtuple(
    "Image", ["id", "qvec", "tvec", "camera_id", "name", "xys", "point3D_ids"])
Point3D = collections.namedtuple(
    "Point3D", ["id", "xyz", "rgb", "error", "image_ids", "point2D_idxs"])


def qvec2rotmat(qvec):
    return np.array([
        [1 - 2 * qvec[2]**2 - 2 * qvec[3]**2,
         2 * qvec[1] * qvec[2] - 2 * qvec[0] * qvec[3],
         2 * qvec[3] * qvec[1] + 2 * qvec[0] * qvec[2]],
        [2 * qvec[1] * qvec[2] + 2 * qvec[0] * qvec[3],
         1 - 2 * qvec[1]**2 - 2 * qvec[3]**2,
         2 * qvec[2] * qvec[3] - 2 * qvec[0] * qvec[1]],
        [2 * qvec[3] * qvec[1] - 2 * qvec[0] * qvec[2],
         2 * qvec[2] * qvec[3] + 2 * qvec[0] * qvec[1],
         1 - 2 * qvec[1]**2 - 2 * qvec[2]**2]])


class Image(BaseImage):
    def qvec2rotmat(self):
        return qvec2rotmat(self.qvec)


def read_intrinsics_text(path):
    """
    Taken from https://github.com/colmap/colmap/blob/dev/scripts/python/read_write_model.py
    """
    cameras = {}
    with open(path, "r") as fid:
        while True:
            line = fid.readline()
            if not line:
                break
            line = line.strip()
            if len(line) > 0 and line[0] != "#":
                elems = line.split()
                camera_id = int(elems[0])
                model = elems[1]
                assert model == "PINHOLE", "While the loader support other types, the rest of the code assumes PINHOLE"
                width = int(elems[2])
                height = int(elems[3])
                params = np.array(tuple(map(float, elems[4:])))
                cameras[camera_id] = Camera(id=camera_id, model=model,
                                            width=width, height=height,
                                            params=params)
    return cameras


def read_extrinsics_text(path):
    """
    Taken from https://github.com/colmap/colmap/blob/dev/scripts/python/read_write_model.py
    """
    images = {}
    with open(path, "r") as fid:
        while True:
            line = fid.readline()
            if not line:
                break
            line = line.strip()
            if len(line) > 0 and line[0] != "#":
                elems = line.split()
                image_id = int(elems[0])
                qvec = np.array(tuple(map(float, elems[1:5])))
                tvec = np.array(tuple(map(float, elems[5:8])))
                camera_id = int(elems[8])
                image_name = elems[9]
                elems = fid.readline().split()
                xys = np.column_stack([tuple(map(float, elems[0::3])),
                                       tuple(map(float, elems[1::3]))])
                point3D_ids = np.array(tuple(map(int, elems[2::3])))
                images[image_id] = Image(
                    id=image_id, qvec=qvec, tvec=tvec,
                    camera_id=camera_id, name=image_name,
                    xys=xys, point3D_ids=point3D_ids)
    return images


def read_colmap_bin_array(path):
    """
    Taken from https://github.com/colmap/colmap/blob/dev/scripts/python/read_dense.py

    :param path: path to the colmap binary file.
    :return: nd array with the floating point values in the value
    """
    with open(path, "rb") as fid:
        width, height, channels = np.genfromtxt(fid, delimiter="&", max_rows=1,
                                                usecols=(0, 1, 2), dtype=int)
        fid.seek(0)
        num_delimiter = 0
        byte = fid.read(1)
        while True:
            if byte == b"&":
                num_delimiter += 1
                if num_delimiter >= 3:
                    break
            byte = fid.read(1)
        array = np.fromfile(fid, np.float32)
    array = array.reshape((width, height, channels), order="F")
    return np.transpose(array, (1, 0, 2)).squeeze()


"""
def load_colmap_in_pytorch3d(path, device):
    # Load obj file
    verts, faces = load_ply(os.path.join(path, "stereo/meshed-delaunay.ply"))
    mesh0 = Meshes(verts=[verts.to(device)],
                  faces=[faces.to(device)],
                  textures=Textures(verts_rgb=torch.ones_like(verts.unsqueeze(axis=0)).to(device)))


    images = read_extrinsics_text(os.path.join(path, "stereo/sparse/images.txt"))
    cameras = read_intrinsics_text(os.path.join(path, "stereo/sparse/cameras.txt"))
    assert(len(images) == len(cameras))
    Rs = []
    Ts = []
    FoVys = []
    #FoVxs = []
    for key in images:
        im = images[key]
        cam = cameras[key]
        Rs.append(np.transpose(qvec2rotmat(im.qvec)))
        Ts.append(np.array(im.tvec))
        focal_length_x = cam.params[0]
        focal_length_y = cam.params[1]
        fovy = 2.0 * math.atan(0.5 * cam.height / focal_length_y)
        #fovx = 2.0 * math.atan(0.5 * cam.width / focal_length_x)
        FoVys.append(fovy)
        #FoVxs.append(fovx)
    #cameras = OpenGLPerspectiveCameras(device=device, R=Rs, T=Ts,
    #                                   znear=0.001, zfar=1000.0, fov=FoVys, degrees=False)
    EXT = 3
    cameras = OpenGLPerspectiveCameras(device=device, R=Rs[:EXT], T=Ts[:EXT],
                                       znear=0.001, zfar=1000.0, fov=FoVys[:EXT], degrees=False)
    mesh = mesh0.extend(EXT)

    return mesh, cameras
"""
