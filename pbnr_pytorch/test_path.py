import torch
from scene_loaders.ibr_scene import MultiScene, CameraSimple
import argparse
import random
from utils.system_utils import searchForMaxIteration
from utils.image_utils import crop_image
import os
import torchvision
from diff_rasterization.soft_depth_test import _SoftDepthTest
import json
import numpy as np
import math

def render_viewpoint2(viewpoint_camera, pcloud_cameras, patch=None, gamma=1.0):
    if patch==None:
        patch = (0, 0, viewpoint_camera.image_width, viewpoint_camera.image_height)
    patch_origin_x, patch_origin_y, patch_size_x, patch_size_y = patch
    features_stack = torch.tensor([]).to(device)
    color_stack = torch.tensor([]).to(device)
    depth_gmms_stack = torch.tensor([]).to(device)
    num_gmms_stack = torch.tensor([]).int().to(device)
    l2_stack = torch.tensor([]).to(device)
    blend_scores_stack = torch.tensor([]).to(device)
    for idx, pcloud_cam in enumerate(pcloud_cameras):
        rendered_point_cloud, depth_gmms, num_gmms, blend_scores = pcloud_cam.render_patch(viewpoint_camera=viewpoint_camera,
                                                                                           patch_origin_x=patch_origin_x, patch_size_x=patch_size_x,
                                                                                           patch_origin_y=patch_origin_y, patch_size_y=patch_size_y,
                                                                                           gamma=gamma)
        color_stack = torch.cat((color_stack, rendered_point_cloud), dim=0)
        blend_scores_stack = torch.cat((blend_scores_stack, blend_scores), dim=0)
        features_stack = torch.cat((features_stack, rendered_point_cloud), dim=0)
        depth_gmms_stack = torch.cat((depth_gmms_stack, depth_gmms), dim=0)
        num_gmms_stack = torch.cat((num_gmms_stack, num_gmms.int()), dim=0)
        l2_stack = torch.cat((l2_stack, torch.nn.functional.mse_loss(rendered_point_cloud[:,:3,:,:], crop_image(viewpoint_camera.image, patch)*blend_scores).unsqueeze(0)), dim=0)
    color_stack = color_stack.view(color_stack.shape[0], -1, 3, color_stack.shape[2], color_stack.shape[3])

    with torch.no_grad():
        prob_map = _SoftDepthTest.apply(depth_gmms_stack, num_gmms_stack)

    image = neural_renderer(features_stack, prob_map * blend_scores_stack)
    return image, color_stack, l2_stack.mean()

def render_viewpoint(viewpoint_camera, pcloud_cameras, patch=None, gamma=1.0):
    print("render_viewpoint")
    print(viewpoint_camera.projection_matrix)
    features_stack = torch.tensor([]).to(device)
    color_stack = torch.tensor([]).to(device)
    depth_gmms_stack = torch.tensor([]).to(device)
    num_gmms_stack = torch.tensor([]).int().to(device)
    l2_stack = torch.tensor([]).to(device)
    blend_scores_stack = torch.tensor([]).to(device)
    for idx, pcloud_cam in enumerate(pcloud_cameras):
        rendered_point_cloud, depth_gmms, num_gmms, blend_scores = pcloud_cam.render(viewpoint_camera=viewpoint_camera,
                                                                                     gamma=gamma)
        color_stack = torch.cat((color_stack, rendered_point_cloud), dim=0)
        blend_scores_stack = torch.cat((blend_scores_stack, blend_scores), dim=0)
        features_stack = torch.cat((features_stack, rendered_point_cloud), dim=0)
        depth_gmms_stack = torch.cat((depth_gmms_stack, depth_gmms), dim=0)
        num_gmms_stack = torch.cat((num_gmms_stack, num_gmms.int()), dim=0)
    color_stack = color_stack.view(color_stack.shape[0], -1, 3, color_stack.shape[2], color_stack.shape[3])

    with torch.no_grad():
        prob_map = _SoftDepthTest.apply(depth_gmms_stack, num_gmms_stack)

    image = neural_renderer(features_stack, prob_map * blend_scores_stack)
    return image, color_stack, l2_stack.mean()

def loadLookAtPath(file):
    def normalize(v):
        norm = np.linalg.norm(v)
        if norm == 0:
            return v
        return v / norm

    cameras = []

    w = 900
    h = 600
    aratio = w/h

    f = open(file, "r")
    for cam_str in f.readlines():
        params_str = cam_str.split("-D")[1:]
        eye = np.array(params_str[0].split("=")[1].split(",")).astype(np.float32)
        at = np.array(params_str[1].split("=")[1].split(",")).astype(np.float32)
        up = np.array(params_str[2].split("=")[1].split(",")).astype(np.float32)
        fovy = np.array(params_str[3].split("=")[1]).astype(np.float32)
        clip = np.array(params_str[4].split("=")[1].split(",")).astype(np.float32)

        fovx = fovy*aratio
        zAxis = normalize(eye-at)
        xAxis = normalize(np.cross(up, zAxis))
        yAxis = np.cross(zAxis, xAxis)

        t = np.array([-eye.dot(xAxis), eye.dot(yAxis), eye.dot(zAxis)])
        R = np.array([xAxis, -yAxis, -zAxis]).transpose()

        pixel_size_y = (2 * math.tan(fovy / 2)) / h
        pixel_size_x = (2 * math.tan(fovx / 2)) / w

        cx = w/2
        cy = h/2
        top_pixel = cy
        bottom_pixel = top_pixel - h
        right_pixel = cx
        left_pixel = right_pixel - w
        cameras.append(CameraSimple(R=R, T=t,
                                    top=top_pixel*pixel_size_y, bottom=bottom_pixel*pixel_size_y,
                                    right=right_pixel*pixel_size_x, left=left_pixel*pixel_size_x,
                                    znear=clip[0].item(),
                                    zfar=clip[1].item(),
                                    image_height=h,
                                    image_width=w))
    return cameras

parser = argparse.ArgumentParser(description='Train your network sailor.')
parser.add_argument('-i', '--input_path', required=True)
parser.add_argument('-o', '--output_path', required=False)

parser.add_argument('--load_iter', required=False, type=int, default=None)
parser.add_argument('--max_radius', required=False, type=int, default=8)
parser.add_argument('--test_cameras', required=False, default=3)
parser.add_argument('--extra_features', type=int, default=6)

args = parser.parse_args()

with torch.no_grad():
    device = torch.device("cuda:0")
    torch.cuda.set_device(device)
    torch.manual_seed(0)
    random.seed(0)

    with open(args.input_path) as json_file:
        input_json = json.load(json_file)

    neural_renderer = None
    if input_json.get("neural_weights_folder"):
        if args.load_iter:
            load_iter = args.load_iter
        else:
            load_iter = searchForMaxIteration(input_json.get("neural_weights_folder"))
        neural_renderer = torch.jit.load(os.path.join(input_json.get("neural_weights_folder"), "model_" + str(load_iter)))

    cameras_path = loadLookAtPath("F:/gkopanas/pointbasedIBR/scenes/deep_blending/museum/2021museum.lookat")

    scene = MultiScene(input_json,
                       args.max_radius, args.extra_features,
                       int(args.test_cameras), args.load_iter)


    train_viewpoint = scene.getAllTrainCameras()[2]
    train_pcloud_cams = scene.getPCloudCamsForScore(train_viewpoint, False)
    image, image_stack, _ = render_viewpoint(cameras_path[0], train_pcloud_cams, None, True)
    print("path")
    print(cameras_path[0].world_view_transform)
    print(cameras_path[0].projection_matrix)
    torchvision.utils.save_image(image, "F:/test.png")

    image, image_stack, _ = render_viewpoint2(train_viewpoint, train_pcloud_cams, None, True)
    print(train_viewpoint.image_name)
    print(train_viewpoint.world_view_transform)
    print(train_viewpoint.projection_matrix)
    torchvision.utils.save_image(image, "F:/test2.png")
