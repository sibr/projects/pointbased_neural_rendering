import torch
import math

def geom_transform_vectors(vectors, transf_matrix):

    P, _ = vectors.shape
    zeros = torch.zeros(P, 1, dtype=vectors.dtype, device=vectors.device)
    vectors_hom = torch.cat([vectors, zeros], dim=1)
    vectors_out = torch.matmul(vectors_hom, transf_matrix)

    return (vectors_out[..., :3]).squeeze(dim=0)

def geom_transform_points(points, transf_matrix):

    P, _ = points.shape
    ones = torch.ones(P, 1, dtype=points.dtype, device=points.device)
    points_hom = torch.cat([points, ones], dim=1)
    points_out = torch.matmul(points_hom, transf_matrix)

    denom = points_out[..., 3:]
    return (points_out[..., :3] / denom).squeeze(dim=0)

def geom_transform_Zviewspace(points, full_proj_transform, world2view):
    proj_space = geom_transform_points(points, full_proj_transform)
    view_space = geom_transform_points(points, world2view)
    proj_space[..., 2] = view_space[..., 2]

    return proj_space

def convert_Zworld_to_Zndc(Zworld, cam):
    c = cam.projection_matrix[0, 2, 2]
    d = cam.projection_matrix[0, 3, 2]
    return (c * Zworld + d) / Zworld

def projectToCamera(hom_cloud, cam):
    full_proj_transform = cam.full_proj_transform
    pts_projected = hom_cloud.bmm(full_proj_transform)
    pts_projected_normalised = pts_projected / (pts_projected[..., 3:] + 0.00001)

    view_transform = cam.world_view_transform
    points_viewspace = hom_cloud.bmm(view_transform)
    points_viewspace = points_viewspace / points_viewspace[..., 3:]

    pts_projected_normalised[..., 2] = points_viewspace[..., 2]
    return pts_projected_normalised.squeeze()[:, :3]

def getWorld2View(R, t):
    Rt = torch.zeros(1, 4, 4)
    Rt[:, :3, :3] = R
    Rt[:, 3, :3] = t
    Rt[:, 3, 3] = 1.0
    return Rt

def getPatchProjectionMatrix(znear, zfar, top, bottom, right, left):
    top = top*znear  # pyre-ignore[16]
    bottom = bottom * znear  # pyre-ignore[16]
    right = right * znear  # pyre-ignore[16]
    left = left * znear

    P = torch.zeros( 1, 4, 4)

    #ones = torch.ones((self._N))

    # NOTE: In OpenGL the projection matrix changes the handedness of the
    # coordinate frame. i.e the NDC space postive z direction is the
    # camera space negative z direction. This is because the sign of the z
    # in the projection matrix is set to -1.0.
    # In pytorch3d we maintain a right handed coordinate system throughout
    # so the so the z sign is 1.0.
    z_sign = 1.0

    P[:, 0, 0] = 2.0 * znear / (right - left)
    P[:, 1, 1] = 2.0 * znear / (top - bottom)
    P[:, 0, 2] = (right + left) / (right - left)
    P[:, 1, 2] = (top + bottom) / (top - bottom)
    P[:, 3, 2] = z_sign

    # NOTE: This part of the matrix is for z renormalization in OpenGL
    # which maps the z to [-1, 1]. This won't work yet as the torch3d
    # rasterizer ignores faces which have z < 0.
    # P[:, 2, 2] = z_sign * (far + near) / (far - near)
    # P[:, 2, 3] = -2.0 * far * near / (far - near)
    # P[:, 3, 2] = z_sign * torch.ones((N))

    # NOTE: This maps the z coordinate from [0, 1] where z = 0 if the point
    # is at the near clipping plane and z = 1 when the point is at the far
    # clipping plane. This replaces the OpenGL z normalization to [-1, 1]
    # until rasterization is changed to clip at z = -1.
    P[:, 2, 2] = z_sign * zfar / (zfar - znear)
    P[:, 2, 3] = -(zfar * znear) / (zfar - znear)

    return P


def getProjectionMatrix(znear, zfar, fovX, fovY):
    tanHalfFovY = math.tan((fovY / 2))
    tanHalfFovX = math.tan((fovX / 2))

    top = tanHalfFovY * znear
    bottom = -top
    right = tanHalfFovX * znear
    left = -right

    P = torch.zeros( 1, 4, 4)

    # NOTE: In OpenGL the projection matrix changes the handedness of the
    # coordinate frame. i.e the NDC space postive z direction is the
    # camera space negative z direction. This is because the sign of the z
    # in the projection matrix is set to -1.0.
    # In pytorch3d we maintain a right handed coordinate system throughout
    # so the so the z sign is 1.0.
    z_sign = 1.0

    P[:, 0, 0] = 2.0 * znear / (right - left)
    P[:, 1, 1] = 2.0 * znear / (top - bottom)
    P[:, 0, 2] = (right + left) / (right - left)
    P[:, 1, 2] = (top + bottom) / (top - bottom)
    P[:, 3, 2] = z_sign

    # NOTE: This part of the matrix is for z renormalization in OpenGL
    # which maps the z to [-1, 1]. This won't work yet as the torch3d
    # rasterizer ignores faces which have z < 0.
    # P[:, 2, 2] = z_sign * (far + near) / (far - near)
    # P[:, 2, 3] = -2.0 * far * near / (far - near)
    # P[:, 3, 2] = z_sign * torch.ones((N))

    # NOTE: This maps the z coordinate from [0, 1] where z = 0 if the point
    # is at the near clipping plane and z = 1 when the point is at the far
    # clipping plane. This replaces the OpenGL z normalization to [-1, 1]
    # until rasterization is changed to clip at z = -1.
    P[:, 2, 2] = z_sign * zfar / (zfar - znear)
    P[:, 2, 3] = -(zfar * znear) / (zfar - znear)

    return P