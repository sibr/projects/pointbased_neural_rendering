/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */

#pragma once

# include <core/system/Config.hpp>
# include <core/system/CommandLineArgs.hpp>

# ifdef SIBR_OS_WINDOWS
#  ifdef SIBR_STATIC_DEFINE
#    define SIBR_EXPORT
#    define SIBR_NO_EXPORT
#  else
#    ifndef SIBR_EXP_PBNR_BUILDER_EXPORT
#      ifdef SIBR_EXP_PBNR_EXPORTS
/* We are building this library */
#        define SIBR_EXP_PBNR_EXPORT __declspec(dllexport)
#      else
/* We are using this library */
#        define SIBR_EXP_PBNR_EXPORT __declspec(dllimport)
#      endif
#    endif
#    ifndef SIBR_NO_EXPORT
#      define SIBR_NO_EXPORT
#    endif
#  endif
# else
#  define SIBR_EXP_PBNR_EXPORT
# endif


namespace sibr {

	/// Arguments for all ULR applications.
	struct PbnrAppArgs :
		virtual BasicIBRAppArgs {
		Arg<std::string> tensorboard_path = { "tensorboard_path", "" };
		Arg<std::string> scene_name = { "scene_name", "" };
		Arg<std::string> iteration = { "iteration" , "" };

		Arg<std::string> images_path = { "images_path" , "" };
		Arg<std::string> exp_coef_path = { "exp_coef_path" , "" };
		Arg<std::string> normal_map_path = { "normal_map_path" , "" };
		Arg<std::string> depth_delta_path = { "depth_delta_path" , "" };
		Arg<std::string> model_path = { "model_path", "" };
		Arg<std::string> uncertainties_path = { "uncertainties_path", "" };
		Arg<std::string> features_path = { "features_path", "" };
		Arg<std::string> ogl_data_path = { "ogl_data_path", "" };
		ArgSwitch preprocess_mode = { "preprocess_mode", false, "Render depth maps and normal maps for scene" };
		ArgSwitch debug_mode = { "debug_mode", false, "Render intermediate results for debugging." };
		ArgSwitch leave_one_out = { "leave_one_out", false, "Leave one out rendering." };
		Arg<int> splat_layers = { "splat_layers", 10, "Number of layers for layered rendering." };
		Arg<int> input_cams = { "input_cams", 9, "Number of input cameras used to render each novel view." };
		Arg<int> texture_width = { "texture-width", 900, "Horizontal resolution of the rendering, the vertical will be 1.5 smaller."};

	};

}

