#version 420

layout (location = 0) in vec3 aPos;
layout(location = 3) in vec3 in_normal;

uniform mat4 mvp;

out vec3 GtoF_normal;

void main()
{
    gl_Position = mvp * vec4(aPos, 1.0);
	if (in_normal==vec3(0.0,0.0,0.0)) {
		GtoF_normal = vec3(1.0,1.0,1.0);
	}
	else {
		GtoF_normal = normalize(in_normal);
	}
	GtoF_normal=(GtoF_normal+1.0)/2.0;
	gl_PointSize = 4;
}