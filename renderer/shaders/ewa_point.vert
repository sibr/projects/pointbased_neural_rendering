/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#version 460

layout(location = 0) in vec3 in_position;
layout(location = 1) in vec3 in_color;
layout(location = 2) in vec3 in_normal;
layout(location = 3) in float in_uncertainty;
layout(location = 4) in vec3 in_feats1;
layout(location = 5) in vec3 in_feats2;

out vec3 vPosition;
out vec3 vColor;
out vec3 vNormal;
out float vUncertainty;
out vec3 vFeats1;
out vec3 vFeats2;

out int vDrawID;

void main(void)
{
	vPosition = in_position;
	vColor = in_color;
	vNormal = in_normal;
	vUncertainty = in_uncertainty;
	vFeats1 = in_feats1;
	vFeats2 = in_feats2;
	vDrawID = gl_DrawID;
}
