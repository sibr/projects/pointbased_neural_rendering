// Copyright (c) Facebook, Inc. and its affiliates. All rights reserved.

#pragma once

// Given a pixel coordinate 0 <= i < S, convert it to a normalized device
// coordinate in the range [-1, 1]. We divide the NDC range into S evenly-sized
// pixels, and assume that each pixel falls in the *center* of its range.
__device__ inline float PixToNdc(int i, int S) {
  // NDC x-offset + (i * pixel_width + half_pixel_width)
  return -1 + (2 * i + 1.0f) / S;
}

__device__ inline float NdcToPix(float i, int S) {
  return ((i + 1.0)*S - 1.0)/2.0;
}

// The maximum number of points per pixel that we can return. Since we use
// thread-local arrays to hold and sort points, the maximum size of the array
// needs to be known at compile time. There might be some fancy template magic
// we could use to make this more dynamic, but for now just fix a constant.
// TODO: is 8 enough? Would increasing have performance considerations?
const int32_t kMaxPointsPerPixel = 101;
const int32_t kMaxPointPerPixelLocal = 101;

template <typename T>
__device__ inline void BubbleSort(T* arr, int n) {
  bool already_sorted;
  // Bubble sort. We only use it for tiny thread-local arrays (n < 8); in this
  // regime we care more about warp divergence than computational complexity.
  for (int i = 0; i < n - 1; ++i) {
    already_sorted=true;
    for (int j = 0; j < n - i - 1; ++j) {
      if (arr[j + 1] < arr[j]) {
        already_sorted = false;
        T temp = arr[j];
        arr[j] = arr[j + 1];
        arr[j + 1] = temp;
      }
    }
    if (already_sorted)
        break;
  }
}

__device__ inline void BubbleSort2(int32_t* arr, const float* points, int n) {
  bool already_sorted;
  // Bubble sort. We only use it for tiny thread-local arrays (n < 8); in this
  // regime we care more about warp divergence than computational complexity.
  for (int i = 0; i < n - 1; ++i) {
    already_sorted=true;
    for (int j = 0; j < n - i - 1; ++j) {
      float p_j0_z = points[arr[j]*3 + 2];
      float p_j1_z = points[arr[j+1]*3 + 2];
      if (p_j1_z < p_j0_z) {
        already_sorted = false;
        int32_t temp = arr[j];
        arr[j] = arr[j + 1];
        arr[j + 1] = temp;
      }
    }
    if (already_sorted)
        break;
  }
}
