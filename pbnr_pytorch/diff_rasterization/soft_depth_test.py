import torch
from diff_rasterization import _C

class _SoftDepthTest(torch.autograd.Function):
    @staticmethod
    def forward(
        ctx,
        depth_gmms,
        num_gmms,
    ):
        args = (
            depth_gmms,
            num_gmms,
        )
        prob_map = _C.soft_depth_test(*args)
        return prob_map
