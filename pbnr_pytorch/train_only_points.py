import torch
from scene_loaders.ibr_scene import Scene
import uuid
from torch.utils.tensorboard import SummaryWriter
import argparse
import time
from itertools import compress
from model_defs.Loss_Functions import l1_loss
from utils.system_utils import mkdir_p
import os

def logTransform(image, output_max=1.0):
    c = output_max/torch.log(image.max() + 1)
    return c*torch.log(1+image)

def normalise(vector):
    min_v = torch.min(vector)
    range_v = torch.max(vector) - min_v
    normalised = (vector - min_v) / range_v

    return normalised

def crop(img, startx, width, starty, height):
    b,c,y,x = img.shape
    assert(y>=starty+height and x>=startx+width)
    return img[:, :, starty:starty+height, startx:startx+width]


def render_viewpoint(viewpoint_camera, pcloud_camera, render_scale, patch_origin_x, patch_origin_y, patch_size_x, patch_size_y):
    radius_scaled = radius/render_scale
    rendered_point_cloud, _ = pcloud_camera.render_patch(viewpoint_camera=viewpoint_camera,
                                                         patch_origin_x=patch_origin_x, patch_origin_y=patch_origin_y,
                                                         patch_size_y=patch_size_y, patch_size_x=patch_size_x,
                                                         radius=radius_scaled, scale=render_scale,
                                                         gamma=gamma, bin_size=0)
    return rendered_point_cloud

parser = argparse.ArgumentParser(description='Train your network sailor.')
parser.add_argument('-i', '--input_path', required=True)
parser.add_argument('-o', '--output_path', required=False)
#parser.add_argument('-m', '--model_weights', required=False)
parser.add_argument('--depth_type', required=False)
parser.add_argument('--hierarchical', required=False, default=0)

args = parser.parse_args()

device = torch.device("cuda:0")
torch.cuda.set_device(device)

path = os.path.join(args.input_path, "colmap")
scales_list = [1.0/16.0, 1.0/8.0, 1.0/4.0, 1.0/2.0, 1.0]
scene = Scene(path, int(args.depth_type), scales_list, device)

if args.output_path:
    unique_str = args.output_path
else:
    unique_str = str(uuid.uuid4())

tensorboard_folder = "./tensorboard_3d/{}".format(unique_str)
model_unique_folder = "./models/{}".format(unique_str)

mkdir_p(model_unique_folder)
tb_writer = SummaryWriter(tensorboard_folder)
print("Output Folders:")
print(" * {}".format(tensorboard_folder))
print(" * {}".format(model_unique_folder))


optimizer_depth_map = torch.optim.SGD(scene.getAllCameraDepthParameters(), lr=1000000.0)

iteration = 1
radius = 5
if int(args.hierarchical) == 1:
    scales_mask = [True, True, True, True, True]
    scale_update_step = 10000
    gamma_reset_step = 10000
    gamma_saturated_step= 10000
else:
    scales_mask = [False, False, False, False, True]
    scale_update_step = 10000
    gamma_reset_step = scale_update_step
    gamma_saturated_step = 5*scale_update_step
scales = list(compress(scales_list, scales_mask))

gamma_max = 1.0
gamma_min = 1.0

scales_idx = 1
while True:
    start_time = time.time()
    gamma = gamma_max

    #viewpoint_idx = randint(0, len(scene.getTrainCameras())-1)
    #viewpoint_idx = randint(9, 12)
    viewpoint_idx = 9
    viewpoint_cam = scene.getTrainCameras()[viewpoint_idx]
    pcloud_cams = scene.getNClosestCameras(viewpoint_cam, 4)

    patch_size_y = 143
    patch_size_x = 190
    patch_origin_x = 75
    patch_origin_y = 180

    # Zero out the gradients once, to accumulate the gradients for all scales and do the backwards pass once.
    optimizer_depth_map.zero_grad()
    total_loss = 0.0
    for scale in scales:
        image = render_viewpoint(viewpoint_cam, pcloud_cams[0], scale, patch_origin_x, patch_origin_y, patch_size_x, patch_size_y)
        gt_image = crop(viewpoint_cam.images_scaled[scale], patch_origin_x, patch_size_x, patch_origin_y, patch_size_y)

        loss = l1_loss(image, gt_image)

        loss.backward()
        total_loss += loss.item()
        torch.cuda.synchronize()

        if iteration%1==0:
            tb_writer.add_images("train_point_cloud_{}_{}".format(viewpoint_idx, scale), image,
                                 global_step=iteration)
            #adj_depth_map = torch.clamp(pcloud_cam.getDepth()+pcloud_cam.depth_delta, 0, 100.0)
            #tb_writer.add_images("depth_map_{}".format(idx), logTransform(adj_depth_map),
            #                    global_step=iteration)
            #tb_writer.add_images("depth_delta_{}".format(idx), logTransform(pcloud_cam.depth_delta),
            #                     global_step=iteration)

    # We do the optimization step once for all the scales
    optimizer_depth_map.step()
    total_loss = total_loss/len(scales)
    tb_writer.add_scalars('train_loss_viewpoint_{}'.format(viewpoint_idx),{"l1_loss": total_loss}, iteration)
    tb_writer.add_scalar('gamma', gamma, iteration)
    if iteration%1==0:
        print("[TRAIN ITER {} CAM {} gamma {}] Loss {} ".format(iteration, viewpoint_idx,
                                                                gamma, total_loss))


    iteration += 1



