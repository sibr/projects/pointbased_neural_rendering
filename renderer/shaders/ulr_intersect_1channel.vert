#version 420

uniform mat4 proj;

const int CUBE_DIR = 0;
const int ALONG_NORMAL = 1;
const int ALONG_RAY = 2;

layout(location = 0) in vec3 in_vertex;
layout(location = 1) in vec3 in_color;
layout(location = 3) in vec3 in_normal;

uniform vec3 geomOffset=vec3(0.0, 0.0, 0.0);
uniform int pertrubateMode;
uniform vec3 ncam_pos;

void main(void) {
	vec3 position;
	vec3 offset = geomOffset;
	if (pertrubateMode==CUBE_DIR) {
		position = in_vertex + offset;
	}
	else if (pertrubateMode==ALONG_NORMAL) {
		position = in_vertex + normalize(in_normal)*offset[0];
	}
	else if(pertrubateMode==ALONG_RAY) {
		position = in_vertex + normalize(in_vertex - ncam_pos)*offset[0];
	}
	gl_Position = proj * vec4(position,1.0);
}

