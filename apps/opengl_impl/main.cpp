/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include <fstream>

#include <core/graphics/Window.hpp>
#include <core/view/SceneDebugView.hpp>
#include <core/view/MultiViewManager.hpp>
#include <core/system/String.hpp>
#include <core/renderer/DepthRenderer.hpp>
#include <core/raycaster/Raycaster.hpp>
#include <core/raycaster/CameraRaycaster.hpp>
#include <core/system/Utils.hpp>

#include <projects/pointbased_neural_rendering/renderer/DeepLearningPointViewOGL.hpp>
#include <projects/pointbased_neural_rendering/renderer/PbnrScene.hpp>
#include "projects/torchgl_interop/renderer/torchgl_interop.h"

#include <Windows.h>
#define PROGRAM_NAME "pbnr_sibr_app"
using namespace sibr;

const char* usage = ""
"Usage: " PROGRAM_NAME " -path <dataset-path>"    	                                "\n"
;

bool sortByDistance(sibr::InputCamera lhs, sibr::InputCamera rhs, Vector3f ref_point) {
	float lhs_dist = (lhs.position() - ref_point).squaredNorm();
	float rhs_dist = (rhs.position() - ref_point).squaredNorm();
	return lhs_dist < rhs_dist;
};

int main(int ac, char** av) {
	torch::NoGradGuard no_grad_guard;
	std::cout << std::setprecision(6);
	// Parse Commad-line Args
	CommandLineArgs::parseMainArgs(ac, av);
	PbnrAppArgs myArgs;
	myArgs.displayHelpIfRequired();
	
	// Window setup
	sibr::Window window(PROGRAM_NAME, sibr::Vector2i(50, 50), myArgs);
	sibr::Window::Ptr winptr;
	winptr.reset(&window);
	window.makeContextCurrent();
		
	Vector2u neuralRenderResolutionT(200, 200);
	auto _outTex = std::make_shared<sibr::Texture2DRGB32F>(sibr::ImageRGB32F(neuralRenderResolutionT[0], neuralRenderResolutionT[1], sibr::Vector3f(1.0f, 0.7f, 0.7f)),  SIBR_GPU_LINEAR_SAMPLING);
	auto _copyToOutTex = std::make_shared<CopyToTextureOp>(_outTex->handle());

	PbnrScene::Ptr scene(new PbnrScene(myArgs, myArgs.preprocess_mode));
	if (myArgs.preprocess_mode) {
		return 0;
	}

	// Raycaster.
	std::shared_ptr<sibr::Raycaster> raycaster = std::make_shared<sibr::Raycaster>();
	raycaster->init();
	raycaster->addMesh(scene->proxies()->proxy());

	Vector2u neuralRenderResolution(int(myArgs.texture_width),
	                                int(myArgs.texture_width/1.5));
	Vector2u totalResolution;
	if (myArgs.debug_mode) {
		totalResolution[0] = 3 * neuralRenderResolution[0];
		totalResolution[1] = 4 * neuralRenderResolution[1];
	}
	else {
		totalResolution = neuralRenderResolution;
	}

	// Camera handler for main view.
	sibr::InteractiveCameraHandler::Ptr generalCamera(new InteractiveCameraHandler());
	generalCamera->setup(scene->cameras()->inputCameras(),
		                 Viewport(0, 0, neuralRenderResolution[0], neuralRenderResolution[1]),
		                 raycaster, {0.1, 100.0});

	const std::shared_ptr<sibr::DeepLearningPointViewOGL>
	    deepLearningPointView(new DeepLearningPointViewOGL(winptr,
														   scene,
	                                                       neuralRenderResolution,
		                                                   totalResolution,
		                                                   generalCamera,
														   myArgs.ogl_data_path,
														   myArgs.splat_layers,
	                                                       myArgs.tensorboard_path.get() + "/neural_renderer/model_" + myArgs.iteration.get(),
														   myArgs.input_cams,
			                                               myArgs.debug_mode));
	// Add views to mvm.
	MultiViewManager        multiViewManager(window, false);

	multiViewManager.addIBRSubView("DL view", deepLearningPointView, totalResolution, ImGuiWindowFlags_ResizeFromAnySide);
	multiViewManager.addCameraForView("DL view", generalCamera);


	if (myArgs.leave_one_out) {
		std::string outpathd = myArgs.outPath;

		sibr::RenderTargetRGBA32F::Ptr outFrame;
		outFrame.reset(new RenderTargetRGBA32F(neuralRenderResolution[0], neuralRenderResolution[1]));
		sibr::ImageRGBA32F::Ptr outImage;
		outImage.reset(new ImageRGBA32F(neuralRenderResolution[0], neuralRenderResolution[1]));

		for (int idx = 0; idx < scene->cameras()->inputCameras().size(); idx++) {
			deepLearningPointView->exclude_view = idx;
			outFrame->clear();
			sibr::Camera::Ptr view_cam = scene->cameras()->inputCameras()[idx];
			deepLearningPointView->onRenderIBR(*outFrame, *view_cam);
			std::ostringstream ssZeroPad;
			ssZeroPad << std::setw(8) << std::setfill('0') << idx;
			std::string outFileName = outpathd + "/ours_interactive/renders/" + scene->cameras()->inputCameras()[idx]->name();
			outFrame->readBack(*outImage);
			outImage->save(outFileName, false);

			ImageRGB32F image1;
			image1 = tensorToIm(scene->_images[idx] * scene->_exp_coefs[idx]);
			outFileName = outpathd + "/ours_interactive/gt/" + scene->cameras()->inputCameras()[idx]->name();
			image1.save(outFileName);

		}
		exit(0);
	}


	if (myArgs.pathFile.get() != "") {
		generalCamera->getCameraRecorder().loadPath(myArgs.pathFile.get(), neuralRenderResolution[0], neuralRenderResolution[1]);
		generalCamera->getCameraRecorder().recordOfflinePath(myArgs.outPath, multiViewManager.getIBRSubView("DL view"), "dl");
		if (!myArgs.noExit)
			exit(0);
	}

	// Main looooooop.
	while (window.isOpened()) {

		sibr::Input::poll();
		window.makeContextCurrent();
		if (sibr::Input::global().key().isPressed(sibr::Key::Escape)) {
			window.close();
		}

		multiViewManager.onUpdate(sibr::Input::global());
		multiViewManager.onRender(window);

		window.swapBuffer();
		CHECK_GL_ERROR;
	}

	return EXIT_SUCCESS;
}
