import torch.nn as nn
from model_defs.netblocs import ConvModule, FixupResidualChain


class FixUpResNet(nn.Module):
    def __init__(self, in_channels, internal_depth, blocks, kernel_size, dropout):
        super(FixUpResNet, self).__init__()

        self.encoder = nn.Sequential(
                           ConvModule(in_channels, internal_depth, ksize=kernel_size, pad=True, activation="relu", norm_layer=None, padding_mode="reflect"),
                           nn.Dropout(dropout),
                           FixupResidualChain(internal_depth, ksize=kernel_size, depth=int(blocks/4), padding_mode="reflect", dropout=dropout),
                       )

        self.decoder = nn.Sequential(
                           FixupResidualChain(internal_depth, ksize=kernel_size, depth=int(blocks/4), padding_mode="reflect", dropout=dropout),
                           ConvModule(internal_depth, 3, ksize=kernel_size, pad=True, activation=None, norm_layer=None, padding_mode="reflect")
                       )

    def forward(self, x, w):
        x_encoded = self.encoder(x)
        x_scaled = (x_encoded*w).sum(dim=0, keepdim=True)/(w.sum(dim=0, keepdim=True)+0.0000001)
        x_out = self.decoder(x_scaled)
        return x_out