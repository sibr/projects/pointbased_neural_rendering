#version 420

uniform mat4 proj;
uniform mat4 world2view;

layout(location = 0) in vec3 in_vertex;

out vec3 vertex_coord;

void main(void) {
	vec4 viewPos = proj * world2view * vec4(in_vertex, 1.0);
	gl_Position = viewPos; 
	vertex_coord  = in_vertex;
}
