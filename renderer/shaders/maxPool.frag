#version 420

out float out_val;
uniform int bestImprId;

in vec2 vertex_coord;

layout(binding=1) uniform sampler2DArray individualScores;
layout(binding=2) uniform sampler2D currentScore;

void main(void) {

	float score = texture(currentScore, vertex_coord).x;
	float proposedScore = texture(individualScores, vec3(vertex_coord.xy,bestImprId)).x;
	out_val=max(score,proposedScore);
}
