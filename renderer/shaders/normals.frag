#version 420

in vec3 GtoF_normal;

out vec4 FragColor;

void main()
{
    FragColor = vec4(GtoF_normal, 1.0);
} 