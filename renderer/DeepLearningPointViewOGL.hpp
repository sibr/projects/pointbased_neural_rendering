/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */

#pragma once
#include "Config.hpp"
#include <core/view/MultiMeshManager.hpp>
#include "PbnrScene.hpp"
#include <core/graphics/Shader.hpp>
#include <core/graphics/Texture.hpp>
#include "torch/torch.h"
#include "projects/torchgl_interop/renderer/CopyToTextureOp.h"
#include "projects/torchgl_interop/renderer/torchgl_interop.h"
#include "projects/torchgl_interop/renderer/TextureArrayInputOp.h"
#include "./p3d_rasterizer/soft_depth_test.h"

namespace sibr
{
	class SIBR_EXP_PBNR_EXPORT DeepLearningPointViewOGL : public ViewBase
	{
	public:
		DeepLearningPointViewOGL(sibr::Window::Ptr window,
								 const PbnrScene::Ptr scene,
		                         const Vector2u& neuralRenderResolution,
		                         const Vector2u& totalResolution,
		                         const InteractiveCameraHandler::Ptr& camHandle,
								 const std::string& ogl_data_path,
								 const int splatLayers,
		                         const std::string& model_path,
		                         const int input_cams,
		                         const int debug_mode);
		virtual void onUpdate(Input& input, const Viewport& viewport);
		virtual void onRenderIBR(sibr::IRenderTarget& dst, const sibr::Camera& eye);
		int exclude_view = -1;
	private:

		void loadShaders();
		void prepareLayeredTexture(sibr::Texture2DArrayRGBA32F::Ptr& tex, const Vector2u& res, int layerCount);
		void prepareFramebuffer(GLuint& fb_handle, int numAttachments);
		void buildFeatureBuffer(const std::string& ogl_data_path);

		std::vector<int> getNClosestCameras(const sibr::Camera& ref_cam, int N);

		sibr::Window::Ptr _window;

		torch::jit::script::Module _neural_renderer;
		sibr::PbnrScene::Ptr _scene;
		InteractiveCameraHandler::Ptr _camHandle;
		Vector2u _totalResolution;
		Vector2u _neuralRenderResolution;
		int _layerCount;
		int _compositingLayerCount;
		int _totalVertexCount;
		int _activeCamCount;
		int _featureCount;
		int _featureTextureCount;
		std::vector<int> _vertexCounts;
		std::vector<int> _cumVertexCount;

		GLShader _depth_shader;
		GLShader _minmax_mip_shader;
		GLShader _ewa_point_shader;
		GLShader _compositing_shader;
		GLShader _summary_shader;

		GLuniform<sibr::Matrix4f> _depthViewMatrix;
		GLuniform<sibr::Matrix4f> _depthProjMatrix;
		GLuniform<sibr::Vector3f> _depthCamPos;

		GLuniform<sibr::Matrix4f> _viewMatrix;
		GLuniform<sibr::Matrix4f> _projMatrix;
		
		struct CameraUBOInfos
		{
			Matrix4f viewMatrix;
			Matrix4f projMatrix;
			Vector4f position; // 4D to avoid buffer packing problems
		};

		GLuniform<int> _uniform_ewa_splatLayerCount;
		GLuniform<sibr::Vector3f> _uniform_ewa_outCamPosition;
		GLuniform<sibr::Vector2f> _uniform_ewa_depthBounds;
		std::vector<CameraUBOInfos> _inCameras; 
		GLuint _inCamBuffer;
		GLuniform<int> _uniform_compositing_splatLayerCount;
		GLuniform<int> _uniform_compositing_featureCount;
		GLuniform<int> _uniform_minmax_level;

		GLuint _proxyFramebuffer;
		GLuint _splatFramebuffer;
		GLuint _compositingFramebuffer;

		sibr::RenderTargetRGBA32F::Ptr _proxyDepthTex;
		sibr::Texture2DArrayRGBA32F::Ptr _splatColorTex;
		sibr::Texture2DArrayRGBA32F::Ptr _splatAlphaDepthTex;
		sibr::Texture2DArrayRGBA32F::Ptr _splatDistortionTex;
		std::vector<sibr::Texture2DArrayRGBA32F::Ptr> _splatFeatsTexVec;
		sibr::Texture2DArrayRGBA32F::Ptr _compositingTex;

		GLuint _vaoFeatsID;
		GLuint _bufferFeatsID;

		std::shared_ptr<TextureArrayInputOp> _compositing_texInputOp;
		std::shared_ptr<TextureArrayInputOp> _alphaDepth_texInputOp;

		torch::Tensor _compositingTensor;
		torch::Tensor _depth_gmmsTensor;
		torch::Tensor _num_gmmsTensor;

		std::shared_ptr<CopyToTextureOp> _copyToOutTex;
		Texture2DRGB32F::Ptr _outTex;

	};
}