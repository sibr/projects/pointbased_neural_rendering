/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */

#include "DeepLearningPointViewOGL.hpp"
#include <torch/script.h>

#define BUFFER_OFFSET(i) ((void*)(i))

namespace sibr
{
	DeepLearningPointViewOGL::DeepLearningPointViewOGL(
		sibr::Window::Ptr window,
		const PbnrScene::Ptr scene,
		const Vector2u& neuralRenderResolution,
		const Vector2u& totalResolution,
		const InteractiveCameraHandler::Ptr& camHandle,
		const std::string& ogl_data_path,
		const int splatLayers,
		const std::string& model_path,
		const int input_cams,
		const int debug_mode) :
		_window(window), _scene(scene), _camHandle(camHandle), _totalResolution(totalResolution), _neuralRenderResolution(neuralRenderResolution), _layerCount(splatLayers)
	{
		torch::NoGradGuard no_grad_guard;
		
		try {
			// Deserialize the ScriptModule from a file using torch::jit::load().
			_neural_renderer = torch::jit::load(model_path);
			_neural_renderer.to(torch::kCUDA);
			std::cout << "Successfuly loaded model" << std::endl;
		}
		catch (const c10::Error& e) {
			std::cerr << e.msg() << std::endl;
			std::cerr << "error loading the model\n";
			SIBR_ERR;
		}

		//_outTex.reset(new Texture2DRGB32F(sibr::ImageRGB32F(_totalResolution[0], _totalResolution[1]), SIBR_GPU_LINEAR_SAMPLING));
		_outTex = std::make_shared<sibr::Texture2DRGB32F>(sibr::ImageRGB32F(_totalResolution[0], _totalResolution[1], sibr::Vector3f(1.0f, 0.7f, 0.7f)), SIBR_GPU_LINEAR_SAMPLING);
		_copyToOutTex = std::make_shared<CopyToTextureOp>(_outTex->handle());

		Vector2u proxyDepthRes = _neuralRenderResolution / 16;

		//SIBR_LOG << "proxyDepthRes: " << proxyDepthRes << std::endl;

		_activeCamCount = input_cams;
		_featureCount = _scene->_features[0].sizes()[1];
		_featureTextureCount = int(ceil(_featureCount / 3.f));
		const int splatLayerCount = _layerCount * _activeCamCount;
		_compositingLayerCount = _activeCamCount * (1 + _featureTextureCount);


		loadShaders();
		_depthViewMatrix.init(_depth_shader, "viewMatrix");
		_depthProjMatrix.init(_depth_shader, "projMatrix");
		_depthCamPos.init(_depth_shader, "camPos");
		
		_uniform_minmax_level.init(_minmax_mip_shader, "level");
		
		_viewMatrix.init(_ewa_point_shader, "viewMatrix");
		_projMatrix.init(_ewa_point_shader, "projMatrix");
		_projMatrix.init(_ewa_point_shader, "projMatrix");
		_uniform_ewa_splatLayerCount.init(_ewa_point_shader, "splatLayerCount");
		_uniform_ewa_splatLayerCount = _layerCount;
		_uniform_ewa_outCamPosition.init(_ewa_point_shader, "outCamPosition");
		_uniform_ewa_depthBounds.init(_ewa_point_shader, "depthBounds");

		_inCameras.clear();
		_inCameras.resize(_activeCamCount);
		glGenBuffers(1, &_inCamBuffer);
		glBindBuffer(GL_UNIFORM_BUFFER, _inCamBuffer);
		glBufferData(GL_UNIFORM_BUFFER, sizeof(CameraUBOInfos) * _activeCamCount, &_inCameras[0], GL_DYNAMIC_DRAW);
		glBindBuffer(GL_UNIFORM_BUFFER, 0);

		_uniform_compositing_splatLayerCount.init(_compositing_shader, "splatLayerCount");
		_uniform_compositing_splatLayerCount = _layerCount;
		_uniform_compositing_featureCount.init(_compositing_shader, "featureCount");
		_uniform_compositing_featureCount = _featureTextureCount;

		CHECK_GL_ERROR;

		buildFeatureBuffer(ogl_data_path);

		_proxyDepthTex.reset(new sibr::RenderTargetRGBA32F(proxyDepthRes.x(), proxyDepthRes.y()));
		glBindTexture(GL_TEXTURE_2D, _proxyDepthTex->handle());
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glGenerateMipmap(GL_TEXTURE_2D); // allocate the space already
		glBindTexture(GL_TEXTURE_2D, 0);

		prepareFramebuffer(_proxyFramebuffer, 1);

		//--------------------------------------------------------------------------------------
		// prepare layered rendering
		// SIBR does not support layered rendertargets, so this needs to be done manually

		prepareLayeredTexture(_splatColorTex, _neuralRenderResolution, splatLayerCount);
		prepareLayeredTexture(_splatAlphaDepthTex, _neuralRenderResolution, splatLayerCount);
		prepareLayeredTexture(_splatDistortionTex, _neuralRenderResolution, splatLayerCount);
		for (int i = 0; i < _featureTextureCount; i++)
		{
			sibr::Texture2DArrayRGBA32F::Ptr fTex;
			prepareLayeredTexture(fTex, _neuralRenderResolution, splatLayerCount);
			_splatFeatsTexVec.push_back(fTex);
		}
		prepareLayeredTexture(_compositingTex, _neuralRenderResolution, _compositingLayerCount);


		prepareFramebuffer(_splatFramebuffer, 3 + _featureTextureCount);
		prepareFramebuffer(_compositingFramebuffer, 1);

		glBindFramebuffer(GL_FRAMEBUFFER, _splatFramebuffer);
		glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, _splatColorTex->handle(), 0);
		glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, _splatAlphaDepthTex->handle(), 0);
		glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, _splatDistortionTex->handle(), 0);
		for (int i = 0; i < _featureTextureCount; i++)
		{
			glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3 + i, _splatFeatsTexVec[i]->handle(), 0);
		}
		CHECK_GL_ERROR;

		glBindFramebuffer(GL_FRAMEBUFFER, _compositingFramebuffer);
		glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, _compositingTex->handle(), 0);
		CHECK_GL_ERROR;

		glBindFramebuffer(GL_FRAMEBUFFER, 0);


		//--------------------------------------------------------------------------------------
		// prepare texture views for transferring output texture arrays to pytorch tensors

		SIBR_LOG << "Preparing OGL-Pytorch bridge..." << std::endl;

		_compositing_texInputOp = std::make_shared<TextureArrayInputOp>(_window, _compositingTex->handle(), 4, _compositingLayerCount);
		_alphaDepth_texInputOp = std::make_shared<TextureArrayInputOp>(_window, _splatAlphaDepthTex->handle(), 4, splatLayerCount);
		
		_compositingTensor = torch::zeros(
			{ 1, 4 * _compositingLayerCount, _compositingTex->h(), _compositingTex->w() }, 
			torch::TensorOptions().dtype(torch::kFloat32).device(torch::kCUDA, 0)).contiguous();

		_depth_gmmsTensor = torch::zeros(
			{ 1, 4 * splatLayerCount, _compositingTex->h(), _compositingTex->w() },
			torch::TensorOptions().dtype(torch::kFloat32).device(torch::kCUDA, 0)).contiguous();

		_num_gmmsTensor = (_layerCount + 1) * torch::ones(
			{ _activeCamCount, _compositingTex->h(), _compositingTex->w(), 1 },
			torch::TensorOptions().dtype(torch::kInt32).device(torch::kCUDA, 0)).contiguous();

	}

	
	void DeepLearningPointViewOGL::prepareLayeredTexture(sibr::Texture2DArrayRGBA32F::Ptr& tex, const Vector2u& res, int layerCount)
	{
		tex.reset(new sibr::Texture2DArrayRGBA32F(res.x(), res.y(), layerCount, SIBR_GPU_LINEAR_SAMPLING));
		CHECK_GL_ERROR;
		glBindTexture(GL_TEXTURE_2D_ARRAY, tex->handle());
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
		CHECK_GL_ERROR;
	}

	void DeepLearningPointViewOGL::prepareFramebuffer(GLuint& fb_handle, int numAttachments)
	{	
		glGenFramebuffers(1, &fb_handle);
		glBindFramebuffer(GL_FRAMEBUFFER, fb_handle);
		GLenum drawbuffers[SIBR_MAX_SHADER_ATTACHMENTS];
		for (uint i = 0; i < SIBR_MAX_SHADER_ATTACHMENTS; i++)
			drawbuffers[i] = GL_COLOR_ATTACHMENT0 + i;
		glDrawBuffers(numAttachments, drawbuffers);
		CHECK_GL_ERROR;
	}


	void DeepLearningPointViewOGL::loadShaders()
	{
		SIBR_LOG << "Re-loading shaders... ";
		
		const std::string shaderSrcPath = sibr::getBinDirectory() + "/../../src/projects/pointbased_neural_rendering/renderer/shaders/";
		const std::string shaderDstPath = sibr::getShadersDirectory() + "/pbnr/";
		const auto files = sibr::listFiles(shaderSrcPath, false, false, { "vert", "frag", "geom" });
		for (const auto& file : files) {
			sibr::copyFile(shaderSrcPath + file, shaderDstPath + file, true);
		}
		
		GLShader::Define::List defines;
		defines.emplace_back("IN_CAM_COUNT", _activeCamCount);

		_depth_shader.init("DepthShader",
			sibr::loadFile(sibr::getShadersDirectory() + "/pbnr/proxy_depth.vert"),
			sibr::loadFile(sibr::getShadersDirectory() + "/pbnr/proxy_depth.frag"));
		
		_minmax_mip_shader.init("MinMaxMIPShader",
			sibr::loadFile(sibr::getShadersDirectory() + "/pbnr/vertex2D.vert"),
			sibr::loadFile(sibr::getShadersDirectory() + "/pbnr/minmax_mip.frag"));

		_ewa_point_shader.init("EWAPointShader",
			sibr::loadFile(sibr::getShadersDirectory() + "/pbnr/ewa_point.vert"),
			sibr::loadFile(sibr::getShadersDirectory() + "/pbnr/ewa_point.frag"),
			sibr::loadFile(sibr::getShadersDirectory() + "/pbnr/ewa_point.geom", defines));

		_compositing_shader.init("CompositingShader",
			sibr::loadFile(sibr::getShadersDirectory() + "/pbnr/vertex2D.vert"),
			sibr::loadFile(sibr::getShadersDirectory() + "/pbnr/compositing.frag"),
			sibr::loadFile(sibr::getShadersDirectory() + "/pbnr/compositing.geom"));

		_summary_shader.init("SummaryShader",
			sibr::loadFile(sibr::getShadersDirectory() + "/pbnr/vertex2D.vert"),
			sibr::loadFile(sibr::getShadersDirectory() + "/pbnr/summary.frag"));

		std::cout << "Done." << std::endl;
	}


	std::vector<int> DeepLearningPointViewOGL::getNClosestCameras(const sibr::Camera& ref_cam, int N) {
		const Vector3f pos = ref_cam.position();
		std::vector<std::tuple<float, int>> dist_cam_vector;
		for (int i = 0; i < _scene->cameras()->inputCameras().size(); i++) {
			sibr::InputCamera::Ptr cam = _scene->cameras()->inputCameras()[i];
			dist_cam_vector.push_back(std::tuple<float, int>((pos - cam->position()).norm(), i));
		}
		std::sort(dist_cam_vector.begin(), dist_cam_vector.end());
		std::vector<int> topN;
		for (int i = 0; i < N; i++) {
			topN.push_back(std::get<1>(dist_cam_vector[i]));
		}
		return topN;
	}


	void DeepLearningPointViewOGL::buildFeatureBuffer(const std::string& ogl_data_path)
	{
		SIBR_LOG << "Collecting data..." << std::endl;
		
		// position + color + normal + uncertainty + features
		int numEntries = 3 + 3 + 3 + 1 + 3 * _featureTextureCount; 

		int camCount = _scene->cameras()->inputCameras().size();
		_totalVertexCount = 0;
		_cumVertexCount.push_back(0);

		for (int c = 0; c < camCount; c++)
		{
			int vCount = std::get<0>(_scene->_points3d_color_tuple[c]).sizes()[0];
			_vertexCounts.push_back(vCount);
			_totalVertexCount += vCount;
			_cumVertexCount.push_back(_totalVertexCount);
		}

		const int dataSize = numEntries * _totalVertexCount;
		float* data = new float[dataSize];
		bool data_file_exists = boost::filesystem::exists(ogl_data_path);
		bool data_file_empty = ogl_data_path == "";

		if (data_file_empty || !data_file_exists)
		{
			SIBR_LOG << "OGL data does not exist, will create from scratch. This might take a while..." << std::endl;
			if (data_file_empty) SIBR_LOG << "Provide ogl_data_path to speed up loading." << std::endl;

#pragma omp parallel for 
			for (int c = 0; c < camCount; c++)
			{
				std::tuple<torch::Tensor, torch::Tensor> packed_tensors = _scene->_points3d_color_tuple[c];
				torch::Tensor point_cloud = std::get<0>(packed_tensors);
				torch::Tensor point_colors = std::get<1>(packed_tensors) * _scene->_exp_coefs[c];

				torch::Tensor pc_cpu = point_cloud.cpu();
				torch::Tensor colors_cpu = point_colors.cpu();
				torch::Tensor features_cpu = _scene->_features[c].cpu();
				torch::Tensor normals_cpu = (_scene->_normals[c] / _scene->_normals[c].norm(2, 1, true)).cpu();
				torch::Tensor uncertainties_cpu = _scene->_uncertainties[c].cpu();

				for (int i = 0; i < _vertexCounts[c]; i++)
				{
					int startIdx = numEntries * (_cumVertexCount[c] + i);
					data[startIdx + 0] = pc_cpu[i][0].data<float>()[0];
					data[startIdx + 1] = pc_cpu[i][1].data<float>()[0];
					data[startIdx + 2] = pc_cpu[i][2].data<float>()[0];
					data[startIdx + 3] = colors_cpu[i][0].data<float>()[0];
					data[startIdx + 4] = colors_cpu[i][1].data<float>()[0];
					data[startIdx + 5] = colors_cpu[i][2].data<float>()[0];
					data[startIdx + 6] = normals_cpu[i][0].data<float>()[0];
					data[startIdx + 7] = normals_cpu[i][1].data<float>()[0];
					data[startIdx + 8] = normals_cpu[i][2].data<float>()[0];
					data[startIdx + 9] = uncertainties_cpu[i][0].data<float>()[0];
					for (int f = 0; f < _featureCount; f++)
					{
						data[startIdx + 10 + f] = features_cpu[i][f].data<float>()[0];
					}
				}
			}

			if (ogl_data_path != "")
			{
				SIBR_LOG << "Writing out OGL data." << std::endl;
				std::ofstream fout(ogl_data_path, std::ios::out | std::ios::binary);
				fout.write((char*)&data[0], dataSize * sizeof(float));
				fout.close();
			}
		}
		else
		{
			SIBR_LOG << "Loading OGL data from disk." << std::endl;
			std::fstream fin(ogl_data_path, std::ios::in | std::ios::binary);
			fin.read((char*)&data[0], dataSize * sizeof(float));
			fin.close();
		}

		SIBR_LOG << "Uploading data..." << std::endl;

		glGenVertexArrays(1, &_vaoFeatsID);
		glGenBuffers(1, &_bufferFeatsID);

		glBindVertexArray(_vaoFeatsID);
		CHECK_GL_ERROR;

		glBindBuffer(GL_ARRAY_BUFFER, _bufferFeatsID);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * dataSize, data, GL_STATIC_DRAW);
		CHECK_GL_ERROR;

		// position
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, numEntries * sizeof(float), BUFFER_OFFSET(0));
		glEnableVertexAttribArray(0);
	
		// color
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, numEntries * sizeof(float), BUFFER_OFFSET(3 * sizeof(float)));
		glEnableVertexAttribArray(1);

		// normal
		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, numEntries * sizeof(float), BUFFER_OFFSET(6 * sizeof(float)));
		glEnableVertexAttribArray(2);

		// uncertainty
		glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, numEntries * sizeof(float), BUFFER_OFFSET(9 * sizeof(float)));
		glEnableVertexAttribArray(3);

		// features
		for (int i = 0; i < _featureTextureCount; i++)
		{
			int vaoIdx = 4 + i;
			glVertexAttribPointer(vaoIdx, 3, GL_FLOAT, GL_FALSE, numEntries * sizeof(float), BUFFER_OFFSET((10 + 3 * i) * sizeof(float)));
			glEnableVertexAttribArray(vaoIdx);
		}

		glBindVertexArray(0);

		delete[] data;
	}



	void DeepLearningPointViewOGL::onRenderIBR(sibr::IRenderTarget& dst, const sibr::Camera& eye)
	{		
		torch::NoGradGuard no_grad_guard;
		
		_viewMatrix = eye.view();
		_projMatrix = eye.proj();
		_uniform_ewa_outCamPosition = eye.position();

		//----------------------------------------------------
		// proxy rasterization
		_proxyDepthTex->bind();
		glViewport(0, 0, _proxyDepthTex->w(), _proxyDepthTex->h());
		glClearColor(0, 0, 0, 0);
		glClearDepth(1.0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
		_depth_shader.begin();

		_depthViewMatrix.set(eye.view());
		_depthProjMatrix.set(eye.proj());
		_depthCamPos.set(eye.position());

		_scene->proxies()->proxy().render(true, true);
		
		_depth_shader.end();
		_proxyDepthTex->unbind();

		// build min-max mip map to determine depth range
		int levelCount = int(log(std::max(_proxyDepthTex->w(), _proxyDepthTex->w())) / log(2)) + 1; // TODO: move outside the loop

		glBindFramebuffer(GL_FRAMEBUFFER, _proxyFramebuffer);
		_minmax_mip_shader.begin();
	
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, _proxyDepthTex->handle());
		CHECK_GL_ERROR;

		for (int l = 1; l < levelCount; l++)
		{
			_uniform_minmax_level.set(l);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, _proxyDepthTex->handle(), l);
			RenderUtility::renderScreenQuad();
		}
		_minmax_mip_shader.end();
		CHECK_GL_ERROR;
		
		sibr::Vector4f rawMinMax;
		glGetTexImage(GL_TEXTURE_2D, levelCount-1, GL_RGBA, GL_FLOAT, &rawMinMax);
		
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glBindTexture(GL_TEXTURE_2D, 0);
		CHECK_GL_ERROR;

		// extend depth range by 5%
		float overshoot = 0.05f * (rawMinMax.y() - rawMinMax.x());
		sibr::Vector2f minMax = Vector2f(rawMinMax.x() - overshoot, rawMinMax.y() + overshoot);
		
		//SIBR_LOG << "depth minmax: " << minMax << std::endl;
			
		//----------------------------------------------------
		// camera selection
		std::vector<int> closestCams;
		//closestCams = getNClosestCameras(eye, _activeCamCount);
		//closestCams = _scene->getNBestCoverageCamerasGPU(eye, _neuralRenderResolution, _activeCamCount, 1.0 / 4.0, true, exclude_view);
		closestCams = _scene->getNBestCoverageCameras(eye, _neuralRenderResolution, _activeCamCount, 1.0 / 16.0, true, exclude_view);
		//SIBR_LOG << "=== closest cams:" << closestCams << std::endl;
		
		std::sort(closestCams.begin(), closestCams.end());

		for (int idx = 0; idx < _scene->cameras()->inputCameras().size(); idx++) {
			float alpha = 0.05;
			float w = 0.0;
			if (std::find(closestCams.begin(), closestCams.end(), idx) != closestCams.end()) {
				w = 1.0;
			}
			float new_w = alpha * w + (1.0 - alpha) * std::get<0>(_scene->_cam_weights_idx[idx]);
			if (exclude_view != -1) {
				_scene->_cam_weights_idx[idx] = std::make_tuple(w, idx);
			}
			else {
				_scene->_cam_weights_idx[idx] = std::make_tuple(new_w, idx);
			}
		}

		std::vector<std::tuple<float, int>> best_cams_sorted(_activeCamCount);
		std::partial_sort_copy(std::begin(_scene->_cam_weights_idx), std::end(_scene->_cam_weights_idx), std::begin(best_cams_sorted), std::end(best_cams_sorted),
			std::greater<std::tuple<float, int>>());

		torch::Tensor w = torch::tensor({}).cuda();

		// populate draw commands & get camera weights
		std::vector<int> startIndices;
		std::vector<int> counts;

		int cIdx = 0;

		for (std::tuple<float, int> cam_w_idx : best_cams_sorted)
		{
			int camId = std::get<1>(cam_w_idx);
			float w_cam = std::get<0>(cam_w_idx);
			int startIdx = _cumVertexCount[camId];
			startIndices.push_back(startIdx);
			counts.push_back(_cumVertexCount[camId + 1] - startIdx);
			w = torch::cat(torch::TensorList({ w, torch::tensor({w_cam}).unsqueeze(0).cuda() }), 0);

			InputCamera::Ptr cam = _scene->cameras()->inputCameras()[camId];
			_inCameras[cIdx].viewMatrix = cam->view();
			_inCameras[cIdx].projMatrix = cam->proj();
			_inCameras[cIdx].position = cam->position().homogeneous();
			cIdx++;
		}

		if (exclude_view == -1) {
			w = w - (w.min() - 0.05);
		}
		w = w / w.sum();
		
		//----------------------------------------------------
		// layered point splatting

		glViewport(0, 0, _neuralRenderResolution.x(), _neuralRenderResolution.y());
		glEnable(GL_PROGRAM_POINT_SIZE);
		glEnable(0x8861); // enable point sprites
		glEnable(GL_BLEND);
		glBlendEquation(GL_FUNC_ADD);
		glBlendFunci(0, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);		// alpha-blend colors
		glBlendFunci(1, GL_ONE, GL_ONE);							// accumulate log(1-alpha) and depth
		glBlendFunci(2, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);		// alpha-blend distortions
		for (int i = 0; i < _featureTextureCount; i++)
		{
			glBlendFunci(3+i, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); // alpha-blend features
		}

		glDisable(GL_DEPTH_TEST);
		glDepthMask(GL_FALSE);
		CHECK_GL_ERROR;

		glBindFramebuffer(GL_FRAMEBUFFER, _splatFramebuffer);
		CHECK_GL_ERROR;

		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		_ewa_point_shader.begin();
		_viewMatrix.send();
		_projMatrix.send();
		_uniform_ewa_splatLayerCount.send();
		_uniform_ewa_outCamPosition.send();
		_uniform_ewa_depthBounds.set(minMax);

		glBindBuffer(GL_UNIFORM_BUFFER, _inCamBuffer);
		glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(CameraUBOInfos) * _activeCamCount, &_inCameras[0]);
		glBindBufferBase(GL_UNIFORM_BUFFER, 6, _inCamBuffer);
		glBindBuffer(GL_UNIFORM_BUFFER, 0);
		
		glBindVertexArray(_vaoFeatsID);
		glMultiDrawArrays(GL_POINTS, &startIndices[0], &counts[0], closestCams.size());
		glBindVertexArray(0);
		CHECK_GL_ERROR;
		
		_ewa_point_shader.end();
		CHECK_GL_ERROR;

		glDisable(GL_PROGRAM_POINT_SIZE);
		glDisable(GL_BLEND);
		glDisable(0x8861);
		glDepthMask(GL_TRUE);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		
		glFinish();

		//----------------------------------------------------
		// layer compositing

		glBindFramebuffer(GL_FRAMEBUFFER, _compositingFramebuffer);
		CHECK_GL_ERROR;

		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		_compositing_shader.begin();
		_uniform_compositing_splatLayerCount.send();
		_uniform_compositing_featureCount.send();

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D_ARRAY, _splatColorTex->handle());
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D_ARRAY, _splatAlphaDepthTex->handle());
		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D_ARRAY, _splatDistortionTex->handle());
		for (int i = 0; i < _featureTextureCount; i++)
		{
			glActiveTexture(GL_TEXTURE3 + i);
			glBindTexture(GL_TEXTURE_2D_ARRAY, _splatFeatsTexVec[i]->handle());
		}
		
		// start one thread per output layer,
		// each will create a viewport quad in the geometry shader to achieve fully parallel rendering
		glDrawArrays(GL_POINTS, 0, _activeCamCount * (1 + _featureTextureCount));
		
		_compositing_shader.end();


		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		//----------------------------------------------------
		// OGL textures to Pytorch tensors & some re-organization

		_compositing_texInputOp->Compute(_compositingTensor);
		_alphaDepth_texInputOp->Compute(_depth_gmmsTensor);


		// extract the first 3 channels of each texture layer and use as features
		torch::Tensor raw_features = _compositingTensor.view({ 1, _compositingLayerCount, 4, _compositingTex->h(), _compositingTex->w() }).contiguous();
		torch::Tensor features_stack = raw_features.slice(2, 0, 3).contiguous();
		features_stack = features_stack.view({_activeCamCount, 3 + _featureCount, _compositingTex->h(), _compositingTex->w() }).contiguous();
		features_stack = torch::clamp(features_stack, 0.0, 1.0);
		torch::Tensor lastChannel = raw_features.slice(2, 3, 4).contiguous();
		lastChannel = lastChannel.view({ _activeCamCount, 3, _compositingTex->h(), _compositingTex->w(), 1 }).contiguous();

		// extract cumulative alpha for each input view
		torch::Tensor cum_alphas = lastChannel.slice(1, 0, 1).permute({ 0, 2, 3, 1, 4 }).contiguous();
		torch::Tensor lastLayer = torch::cat( { 100000.0f * torch::ones_like(cum_alphas), cum_alphas }, 4).contiguous();
		
		// extract distortion for each input view
		torch::Tensor distortion = lastChannel.slice(1, 1, 2).contiguous();
		distortion = distortion.view({ _activeCamCount, 1, _compositingTex->h(), _compositingTex->w() }).contiguous();

		//SIBR_LOG << "distortion: " << distortion.sizes() << std::endl;

		//for (int i = 0; i < 9; i++)
		//{
		//	ImageRGB32F image;
		//	//torch::Tensor outDistortion = distortion.permute({ 0, 2, 3, 1 });
		//	torch::Tensor tmp = distortion.index({ i }).view({ 1, 1, _compositingTex->h(), _compositingTex->w() });
		//	tmp = torch::repeat_interleave(tmp, 3, 1);
		//	SIBR_LOG << "tmp: " << tmp.sizes() << std::endl;
		//	image = tensorToIm(tmp);
		//	std::stringstream stream;
		//	stream << "C:/tmptest/dist_" << i;
		//	//image.saveHDR(stream.str() + ".exr");
		//	image.save(stream.str() + ".png");
		//}

		//return;

		/*for (int i = 0; i < 9; i++)
		{
			ImageRGB32F image;
			for (int f = 0; f < 3; f++)
			{
				torch::Tensor tmp = features_stack.index({ i }).slice(0, 3*f, 3*f + 3).unsqueeze(0);
				//SIBR_LOG << "tmp: " << tmp.sizes() << std::endl;
				//continue;
				image = tensorToIm(tmp);
				//image.flipH();
				std::stringstream stream;
				stream << "C:/tmptest/img_" << i << "_" << f << "_t";
				image.saveHDR(stream.str() + ".exr");
				image.save(stream.str() + ".png");
			}
		}

		return;*/


		//----------------------------------------------------
		// soft depth test

		// normalize weighted depth and combine with last layer at infinity
		torch::Tensor depth_gmms_stack = _depth_gmmsTensor.view(
			{ -1, _layerCount, 4, _compositingTex->h(), _compositingTex->w() }).contiguous();
		torch::Tensor weightedDepth = depth_gmms_stack.slice(2, 1, 2).contiguous();
		torch::Tensor weight = depth_gmms_stack.slice(2, 3, 4).contiguous();
		torch::Tensor depth = torch::where(weight != 0, weightedDepth / weight, weightedDepth).contiguous();
		torch::Tensor alpha = depth_gmms_stack.slice(2, 0, 1).contiguous();
		alpha = 1. - torch::exp(alpha);
		depth_gmms_stack = torch::cat({ depth, alpha }, 2).permute({0, 3, 4, 1, 2}).contiguous();
		depth_gmms_stack = torch::cat({ depth_gmms_stack, lastLayer }, 3).contiguous();


		torch::Tensor prob_map;
		prob_map = SoftDepthTest(depth_gmms_stack, _num_gmmsTensor);

		w = w.view({ -1, 1, 1, 1 }).contiguous();
		w = distortion * prob_map * w;
		//w = prob_map * w;

		//SIBR_LOG << "w: " << w.sizes() << std::endl;


		//----------------------------------------------------
		// run neural renderer

		std::vector<torch::jit::IValue> run_inputNet;

		run_inputNet.push_back(features_stack);
		run_inputNet.push_back(w);

		torch::Tensor final;
		try {
			final = _neural_renderer.forward(run_inputNet).toTensor();
		}
		catch (const c10::Error& e) {
			std::cerr << e.msg() << std::endl;
			std::cerr << "Error while executing the model\n";
			SIBR_ERR;
		}

		if (exclude_view == -1) {
			final += final * (1.0 - _scene->exp_coef_mean);
		}
		final = torch::clamp(final, 0.0, 1.0);


		_copyToOutTex->Compute(final);
		
		//ImageRGB32F outImg = _outTex->readBack();
		//outImg.flipH();
		//outImg.save("C:/tmptest/outImg.png");

		//----------------------------------------------------
		// visualization

		glViewport(0, 0, dst.w(), dst.h());
		dst.clear();
		dst.bind();
		_summary_shader.begin();

		glDisable(GL_DEPTH_TEST);
		
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D_ARRAY, _splatColorTex->handle());
		//glBindTexture(GL_TEXTURE_2D_ARRAY, _splatFeatsTexVec[1]->handle());

		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D_ARRAY, _compositingTex->handle());

		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, _proxyDepthTex->handle());

		glActiveTexture(GL_TEXTURE3);
		glBindTexture(GL_TEXTURE_2D, _outTex->handle());

		glActiveTexture(GL_TEXTURE4);
		glBindTexture(GL_TEXTURE_2D_ARRAY, _splatDistortionTex->handle());


		RenderUtility::renderScreenQuad();
		_summary_shader.end();
		glBindTexture(GL_TEXTURE_2D, 0);
		dst.unbind();

		glActiveTexture(GL_TEXTURE0);
	}



	void DeepLearningPointViewOGL::onUpdate(Input& input, const Viewport& viewport)
	{
		ViewBase::onUpdate(input, viewport);

		// live shader reloading
		if (input.key().isReleased(Key::R))
		{
			loadShaders();
		}
	}

}
