import os
import sys
import math
from PIL import Image
import numpy as np
import torch
from typing import NamedTuple
from scene_loaders.colmap_loader import read_extrinsics_text, read_intrinsics_text, qvec2rotmat, read_colmap_bin_array

class CameraInfo(NamedTuple):
    uid: int
    R: np.array
    T: np.array
    FovY: np.array
    FovX: np.array
    image: np.array
    image_path: str
    image_name: str

class SceneInfo(NamedTuple):
    ply_path: str
    num_cameras: int
    cameras: list
    blacklist: list

def readFvsSceneInfo(name, path):
    ply_path = os.path.join(path, "dense/delaunay_photometric.ply")
    files_path = os.path.join(path, "dense/ibr3d_pw_0.50/")
    Ks = np.load(os.path.join(files_path, "Ks.npy"))
    Rs = np.load(os.path.join(files_path, "Rs.npy"))
    Ts = np.load(os.path.join(files_path, "ts.npy"))
    num_cameras = Ts.shape[0]
    cameras = []
    for idx in range(num_cameras):
        sys.stdout.write('\r')
        # the exact output you're looking for:
        sys.stdout.write("Reading \"{}\" | Camera {}/{}".format(name, idx, num_cameras))
        sys.stdout.flush()

        uid_str = format(idx, '08d')

        image_name = "im_" + uid_str
        image_path = os.path.join(files_path, image_name + ".jpg")
        image = Image.open(image_path)
        image = torch.from_numpy(np.array(image)) / 255.0
        image = image.permute(2, 0, 1).unsqueeze(dim=0)

        height = image.shape[2]
        width = image.shape[3]
        focal_length_x = Ks[idx, 0, 0]
        focal_length_y = Ks[idx, 1, 1]
        FovY = 2.0 * math.atan(0.5 * height / focal_length_y)
        FovX = 2.0 * math.atan(0.5 * width / focal_length_x)

        cam_info = CameraInfo(uid=idx, R=np.transpose(Rs[idx]), T=Ts[idx], FovY=FovY, FovX=FovX, image=image,
                              image_path=image_path, image_name=image_name)
        cameras.append(cam_info)
    sys.stdout.write('\n')
    scene_info = SceneInfo(ply_path=ply_path,
                           num_cameras=num_cameras,
                           cameras=cameras)
    return scene_info

def readColmapSceneInfo_Test(name, path):
    ply_path = os.path.join(path, "stereo/meshed-delaunay.ply")

    blacklist = []
    try:
        blacklist_file = os.path.join(path, "database.blacklist")
        with open(blacklist_file, "r") as f:
            blacklist = [file_name.split(".")[0] for file_name in f.read().split("\n")]
    except:
        print("No blacklist file!")


    cameras_extrinsic_file = os.path.join(path, "stereo/sparse/images.txt")
    cameras_intrinsic_file = os.path.join(path, "stereo/sparse/cameras.txt")
    cam_extrinsics = read_extrinsics_text(cameras_extrinsic_file)
    cam_intrinsics = read_intrinsics_text(cameras_intrinsic_file)

    num_cameras = len(cam_extrinsics)
    cameras = []
    for idx, key in enumerate(cam_extrinsics):
        sys.stdout.write('\r')
        # the exact output you're looking for:
        sys.stdout.write("Reading \"{}\" | Camera {}/{}".format(name, idx, len(cam_extrinsics)))
        sys.stdout.flush()
        # if key not in cam_intrinsics:
        #    continue
        extr = cam_extrinsics[key]
        intr = cam_intrinsics[extr.camera_id]
        height = intr.height
        width = intr.width

        uid = intr.id
        R = np.transpose(qvec2rotmat(extr.qvec))
        T = np.array(extr.tvec)

        focal_length_x = intr.params[0]
        focal_length_y = intr.params[1]
        FovY = 2.0 * math.atan(0.5 * height / focal_length_y)
        FovX = 2.0 * math.atan(0.5 * width / focal_length_x)

        image_path = os.path.join(path, "stereo/images", extr.name)
        image_name = os.path.basename(image_path).split(".")[0]
        image = Image.open(image_path)
        image = torch.from_numpy(np.array(image)) / 255.0
        image = image.permute(2, 0, 1).unsqueeze(dim=0)
        cam_info = CameraInfo(uid=uid, R=R, T=T, FovY=FovY, FovX=FovX, image=image,
                              image_path=image_path, image_name=image_name)
        cameras.append(cam_info)
        # photometric_depth_map = torch.tensor(
        #    read_colmap_bin_array(os.path.join(path, "colmap/stereo/stereo/depth_maps/",
        #                                       extr.name + ".photometric.bin"))).unsqueeze(dim=0).unsqueeze(dim=0)
        # geometric_depth_map = torch.tensor(
        #    read_colmap_bin_array(os.path.join(path, "colmap/stereo/stereo/depth_maps/",
        #                                       extr.name + ".geometric.bin"))).unsqueeze(dim=0).unsqueeze(dim=0)
    sys.stdout.write('\n')
    scene_info = SceneInfo(ply_path=ply_path,
                           num_cameras=num_cameras,
                           cameras=cameras,
                           blacklist=blacklist)
    return scene_info

def readColmapSceneInfo(name, path):
    ply_path = os.path.join(path, "colmap/stereo/meshed-delaunay.ply")

    blacklist = []
    try:
        blacklist_file = os.path.join(path, "colmap/database.blacklist")
        with open(blacklist_file, "r") as f:
            blacklist = [file_name.split(".")[0] for file_name in f.read().split("\n")]
    except:
        print("No blacklist file!")


    cameras_extrinsic_file = os.path.join(path, "colmap/stereo/sparse/images.txt")
    cameras_intrinsic_file = os.path.join(path, "colmap/stereo/sparse/cameras.txt")
    cam_extrinsics = read_extrinsics_text(cameras_extrinsic_file)
    cam_intrinsics = read_intrinsics_text(cameras_intrinsic_file)

    num_cameras = len(cam_extrinsics)
    cameras = []
    for idx, key in enumerate(cam_extrinsics):
        sys.stdout.write('\r')
        # the exact output you're looking for:
        sys.stdout.write("Reading \"{}\" | Camera {}/{}".format(name, idx, len(cam_extrinsics)))
        sys.stdout.flush()
        # if key not in cam_intrinsics:
        #    continue
        extr = cam_extrinsics[key]
        intr = cam_intrinsics[extr.camera_id]
        height = intr.height
        width = intr.width

        uid = intr.id
        R = np.transpose(qvec2rotmat(extr.qvec))
        T = np.array(extr.tvec)

        focal_length_x = intr.params[0]
        focal_length_y = intr.params[1]
        FovY = 2.0 * math.atan(0.5 * height / focal_length_y)
        FovX = 2.0 * math.atan(0.5 * width / focal_length_x)

        image_path = os.path.join(path, "pbnrScene/images", extr.name)
        image_name = os.path.basename(image_path).split(".")[0]
        image = Image.open(image_path)
        image = torch.from_numpy(np.array(image)) / 255.0
        image = image.permute(2, 0, 1).unsqueeze(dim=0)
        cam_info = CameraInfo(uid=uid, R=R, T=T, FovY=FovY, FovX=FovX, image=image,
                              image_path=image_path, image_name=image_name)
        cameras.append(cam_info)
        # photometric_depth_map = torch.tensor(
        #    read_colmap_bin_array(os.path.join(path, "colmap/stereo/stereo/depth_maps/",
        #                                       extr.name + ".photometric.bin"))).unsqueeze(dim=0).unsqueeze(dim=0)
        # geometric_depth_map = torch.tensor(
        #    read_colmap_bin_array(os.path.join(path, "colmap/stereo/stereo/depth_maps/",
        #                                       extr.name + ".geometric.bin"))).unsqueeze(dim=0).unsqueeze(dim=0)
    sys.stdout.write('\n')
    scene_info = SceneInfo(ply_path=ply_path,
                           num_cameras=num_cameras,
                           cameras=cameras,
                           blacklist=blacklist)
    return scene_info

def readNVMFile(path):
    with open(path) as fp:
        lines = fp.readlines()
        nvm_version = lines.pop(0)
        l = lines.pop(0)
        while l == '\n':
            l = lines.pop(0)
        num_cams = int(l)
        cameras = []
        for idx in range(num_cams):
            l = lines.pop(0).split(' ')
            image_name = l[0]
            focal_length = float(l[1])
            qvec = np.array([float(l[2]), float(l[3]),
                             float(l[4]), float(l[5])])
            pos = np.array([float(l[6]), float(l[7]), float(l[8])])
            radial_distortion = int(l[9])
            assert (radial_distortion == 0 and int(l[10]) == 0)
            cameras.append((image_name, focal_length, qvec, pos))
        return cameras

def readDeepBlending(name, path):
    ply_path = os.path.join(path, "global_mesh_with_texture", "mesh.ply")
    cams = readNVMFile(os.path.join(path, "input_camera_poses_as_nvm", "scene.nvm"))
    cameras = []
    num_cameras = len(cams)
    for idx, cam in enumerate(cams):
        sys.stdout.write('\r')
        # the exact output you're looking for:
        sys.stdout.write("Reading \"{}\" | Camera {}/{}".format(name, idx, num_cameras))
        sys.stdout.flush()

        image_path = os.path.join(path, "input_camera_poses_as_nvm", cam[0])
        image_name = os.path.basename(image_path)
        image = Image.open(image_path)
        image = torch.from_numpy(np.array(image)) / 255.0
        image = image.permute(2, 0, 1).unsqueeze(dim=0)

        height = image.shape[2]
        width = image.shape[3]
        focal_length = cam[1]
        FovY = 2.0 * math.atan(0.5 * height / focal_length)
        FovX = 2.0 * math.atan(0.5 * width / focal_length)

        R = np.transpose(qvec2rotmat(cam[2]))
        origin = cam[3]
        T = -np.matmul(np.linalg.inv(R), origin)

        cam_info = CameraInfo(uid=idx, R=R, T=T, FovY=FovY, FovX=FovX, image=image,
                              image_path=image_path, image_name=image_name)
        cameras.append(cam_info)
    sys.stdout.write('\n')
    scene_info = SceneInfo(ply_path=ply_path,
                           num_cameras=num_cameras,
                           cameras=cameras)
    return scene_info

sceneLoadTypeCallbacks = {
    "FVS": readFvsSceneInfo,
    "Colmap": readColmapSceneInfo,
    "DB": readDeepBlending
}