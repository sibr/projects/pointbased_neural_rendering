#version 420

out float out_val;

uniform mat4 sampledCamProj;
uniform vec3 sampledCamPos;
uniform vec3 sampledCamDir;
uniform vec3 currentCamPos;

in vec2 vertex_coord;

//Pos3D texture of the current view.
layout(binding=0) uniform sampler2D current_pos3D;
//Pos3D texture of the input we sample from.
layout(binding=1) uniform sampler2D sampled_pos3D;


vec3 project(vec3 point, mat4 proj) {
  vec4 p1 = proj * vec4(point, 1.0);
  vec3 p2 = (p1.xyz/p1.w);
  return (p2.xyz*0.5 + 0.5);
}

bool frustumTest(vec3 p, vec2 ndc) {
  vec3 d1 = sampledCamDir;
  vec3 d2 = p - sampledCamPos;
  return !any(greaterThan(ndc, vec2(1.0))) && dot(d1,d2)>0.0;
}

void main(void) {

	vec4 visiblePoint = texture(current_pos3D, vertex_coord);
	vec3 uvd = project(visiblePoint.xyz, sampledCamProj);
	//uvd.y= 1.0 - uvd.y;
	vec2 ndc = abs(2.0*uvd.xy-1.0);
	
	out_val=0.0;
	if (frustumTest(visiblePoint.xyz, ndc)){
		vec4 sampledPoint = texture(sampled_pos3D, uvd.xy);
		
		float dD=0.01;
		float depthThrld = length(currentCamPos - visiblePoint.xyz) * dD;
		if(length(sampledPoint.xyz-visiblePoint.xyz)<depthThrld ){
			out_val = max(min(visiblePoint.w/sampledPoint.w,1.25), 0.0);
		}
	}
}
