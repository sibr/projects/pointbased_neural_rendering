import torch
from diff_rasterization import _C

def rasterize_points(
    points,
    features,
    point_score,
    inv_cov,
    max_radius,
    image_height: int = 256,
    image_width: int = 256,
    points_per_pixel: int = 8,
    zfar: float = -0.5,
    znear: float = -0.5,
    gamma: float = None
):

    return _RasterizePoints.apply(
        points,
        features,
        point_score,
        inv_cov,
        max_radius,
        image_height,
        image_width,
        points_per_pixel,
        zfar,
        znear,
        gamma
    )


class _RasterizePoints(torch.autograd.Function):
    @staticmethod
    def forward(
        ctx,
        points,  # (P, 3)
        colors,  # (P, C)
        point_score,  # (P, 1)
        inv_cov,  # (P, 4)
        max_radius,
        image_height: int = 256,
        image_width: int = 256,
        points_per_pixel: int = 8,
        zfar: float = -0.5,
        znear: float = -0.5,
        gamma: float = None
    ):
        # TODO: Add better error handling for when there are more than
        # max_points_per_bin in any bin.
        args = (
            points,
            colors,
            point_score,
            inv_cov,
            max_radius,
            image_height,
            image_width,
            points_per_pixel,
            zfar,
            znear,
            gamma
        )
        idx, color, k_idxs, depth_gmms, num_gmms, point_score = _C.rasterize_points(*args)
        ctx.znear = znear
        ctx.zfar = zfar
        ctx.gamma = gamma
        ctx.max_radius = max_radius
        ctx.save_for_backward(points, colors, inv_cov, idx, k_idxs)
        return idx, color, depth_gmms, num_gmms, point_score

    @staticmethod
    def backward(ctx, grad_idx, grad_out_color, grad_depth_gmms, grad_num_gmms, grad_point_score):
        grad_points = None
        grad_colors = None
        grad_inv_cov = None
        grad_max_radius = None
        grad_image_height = None
        grad_image_width = None
        grad_points_per_pixel = None
        grad_bin_size = None
        grad_zfar = None
        grad_znear = None
        grad_gamma = None
        znear = ctx.znear
        zfar = ctx.zfar
        gamma = ctx.gamma
        max_radius = ctx.max_radius
        points, colors, inv_cov, idx, k_idxs = ctx.saved_tensors
        args = (points, colors, inv_cov, max_radius, idx, k_idxs, znear, zfar, gamma, grad_out_color)
        grad_points, grad_colors, grad_inv_cov = _C.rasterize_points_backward(*args)
        grads = (
            grad_points,
            grad_colors,
            grad_inv_cov,
            grad_max_radius,
            grad_image_height,
            grad_image_width,
            grad_points_per_pixel,
            grad_bin_size,
            grad_zfar,
            grad_znear,
            grad_gamma
        )
        return grads
