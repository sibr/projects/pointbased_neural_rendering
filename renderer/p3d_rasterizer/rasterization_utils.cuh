// Copyright (c) Facebook, Inc. and its affiliates. All rights reserved.

#pragma once

__device__ inline float PixToNdc(int i, int S) {
  // NDC x-offset + (i * pixel_width + half_pixel_width)
  return -1 + (2 * i + 1.0f) / S;
}

__device__ inline float NdcToPix(float i, int S) {
  return ((i + 1.0)*S - 1.0)/2.0;
}

const int32_t kMaxPointsPerPixel = 101;
const int32_t kMaxPointPerPixelLocal = 101;

__device__ inline void BubbleSort2(int32_t* arr, const float* points, int n) {
  bool already_sorted;
  // Bubble sort. We only use it for tiny thread-local arrays (n < 8); in this
  // regime we care more about warp divergence than computational complexity.
  for (int i = 0; i < n - 1; ++i) {
    already_sorted=true;
    for (int j = 0; j < n - i - 1; ++j) {
      float p_j0_z = points[arr[j]*3 + 2];
      float p_j1_z = points[arr[j+1]*3 + 2];
      if (p_j1_z < p_j0_z) {
        already_sorted = false;
        int32_t temp = arr[j];
        arr[j] = arr[j + 1];
        arr[j + 1] = temp;
      }
    }
    if (already_sorted)
        break;
  }
}

template <typename T>
__device__ inline void BubbleSort(T* arr, int n) {
  bool already_sorted;
  for (int i = 0; i < n - 1; ++i) {
    already_sorted=true;
    for (int j = 0; j < n - i - 1; ++j) {
      if (arr[j + 1] < arr[j]) {
        already_sorted = false;
        T temp = arr[j];
        arr[j] = arr[j + 1];
        arr[j + 1] = temp;
      }
    }
    if (already_sorted)
        break;
  }
}

