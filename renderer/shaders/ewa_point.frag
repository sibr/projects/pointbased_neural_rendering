/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#version 420

layout(location=0) out vec4 out_color;
layout(location=1) out vec4 out_alphaDepth;
layout(location=2) out vec4 out_distortion;
layout(location=3) out vec4 out_feats1;
layout(location=4) out vec4 out_feats2;

in vec3 gColor;
in vec3 gFeats1;
in vec3 gFeats2;
in float gWeight;
in vec3 gInvCov;
in float gPointSize;
in float gDepth;
in float gDistortion;

void main(void) {
	vec2 diff = (2 * gl_PointCoord - 1) * gPointSize / 2;
	diff.y *= -1;
	float exponent = -0.5 * (	gInvCov.x * diff.x * diff.x + 
								2 * gInvCov.y * diff.x * diff.y + 
								gInvCov.z * diff.y * diff.y);
	float w = exp(exponent) * gWeight;
	if (w < 1./255) discard;
	out_color = vec4(gColor, w);
	out_alphaDepth = vec4(log(1-w), w * gDepth, w * gDepth * gDepth, w);
	out_feats1 = vec4(gFeats1, w);
	out_feats2 = vec4(gFeats2, w);
	out_distortion = vec4(gDistortion, 0, 0, w);

	//out_color = vec4(diff, 0, 1);
}
