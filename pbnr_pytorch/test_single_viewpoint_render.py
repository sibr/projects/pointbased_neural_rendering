import torch
import random
from scene_loaders.ibr_scene import MultiScene
import argparse
from model_defs.Loss_Functions import l1_loss
import uuid
import os
from torch.utils.tensorboard import SummaryWriter
from utils.image_utils import logTransform

parser = argparse.ArgumentParser(description='Train your network sailor.')
parser.add_argument('-i', '--input_path', required=True)
parser.add_argument('-o', '--output_path', required=False)

parser.add_argument('--neural_renderer_weights', required=False)

parser.add_argument('--depth_lr', required=False, type=float, default=0.01)
parser.add_argument('--uncertainty_lr', required=False, type=float, default=0.01)
parser.add_argument('--feature_lr', required=False, type=float, default=0.01)
parser.add_argument('--normal_lr', required=False, type=float, default=0.01)

args = parser.parse_args()

device = torch.device("cuda:0")
torch.cuda.set_device(device)
torch.manual_seed(0)
random.seed(0)

scenes = MultiScene(multi_scene_json="../../scenes/scenes.json",
                   max_radius=8, extra_features=0, test_cameras=3)

unique_str = str(uuid.uuid4())
tensorboard_folder = "./tensorboard_3d/{}".format(unique_str)
assert not os.path.exists(tensorboard_folder), ("Output folder already exists!")
tb_writer = SummaryWriter(tensorboard_folder)

print("Output Folders:")
print(" * {}".format(tensorboard_folder))

viewpoint_cam = scenes.scenes[0].getAllCameras()[5]
test_cam = scenes.scenes[0].getAllCameras()[15]

optimizer_depth_map = torch.optim.Adam(scenes.getAllDepthParameters(), lr=args.depth_lr)
optimizer_uncertainty = torch.optim.Adam(scenes.getAllUncertaintyParameters(), lr=args.uncertainty_lr)
optimizer_features = torch.optim.Adam(scenes.getAllFeatureParameters(), lr=args.feature_lr)
optimizer_normal_map = torch.optim.Adam(scenes.getAllNormalParameters(), lr=args.normal_lr)

for iter in range(20000):
    optimizer_depth_map.zero_grad()
    optimizer_uncertainty.zero_grad()
    optimizer_features.zero_grad()
    optimizer_normal_map.zero_grad()

    rasterized, _, _ = test_cam.render(viewpoint_cam, 1.0)
    loss = l1_loss(rasterized, viewpoint_cam.image.to(device))
    tb_writer.add_scalars('train_loss', {"total_loss": loss.item()}, global_step=iter)
    print("[ITER {}] Loss: {}".format(iter, loss))

    w = test_cam.image_width
    h = test_cam.image_height
    tb_writer.add_images("render", rasterized, global_step=iter)
    tb_writer.add_images("depth", logTransform(test_cam.depth_map + test_cam.depth_delta), global_step=iter)
    tb_writer.add_images("normal", (test_cam.normal_map + test_cam.normal_map_delta + 1.0)/2.0, global_step=iter)
    tb_writer.add_images("uncertainty", test_cam.uncertainty_map.view(h,w,1).permute(2,0,1).unsqueeze(0)-0.5, global_step=iter)

    loss.backward()

    optimizer_depth_map.step()
    optimizer_uncertainty.step()
    optimizer_features.step()
    optimizer_normal_map.step()

"""
rasterized, _, _ = test_cam.render(viewpoint_cam, 1.0)

for cam in scene.scenes[0].getAllCameras():
    print(cam.uid)
    torchvision.utils.save_image(rasterized[:, 0:3, :, :],
                                 "F:/train_point_cloud_viewpoint_{}_pcloud_{}_feature_02.png".format(viewpoint_cam.uid, cam.uid))
    asd=123
"""