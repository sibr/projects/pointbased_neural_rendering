#version 420

uniform mat4 proj;
uniform mat4 world2view;

layout(location = 0) in vec3 in_vertex;
layout(location = 3) in vec3 in_normal;

out vec3 vertex_coord;
out vec3 normal;

void main(void) {
	vec4 viewPos = proj * world2view * vec4(in_vertex, 1.0);
	gl_Position = viewPos; 
	vertex_coord  = in_vertex;

	if (in_normal==vec3(0.0,0.0,0.0)) {
		normal = vec3(1.0,1.0,1.0);
	}
	else {
		normal = normalize(in_normal);
	}
}
